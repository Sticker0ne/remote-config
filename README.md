# remote-config

## Getting started

### Using containers   
Not recomended. dockerfiles are outdated;

### Bare start  

You need have installed NodeJS v14 and yarn 

- Run your redis server and postgresql
- Config and run backend module
- Config and run builder module
- Config and run frontend module

## Running backend module

```shell
git clone -b '2.0.2' --single-branch --depth 1 https://gitlab.com/Sticker0ne/remote-config.git
cd remote-config/backend
yarn install --frozen-lockfile --network-timeout 1000000
yarn build
```

Then you need setup environment variables for backend
Do not just copy! Configure for you

Warn re-generate salt with 
```shell
yarn gensalt
```
Warn change JWT secret keys

```dotenv
APP_PORT=5000
APP_BASE_PATH=api

DATABASE_TYPE=postgres # postgres or mysql
DATABASE_USER=postgres
DATABASE_PASSWORD=postgres-password
DATABASE_NAME=remote_config
DATABASE_HOST=localhost
DATABASE_PORT=5432

PASSWORD_SALT=$2b$10$rpHwhv19f3jaZ4pSt01fIe # Change salt
JWT_ACCESS_SECRET_KEY=jwt-access-secret-key # Change secret key
JWT_ACCESS_EXPIRE_TIME=3600

JWT_REFRESH_SECRET_KEY=jwt-refresh-secret-key # Change secret key
JWT_REFRESH_EXPIRE_TIME=36000

SUPER_ADMIN_EMAIL=admin@email.com #Super admin inside the app
SUPER_ADMIN_PASSWORD=password #Super admin inside the app

PROPERTY_EXECUTION_TIMEOUT=100

#couchbase or redis
CACHE_SERVICE_TYPE=couchbase

REDIS_HOST=localhost
REDIS_PORT=6379

COUCH_BASE_HOST=couchbase://localhost
COUCH_BASE_USERNAME=Administrator
COUCH_BASE_PASSWORD=password
COUCH_BASE_BUCKET=remote-config-bucket

DISABLE_ISOLATION=false
```

Then you can run backend module via   
```shell
yarn start
```

For example, you can set environment variables via
```shell
yarn cross-env NODE_ENV=production SUPER_ADMIN_PASSWORD=super_password DATABASE_PORT=1234 yarn start
```

Note: you should manually set NODE_ENV=production


## Running builder module

```shell
git clone -b '2.0.2' --single-branch --depth 1 https://gitlab.com/Sticker0ne/remote-config.git
cd remote-config/backend
yarn install --frozen-lockfile --network-timeout 1000000
```

Then you need setup environment variables for builder
Do not just copy! Configure for you

```dotenv
BUILDER_APP_HOST=0.0.0.0
BUILDER_APP_PORT=8080
BUILDER_API_PREFIX=/fast-builder

#couchbase or redis
CACHE_SERVICE_TYPE=couchbase

REDIS_HOST=localhost
REDIS_PORT=6379

COUCH_BASE_HOST=couchbase://localhost
COUCH_BASE_USERNAME=Administrator
COUCH_BASE_PASSWORD=password
COUCH_BASE_BUCKET=remote-config-bucket

ISOLATE_MEMORY_LIMIT=4096
DISABLE_ISOLATION=false
```

Then you can run builder module via
```shell
yarn start:builder-module
```

For example, you can set environment variables via
```shell
yarn cross-env CACHE_SERVICE_TYPE=couchbase COUCH_BASE_HOST=couchbase://localhost COUCH_BASE_USERNAME=Administrator COUCH_BASE_PASSWORD=password COUCH_BASE_BUCKET=remote-config-bucket yarn start:builder-module
```

## Running frontend module

```shell
git clone -b '2.0.2' --single-branch --depth 1 https://gitlab.com/Sticker0ne/remote-config.git
cd remote-config/frontend
yarn install --frozen-lockfile --network-timeout 1000000
```

Then you need setup environment variables for builder
Do not just copy! Configure for you

```dotenv
NODE_ENV=production
REACT_APP_API_URL=some-api.url.com
REACT_APP_SANDBOX_PREFIX=/api/v1/configs/result
REACT_APP_FAST_SANDBOX_PREFIX=/fast-builder
```
Then you can build frontend module via
```shell
yarn build
```

For example, you can set environment variables via
```shell
yarn REACT_APP_API_URL=some-another-api.url.com yarn start:builder-module
```

Then you should copy "build" path to nginx virtual directory (entrypoint is index.html)

#NOTES
Important: frontend and backend apps should be on the same host in order to cookies are able to work

Target arch
![arch](docs/arch.png)
