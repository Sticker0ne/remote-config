## Todo list

- [ ] fix working process with errors (check types of codes, create error creator and so on)
- [x] create logout endpoint
- [ ] add field property for errors
- [x] check JWT refresh
- [x] connect vm2 to jail untrusted code https://github.com/patriksimek/vm2 or https://github.com/laverdet/isolated-vm
- [ ] add possibility to get types per config
