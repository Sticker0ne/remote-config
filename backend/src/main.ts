import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AppConfigService } from './config/app-config.service';
import { AppConstants } from '@src/utils/app-constants';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { exceptionFactory } from '@src/common/pipe-exception-factory';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const appConfigService = new AppConfigService(AppConstants.envFilePath);

  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(appConfigService.envConfig.appBasePath);
  app.enableVersioning({ type: VersioningType.URI });
  app.use(cookieParser());
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: exceptionFactory,
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  app.enableCors();

  const config = new DocumentBuilder()
    .setTitle('remote config')
    .setDescription('The remote config API description')
    .setVersion('1.0')
    .addTag('remote config')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);

  await app.listen(appConfigService.envConfig.appPort);
}
bootstrap();
