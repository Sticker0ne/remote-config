import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppConfigService } from '@src/config/app-config.service';
import { AppConstants } from '@src/utils/app-constants';
import { UsersModule } from './business/users/users.module';
import { AuthModule } from './auth/auth.module';
import { AppConfigModule } from '@src/config/appConfigModule';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from '@src/auth/jwt-auth.guard';
import { ConfigsModule } from './business/configs/configs.module';
import { VariablesModule } from './business/variables/variables.module';
import { ExtractorsModule } from './business/extractors/extractors.module';
import { PropertiesModule } from './business/properties/properties.module';
import { CacheModule } from './cache/cache.module';

const appConfigService = new AppConfigService(AppConstants.envFilePath);

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: appConfigService.envConfig.databaseType,
      host: appConfigService.envConfig.databaseHost,
      port: appConfigService.envConfig.databasePort,
      username: appConfigService.envConfig.databaseUser,
      password: appConfigService.envConfig.databasePassword,
      database: appConfigService.envConfig.databaseName,
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    UsersModule,
    AuthModule,
    AppConfigModule,
    ConfigsModule,
    VariablesModule,
    ExtractorsModule,
    PropertiesModule,
    CacheModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
