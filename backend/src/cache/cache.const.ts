export const CACHE_KEY_GLOBAL_VARIABLES = 'variables';
export const CACHE_KEY_WARMED_UP = 'warmedUp';
export const CACHE_KEY_CONFIG_PREFIX = 'config_';
