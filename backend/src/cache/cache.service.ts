import { HttpStatus, Injectable } from '@nestjs/common';
import { AppConfigService } from '@src/config/app-config.service';
import { createClient, RedisClient } from 'redis';
import { IErrorAble } from '@src/common/dto/error.types';
import { ConfigsService } from '@src/business/configs/configs.service';
import {
  CACHE_KEY_CONFIG_PREFIX,
  CACHE_KEY_GLOBAL_VARIABLES,
  CACHE_KEY_WARMED_UP,
} from '@src/cache/cache.const';
import { BuildConfigService } from '@src/business/configs/build-config.service';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { ICacheConfig } from '@src/cache/types/cache-config.type';
import { CommonUtils } from '@src/utils/common.utils';
import { ICacheExtractor } from '@src/cache/types/cache-extractor.type';
import { ICacheProperty } from '@src/cache/types/cache-property.type';
import { dataTypeToDefaultValueMap } from '@src/business/configs/constants/data.const';
import { connect } from 'couchbase';
import { CacheServiceTypesEnum } from '@src/config/config.types';
import { validateDataTypeValue } from '@src/business/common/validators/data-types-validator';

@Injectable()
export class CacheService {
  public constructor(
    private readonly appConfigService: AppConfigService,
    private readonly configService: ConfigsService,
    private readonly buildConfigService: BuildConfigService,
  ) {}

  private convertConfigToCacheConfig(config: ConfigEntity): ICacheConfig {
    const { extractors, variables, properties, name } = config;

    const { payload: processedVariables } =
      this.buildConfigService.extractDataFromVariableCollection(variables);

    const newVariablesData = processedVariables || {};

    const newExtractors: ICacheExtractor[] = extractors.map((extractor) => {
      const { name, dataType, extractName, extractWay, stringifiedDefaultValue } = extractor;

      const valueHasCorrectDataType = validateDataTypeValue(
        dataType,
        CommonUtils.jsonSafeParse(stringifiedDefaultValue),
      );

      const defaultValue = valueHasCorrectDataType
        ? CommonUtils.jsonSafeParse(stringifiedDefaultValue)
        : dataTypeToDefaultValueMap[dataType];

      return { defaultValue, name, dataType, extractName, extractWay };
    });

    const newProperties: ICacheProperty[] = properties.map((property) => {
      const { name, dataType, code, stringifiedDefaultValue } = property;

      const valueHasCorrectDataType = validateDataTypeValue(
        dataType,
        CommonUtils.jsonSafeParse(stringifiedDefaultValue),
      );

      const defaultValue = valueHasCorrectDataType
        ? CommonUtils.jsonSafeParse(stringifiedDefaultValue)
        : dataTypeToDefaultValueMap[dataType];

      return { defaultValue, name, dataType, code };
    });

    return { name, properties: newProperties, extractors: newExtractors, variables: newVariablesData };
  }

  private save(redisClient: RedisClient): Promise<void> {
    return new Promise((resolve) => {
      redisClient.save(() => resolve());
    });
  }

  private quit(redisClient: RedisClient): Promise<void> {
    return new Promise((resolve) => {
      redisClient.quit(() => resolve());
    });
  }

  private async fillRedisCache(): Promise<IErrorAble<boolean>> {
    let redisClient!: RedisClient;
    try {
      const { redisHost, redisPort } = this.appConfigService.envConfig;
      redisClient = createClient(redisPort, redisHost);
      redisClient.on('error', (err) => console.log('Redis Client Error', err));

      const { payload: variablesData } = await this.buildConfigService.extractDataFromGlobalVariables();
      const configs = await this.configService.getAllWithRelations();

      redisClient.set(CACHE_KEY_GLOBAL_VARIABLES, JSON.stringify(variablesData || {}));
      redisClient.set(CACHE_KEY_WARMED_UP, JSON.stringify(true));

      configs.forEach((config) => {
        const key = CACHE_KEY_CONFIG_PREFIX + config.name;
        const cacheConfig = this.convertConfigToCacheConfig(config);
        redisClient.set(key, JSON.stringify(cacheConfig));
      });

      await this.save(redisClient);
    } catch (e) {
      const res = {
        errors: [
          {
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            message: 'Unexpected error during setting cache',
            details: e.toString(),
          },
        ],
        payload: false,
      };
      return res;
    }

    if (redisClient) await this.quit(redisClient);

    return { payload: true, errors: null };
  }

  private async fillCouchBaseCache(): Promise<IErrorAble<boolean>> {
    try {
      const { couchBaseHost, couchBaseUsername, couchBasePassword, couchBaseBucket } =
        this.appConfigService.envConfig;

      const cluster = await connect(couchBaseHost, {
        username: couchBaseUsername,
        password: couchBasePassword,
      });

      const bucket = cluster.bucket(couchBaseBucket);
      const cache = bucket.defaultCollection();

      const { payload: variablesData } = await this.buildConfigService.extractDataFromGlobalVariables();
      const configs = await this.configService.getAllWithRelations();

      await cache.upsert(CACHE_KEY_GLOBAL_VARIABLES, JSON.stringify(variablesData || {}));
      await cache.upsert(CACHE_KEY_WARMED_UP, JSON.stringify(true));

      await Promise.all(
        configs.map(async (config) => {
          const key = CACHE_KEY_CONFIG_PREFIX + config.name;
          const cacheConfig = this.convertConfigToCacheConfig(config);
          return cache.upsert(key, JSON.stringify(cacheConfig));
        }),
      );

      return { payload: true };
    } catch (e) {
      const res = {
        errors: [
          {
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            message: 'Unexpected error during setting cache',
            details: e.toString(),
          },
        ],
        payload: false,
      };
      return res;
    }
  }

  public fillCache(): Promise<IErrorAble<boolean>> {
    const { cacheServiceType } = this.appConfigService.envConfig;

    if (cacheServiceType === CacheServiceTypesEnum.redis) return this.fillRedisCache();
    if (cacheServiceType === CacheServiceTypesEnum.couchbase) return this.fillCouchBaseCache();

    return Promise.resolve({
      errors: [
        {
          code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
          message: 'Incorrect environment variables for cache service. See README.md',
        },
      ],
    });
  }
}
