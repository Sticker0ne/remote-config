import { Controller, HttpStatus, Post, Res, Version } from '@nestjs/common';
import { ApiNoContentResponse, ApiTags } from '@nestjs/swagger';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { TypedHttpException } from '@src/common/typed-http-exception';
import { CacheService } from '@src/cache/cache.service';
import { Response } from 'express';

@Controller('cache')
@ApiTags('cache')
export class CacheController {
  constructor(private readonly service: CacheService) {}

  @ApiNoContentResponse()
  @Version('1')
  @Post('/fill')
  async fillV1(@Res() response: Response): Promise<ErrorsDto | void> {
    const { payload, errors } = await this.service.fillCache();

    if (!payload) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    response.status(HttpStatus.NO_CONTENT);
    response.send();
  }
}
