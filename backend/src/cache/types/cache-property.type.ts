import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { DefaultValueType } from '@src/cache/types/default-value.type';

export interface ICacheProperty {
  dataType: DataTypes;
  name: string;
  code: string;
  defaultValue: DefaultValueType;
}
