import { ICacheProperty } from '@src/cache/types/cache-property.type';
import { ICacheExtractor } from '@src/cache/types/cache-extractor.type';

export interface ICacheConfig {
  name: string;
  properties: ICacheProperty[];
  variables: Record<string, any>;
  extractors: ICacheExtractor[];
}
