import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { ExtractWays } from '@src/business/extractors/entities/extract-way.enum';
import { DefaultValueType } from '@src/cache/types/default-value.type';

export interface ICacheExtractor {
  dataType: DataTypes;
  extractWay: ExtractWays;
  extractName: string;
  name: string;
  defaultValue: DefaultValueType;
}
