export interface ITokenPayload {
  email: string;
  id: number;
}
