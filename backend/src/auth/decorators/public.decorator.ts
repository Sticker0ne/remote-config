import { SetMetadata } from '@nestjs/common';
import { AppConstants } from '@src/utils/app-constants';

export const Public = () => SetMetadata(AppConstants.metaKeys.isPublic, true);
