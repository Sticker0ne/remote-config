import { SetMetadata } from '@nestjs/common';
import { AppConstants } from '@src/utils/app-constants';

export const ForSuperAdminOnly = () => SetMetadata(AppConstants.metaKeys.isForSuperAdminOnly, true);
