import { Body, Controller, Get, HttpCode, HttpStatus, Post, Res, Version } from '@nestjs/common';
import { UserResponseDto } from '@src/business/users/dto/user-response.dto';
import {
  ApiBadRequestResponse,
  ApiNoContentResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { TypedHttpException } from '@src/common/typed-http-exception';
import { AuthService } from '@src/auth/auth.service';
import { LoginRequestDto } from '@src/auth/dto/login-request.dto';
import { Response } from 'express';
import { Public } from '@src/auth/decorators/public.decorator';
import { clearResponseJWTCookies, setResponseJWTCookies } from '@src/auth/utils/cookie.utils';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiNoContentResponse({ type: UserResponseDto, description: 'authorization success' })
  @ApiBadRequestResponse({ type: ErrorsDto, description: 'incorrect request params' })
  @ApiUnauthorizedResponse({ type: ErrorsDto, description: 'incorrect credentials' })
  @Version('1')
  @Public()
  @Post('login')
  @HttpCode(HttpStatus.OK)
  async loginV1(@Body() request: LoginRequestDto, @Res() response: Response) {
    const loginResultData = await this.authService.validateUser(request.email, request.password);

    if (!loginResultData.payload) {
      clearResponseJWTCookies(response);
      throw new TypedHttpException({ errors: loginResultData.errors }, HttpStatus.NOT_ACCEPTABLE);
    }

    const tokens = await this.authService.createTokens(loginResultData.payload);

    setResponseJWTCookies(response, tokens.accessToken, tokens.refreshToken);
    response.status(HttpStatus.NO_CONTENT);
    response.send();
  }

  @ApiNoContentResponse({ description: 'logout success' })
  @Version('1')
  @Public()
  @Get('logout')
  @HttpCode(HttpStatus.OK)
  logoutV1(@Res() response: Response) {
    clearResponseJWTCookies(response);

    response.status(HttpStatus.NO_CONTENT);
    response.send();
  }
}
