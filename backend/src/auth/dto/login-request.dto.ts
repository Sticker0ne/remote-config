import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginRequestDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'user email' })
  public email: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'user password' })
  public password: string;
}
