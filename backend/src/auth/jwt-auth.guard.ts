import { ExecutionContext, HttpStatus, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TypedHttpException } from '@src/common/typed-http-exception';
import { AppConstants } from '@src/utils/app-constants';
import { AuthService } from '@src/auth/auth.service';
import { UsersService } from '@src/business/users/users.service';
import { Reflector } from '@nestjs/core';
import { setRequestJWTCookies, setResponseJWTCookies } from '@src/auth/utils/cookie.utils';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private reflector: Reflector,
  ) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic = this.reflector.getAllAndOverride<boolean>(AppConstants.metaKeys.isPublic, [
      context.getHandler(),
      context.getClass(),
    ]);

    const isForSuperAdminOnly = this.reflector.getAllAndOverride<boolean>(
      AppConstants.metaKeys.isForSuperAdminOnly,
      [context.getHandler(), context.getClass()],
    );

    if (isPublic) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const response = context.switchToHttp().getResponse();

    try {
      const accessToken = request.cookies?.[AppConstants.cookiesKeys.jwtAccessTokenKey];
      if (!accessToken) throw this.createError('Access token is not set');

      const payload = this.authService.getAccessTokenPayload(accessToken);
      const condition = { id: payload.id, isActive: true };
      const userData = await this.usersService.findOneByCondition(condition);
      if (!userData.payload) throw this.createError('user is not valid');
      if (isForSuperAdminOnly && !userData.payload.isSuperAdmin)
        throw this.createError('super admin permission denied');

      const isValidAccessToken = this.authService.verifyAccessToken(accessToken);
      if (isValidAccessToken) return this.activate(context);

      const refreshToken = request.cookies?.[AppConstants.cookiesKeys.jwtRefreshTokenKey];
      if (!refreshToken) throw this.createError('Refresh token is not set');

      const isValidTokensPair = this.authService.verifyTokenPair(accessToken, refreshToken);
      if (!isValidTokensPair) throw this.createError('Refresh or access token is not valid');

      const tokens = this.authService.createTokens(userData.payload);

      setResponseJWTCookies(response, tokens.accessToken, tokens.refreshToken);
      setRequestJWTCookies(request, tokens.accessToken, tokens.refreshToken);

      return this.activate(context);
    } catch (err) {
      response.clearCookie(AppConstants.cookiesKeys.jwtAccessTokenKey, { httpOnly: true });
      response.clearCookie(AppConstants.cookiesKeys.jwtRefreshTokenKey, { httpOnly: true });

      throw err;
    }
  }

  async activate(context: ExecutionContext): Promise<boolean> {
    return super.canActivate(context) as Promise<boolean>;
  }

  createError(message: string) {
    return new TypedHttpException(
      {
        errors: [{ code: HttpStatus.UNAUTHORIZED.toString(), message }],
      },
      HttpStatus.UNAUTHORIZED,
    );
  }

  handleRequest(err, user) {
    if (err || !user) {
      throw err || this.createError('Unauthorized');
    }
    return user;
  }
}
