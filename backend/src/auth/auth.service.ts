import { HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '@src/business/users/users.service';
import * as bcrypt from 'bcrypt';
import * as jsonwebtoken from 'jsonwebtoken';
import type { IErrorAble } from '@src/common/dto/error.types';
import type { User } from '@src/business/users/entities/user.entity';
import { AppConfigService } from '@src/config/app-config.service';
import type { ITokenPayload } from '@src/auth/types/token-payload.type';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly appConfigService: AppConfigService,
  ) {}

  async validateUser(email: string, pass: string): Promise<IErrorAble<User>> {
    const userFindResult = await this.usersService.findOneByCondition({ email, isActive: true });
    const userValidateErroredResult = {
      errors: [{ code: HttpStatus.UNAUTHORIZED.toString(), message: 'Incorrect credentials' }],
    };

    if (!userFindResult.payload) return userValidateErroredResult;

    if (bcrypt.compareSync(pass, userFindResult.payload.passwordHash)) {
      return { payload: userFindResult.payload, errors: [] };
    }
    return userValidateErroredResult;
  }

  createTokens(user: ITokenPayload) {
    const payload: ITokenPayload = { email: user.email, id: user.id };
    const { jwtAccessSecretKey, jwtAccessExpireTime, jwtRefreshExpireTime, jwtRefreshSecretKey } =
      this.appConfigService.envConfig;

    return {
      accessToken: jsonwebtoken.sign(payload, jwtAccessSecretKey, {
        expiresIn: jwtAccessExpireTime,
      }),
      refreshToken: jsonwebtoken.sign(payload, jwtRefreshSecretKey, {
        expiresIn: jwtRefreshExpireTime,
      }),
    };
  }

  verifyAccessToken(token: string) {
    const { jwtAccessSecretKey } = this.appConfigService.envConfig;

    try {
      const payloadAccess = jsonwebtoken.verify(token, jwtAccessSecretKey, {
        ignoreExpiration: false,
      });

      return !!payloadAccess;
    } catch (err) {
      return false;
    }
  }

  verifyTokenPair(access: string, refresh: string) {
    const { jwtAccessSecretKey, jwtRefreshSecretKey } = this.appConfigService.envConfig;

    try {
      const payloadAccess = jsonwebtoken.verify(access, jwtAccessSecretKey, {
        ignoreExpiration: true,
      });
      const payloadRefresh = jsonwebtoken.verify(refresh, jwtRefreshSecretKey, {
        ignoreExpiration: false,
      });

      if (typeof payloadAccess === 'string' || typeof payloadRefresh === 'string')
        return payloadAccess === payloadRefresh;

      return payloadAccess.id === payloadRefresh.id;
    } catch (err) {
      return false;
    }
  }

  getAccessTokenPayload(token: string): ITokenPayload | null {
    const { jwtAccessSecretKey } = this.appConfigService.envConfig;

    try {
      const payloadAccess = jsonwebtoken.verify(token, jwtAccessSecretKey, {
        ignoreExpiration: true,
      });

      if (typeof payloadAccess === 'string') return null;
      return payloadAccess as ITokenPayload;
    } catch (err) {
      return null;
    }
  }
}
