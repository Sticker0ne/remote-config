import { Global, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from '@src/business/users/users.module';
import { AuthController } from './auth.controller';
import { JwtStrategy } from '@src/auth/jwt.strategy';

@Global()
@Module({
  providers: [AuthService, JwtStrategy],
  imports: [UsersModule],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
