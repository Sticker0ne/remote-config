import type { Request, Response } from 'express';
import { AppConstants } from '@src/utils/app-constants';
import { AppConfigService } from '@src/config/app-config.service';

const appConfigService = new AppConfigService(AppConstants.envFilePath);

// WARNING unclearFunction
export function clearResponseJWTCookies(res: Response) {
  res.clearCookie(AppConstants.cookiesKeys.jwtAccessTokenKey, { httpOnly: true });
  res.clearCookie(AppConstants.cookiesKeys.jwtRefreshTokenKey, { httpOnly: true });
}

// WARNING unclearFunction
export function setResponseJWTCookies(res: Response, access: string, refresh: string) {
  const millisInSecond = 1000;
  res.cookie(AppConstants.cookiesKeys.jwtAccessTokenKey, access, {
    httpOnly: true,
    maxAge: appConfigService.envConfig.jwtRefreshExpireTime * millisInSecond,
  });
  res.cookie(AppConstants.cookiesKeys.jwtRefreshTokenKey, refresh, {
    httpOnly: true,
    maxAge: appConfigService.envConfig.jwtRefreshExpireTime * millisInSecond,
  });
}

// WARNING unclearFunction
export function setRequestJWTCookies(req: Request, access: string, refresh: string) {
  req.cookies[AppConstants.cookiesKeys.jwtAccessTokenKey] = access;
  req.cookies[AppConstants.cookiesKeys.jwtRefreshTokenKey] = refresh;
}
