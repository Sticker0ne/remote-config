import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AppConfigService } from '@src/config/app-config.service';
import { AppConstants } from '@src/utils/app-constants';
import type { ITokenPayload } from '@src/auth/types/token-payload.type';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly appConfigService: AppConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request) => {
          return request?.cookies?.[AppConstants.cookiesKeys.jwtAccessTokenKey];
        },
      ]),
      ignoreExpiration: false,
      secretOrKey: appConfigService.envConfig.jwtAccessSecretKey,
    });
  }

  validate(payload: ITokenPayload) {
    return { id: payload.id, email: payload.email };
  }
}
