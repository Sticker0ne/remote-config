import * as t from 'io-ts';
import prettyReporter from 'io-ts-reporters';

type IEnum = {
  [key in string | number]: string | number;
};

export class FptsUtils {
  public static optional<RT extends t.Any>(
    type: RT,
    name = `${type.name} | undefined`,
  ): t.UnionType<
    [RT, t.UndefinedType],
    t.TypeOf<RT> | undefined,
    t.OutputOf<RT> | undefined,
    t.InputOf<RT> | undefined
  > {
    return t.union<[RT, t.UndefinedType]>([type, t.undefined], name);
  }

  /**
   * Проверяет является ли enum с цифровыми значениями или со строками.
   * @param e enum который нужно проверить
   */
  private static isConstEnum(e: any): boolean {
    const values = Object.values(e);
    const firstType = typeof values[0];
    let result = false;

    values.forEach((value) => {
      if (typeof value !== firstType) result = true;
    });

    return result;
  }

  /**
   * Приведит enum к массиву строк или цифр. Если enum числовой - то строки, иначе цифры
   * @param e enum который нужно привести к массиву
   */
  private static enumToArray<T>(e: T): [T[keyof T]] {
    if (this.isConstEnum(e)) {
      const keys = Object.keys(e).filter((k) => typeof (e as any)[k as any] === 'number');
      const values = keys.map((k) => (e as any)[k as any]);
      return values as any;
    }

    return Object.values(e) as any;
  }

  public static validatorFromEnum<T extends IEnum>(e: T, name?: string): t.Type<T[keyof T]> {
    const arr = this.enumToArray(e);
    if (arr.length < 2) throw new Error('Enum min length is 2');

    if (typeof arr[0] === 'string') {
      const reducedArr = arr.reduce((acc, value: string | number) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        acc[value] = null;
        return acc;
      }, {});

      return t.keyof(reducedArr, name) as any;
    }

    const arrayOfLiteral: t.LiteralC<T[keyof T]>[] = [];
    arr.forEach((value, index) => {
      arrayOfLiteral[index] = t.literal(value);
    });

    const union = t.union(
      arrayOfLiteral as [t.LiteralC<T[keyof T]>, t.LiteralC<T[keyof T]>, ...t.LiteralC<T[keyof T]>[]],
      name,
    );

    return union;
  }

  public static decode<T, O, I>(
    validator: t.Type<T, O, I>,
    payload: I,
  ): { value: T | null; errors: string[] | null } {
    const decodedValue = validator.decode(payload);
    const isRight = decodedValue._tag === 'Right';

    if (isRight) return { value: payload as unknown as T, errors: null };
    else return { value: null, errors: prettyReporter.report(decodedValue) };
  }
}
