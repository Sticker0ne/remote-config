export class AppConstants {
  public static envFilePath = `env/${process.env.NODE_ENV}.env`;
  public static errorCodes = {
    databaseUniqueKeyConstraintError: '23505', // code from typeorm or PG
    databaseForeignKeyConstraintError: '23503', // code from typeorm or PG
  };
  public static cookiesKeys = {
    jwtRefreshTokenKey: 'jwtRefreshToken',
    jwtAccessTokenKey: 'jwtAccessToken',
  };
  public static metaKeys = {
    isPublic: 'isPublic',
    isForSuperAdminOnly: 'isForSuperAdminOnly',
  };
}
