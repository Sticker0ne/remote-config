export class CommonUtils {
  public static jsonClone<T>(payload: T): T {
    return JSON.parse(JSON.stringify(payload));
  }

  public static jsonSafeParse(payload: any): any {
    try {
      // Fix glitch with single length arrays
      if (Array.isArray(payload)) return payload;

      return JSON.parse(payload);
    } catch (e) {
      return payload;
    }
  }

  public static jsonSafeParseArray(payload: any): any {
    try {
      // Fix glitch with single length arrays
      if (!Array.isArray(payload)) return payload;

      return payload.map(CommonUtils.jsonSafeParse);
    } catch (e) {
      return payload;
    }
  }
}
