import { ParseIntPipe } from '@nestjs/common';
import { parsePipeExceptionFactory } from '@src/common/pipe-exception-factory';

export const getCustomParseIntPipe = () => new ParseIntPipe({ exceptionFactory: parsePipeExceptionFactory });
