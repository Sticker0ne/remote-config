import iterate from 'iterare';
import type { ValidationError } from '@nestjs/common';
import { HttpStatus } from '@nestjs/common';
import { TypedHttpException } from '@src/common/typed-http-exception';

function mapChildrenToValidationErrors(error, parentPath) {
  if (!(error.children && error.children.length)) {
    return [error];
  }
  const validationErrors = [];
  parentPath = parentPath ? `${parentPath}.${error.property}` : error.property;
  for (const item of error.children) {
    if (item.children && item.children.length) {
      validationErrors.push(...this.mapChildrenToValidationErrors(item, parentPath));
    }
    validationErrors.push(this.prependConstraintsWithParentProp(parentPath, item));
  }
  return validationErrors;
}

function flattenValidationErrors(validationErrors): string[] {
  return iterate(validationErrors)
    .map((error) => mapChildrenToValidationErrors(error, ''))
    .flatten()
    .filter((item) => !!item.constraints)
    .map((item) => Object.values(item.constraints))
    .flatten()
    .toArray() as string[];
}

export function exceptionFactory(validationErrors: ValidationError[] = []) {
  const flattenErrorsString = flattenValidationErrors(validationErrors);
  const preparedErrors = flattenErrorsString.map((error) => ({
    code: HttpStatus.BAD_REQUEST.toString(),
    message: error,
  }));
  return new TypedHttpException(
    {
      errors: preparedErrors,
    },
    HttpStatus.BAD_REQUEST,
  );
}

export function parsePipeExceptionFactory(message: string) {
  const error = {
    code: HttpStatus.BAD_REQUEST.toString(),
    message,
  };

  return new TypedHttpException(
    {
      errors: [error],
    },
    HttpStatus.BAD_REQUEST,
  );
}
