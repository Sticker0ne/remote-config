import { ApiProperty } from '@nestjs/swagger';
import { ErrorDto } from '@src/common/dto/error.dto';

export class ErrorsDto {
  @ApiProperty({ type: [ErrorDto], description: 'errors list' })
  errors!: ErrorDto[];
}
