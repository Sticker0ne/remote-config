export interface IError {
  code: string;
  message: string;
  details?: string;
}

export interface IErrorAble<P> {
  payload?: P;
  errors?: IError[] | null;
}
