import { IError } from '@src/common/dto/error.types';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class ErrorDto implements IError {
  @ApiProperty({ description: 'error code' })
  code: string;

  @ApiProperty({ description: 'error message' })
  message: string;

  @ApiPropertyOptional({ description: 'error details' })
  details?: string;
}
