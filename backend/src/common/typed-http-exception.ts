import { HttpException, HttpStatus } from '@nestjs/common';
import { ErrorsDto } from '@src/common/dto/errors.dto';

export class TypedHttpException extends HttpException {
  constructor(payload: ErrorsDto, status: HttpStatus | number) {
    super(payload, status);
  }
}
