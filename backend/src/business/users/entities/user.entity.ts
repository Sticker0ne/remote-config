import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity({ name: 'users', orderBy: { id: 'DESC' } })
export class User {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ unique: true })
  public email: string;

  @Column()
  public firstName: string;

  @Column()
  public lastName: string;

  @Column()
  public passwordHash: string;

  @Column({ default: false })
  public isSuperAdmin!: boolean;

  @Column({ default: true })
  public isActive!: boolean;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}
