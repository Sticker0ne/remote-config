import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  Res,
  Version,
} from '@nestjs/common';
import { UsersService } from '@src/business/users/users.service';
import { CreateUserRequestDto } from '@src/business/users/dto/create-user-request.dto';
import { UserResponseDto } from '@src/business/users/dto/user-response.dto';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { TypedHttpException } from '@src/common/typed-http-exception';
import { Mapper } from 'ts-simple-automapper';
import { getCustomParseIntPipe } from '@src/common/custom-pipes';
import { GetAllUsersQueryDto } from '@src/business/users/dto/get-all-users-query.dto';
import { ForSuperAdminOnly } from '@src/auth/decorators/super-admin-only.decorator';
import { UpdateUserRequestDto } from '@src/business/users/dto/update-user-request.dto';
import { CurrentUser } from '@src/auth/decorators/current-user.decorator';
import { ITokenPayload } from '@src/auth/types/token-payload.type';
import { Response } from 'express';

@Controller('users')
@ApiTags('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiCreatedResponse({ type: UserResponseDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @ForSuperAdminOnly()
  @Post()
  async createV1(@Body() body: CreateUserRequestDto): Promise<UserResponseDto> {
    const response = await this.usersService.create(body);

    if (!response.payload) {
      throw new TypedHttpException({ errors: response.errors }, HttpStatus.BAD_REQUEST);
    }

    return new Mapper().map(response.payload, new UserResponseDto());
  }

  @ApiOkResponse({ type: UserResponseDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get('/current')
  async getCurrentUserV1(@CurrentUser() currentUser: ITokenPayload): Promise<UserResponseDto> {
    const response = await this.usersService.findOneByCondition({
      id: currentUser.id,
      isActive: true,
    });

    if (!response.payload) {
      throw new TypedHttpException({ errors: response.errors }, HttpStatus.NOT_FOUND);
    }

    return new Mapper().map(response.payload, new UserResponseDto());
  }

  @ApiOkResponse({ type: UserResponseDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get(':id')
  async getV1(@Param('id', getCustomParseIntPipe()) userId: number): Promise<UserResponseDto> {
    const response = await this.usersService.findOneByCondition({ id: userId });

    if (!response.payload) {
      throw new TypedHttpException({ errors: response.errors }, HttpStatus.NOT_FOUND);
    }

    return new Mapper().map(response.payload, new UserResponseDto());
  }

  @ApiOkResponse({ type: [UserResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ForSuperAdminOnly()
  @Version('1')
  @Get()
  async getAllV1(@Query() query: GetAllUsersQueryDto): Promise<UserResponseDto[]> {
    const condition = query.includeDisabled ? {} : { isActive: true };
    const users = await this.usersService.findAllByCondition(condition);

    return new Mapper().mapList(users, UserResponseDto);
  }

  @ApiOkResponse({ type: [UserResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ForSuperAdminOnly()
  @Version('1')
  @Patch(':id')
  async updateV1(
    @Body() body: UpdateUserRequestDto,
    @Param('id', getCustomParseIntPipe()) id: number,
  ): Promise<UserResponseDto> {
    const { payload, errors } = await this.usersService.patchUser(id, body);

    if (!payload) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    return new Mapper().map(payload, new UserResponseDto());
  }

  @ApiNoContentResponse()
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ForSuperAdminOnly()
  @Version('1')
  @Delete(':id')
  async removeV1(
    @Param('id', getCustomParseIntPipe()) id: number,
    @Res() response: Response,
  ): Promise<ErrorsDto | void> {
    const { payload, errors } = await this.usersService.remove(id);

    if (!payload) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    response.status(HttpStatus.NO_CONTENT);
    response.send();
  }
}
