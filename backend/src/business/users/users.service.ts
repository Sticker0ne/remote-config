import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@src/business/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserRequestDto } from '@src/business/users/dto/create-user-request.dto';
import * as bcrypt from 'bcrypt';
import { AppConfigService } from '@src/config/app-config.service';
import { UserResponseDto } from '@src/business/users/dto/user-response.dto';
import type { IError, IErrorAble } from '@src/common/dto/error.types';
import { AppConstants } from '@src/utils/app-constants';

@Injectable()
export class UsersService {
  public constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    private readonly appConfigService: AppConfigService,
  ) {
    this.createDefaultSuperAdminAtAppStart();
  }

  private generatePasswordHash(password: string): Promise<string> {
    return bcrypt.hash(password, this.appConfigService.envConfig.passwordSalt);
  }

  public async create(userCreateModel: CreateUserRequestDto): Promise<IErrorAble<UserResponseDto>> {
    const passwordHash = await this.generatePasswordHash(userCreateModel.password);

    try {
      const user = await this.usersRepository.save({ ...userCreateModel, passwordHash, isActive: true });
      return { payload: user, errors: [] };
    } catch (err) {
      if (err.code === AppConstants.errorCodes.databaseUniqueKeyConstraintError) {
        const error: IError = { code: err.code, message: err.message || '', details: err.detail || '' };
        return { errors: [error] };
      }

      throw err;
    }
  }

  public async findOneByCondition(condition: Partial<User>): Promise<IErrorAble<User>> {
    const user = await this.usersRepository.findOne(condition);

    if (user) return { payload: user, errors: [] };
    else
      return {
        errors: [{ code: HttpStatus.NOT_FOUND.toString(), message: 'User not found' }],
      };
  }

  public async findAllByCondition(condition: Partial<User>): Promise<User[]> {
    const users = await this.usersRepository.find(condition);

    return users;
  }

  private async createDefaultSuperAdminAtAppStart() {
    const { superAdminEmail, superAdminPassword } = this.appConfigService.envConfig;
    if (!superAdminEmail || !superAdminPassword) return;

    const passwordHash = await bcrypt.hash(superAdminPassword, this.appConfigService.envConfig.passwordSalt);

    try {
      const { id } = (await this.usersRepository.findOne({ email: superAdminEmail })) || {};
      await this.usersRepository.save({
        id,
        firstName: 'fn',
        lastName: 'ln',
        email: superAdminEmail,
        passwordHash,
        isSuperAdmin: true,
        isActive: true,
      });
    } catch (err) {}
  }

  public async patchUser(
    id: number,
    partialUserData: Partial<User> & { password?: string },
  ): Promise<IErrorAble<User>> {
    if (partialUserData?.password) {
      partialUserData.passwordHash = await this.generatePasswordHash(partialUserData.password);
      delete partialUserData.password;
    }
    try {
      const { payload: user, errors } = await this.findOneByCondition({
        id: id,
      });
      if (!user) return { errors };
      const updateData = await this.usersRepository.save({ ...user, ...partialUserData });
      return { payload: updateData, errors: null };
    } catch (err) {
      return {
        errors: [
          {
            message: err.message,
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            details: JSON.stringify(err, null, 4),
          },
        ],
      };
    }
  }

  public async remove(id: number): Promise<IErrorAble<any>> {
    try {
      const deleteResult = await this.usersRepository.delete({ id });
      if (deleteResult.affected > 0) return { payload: deleteResult, errors: null };
      else
        return {
          errors: [
            {
              message: 'Item was not found',
              code: HttpStatus.NOT_FOUND.toString(),
            },
          ],
        };
    } catch (err) {
      return {
        errors: [
          {
            message: err.message,
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            details: JSON.stringify(err, null, 4),
          },
        ],
      };
    }
  }
}
