import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { User } from '@src/business/users/entities/user.entity';

export class CreateUserRequestDto
  implements Omit<User, 'passwordHash' | 'id' | 'createdAt' | 'updatedAt' | 'isActive'>
{
  @IsEmail()
  @IsString()
  @ApiProperty({ description: 'user email' })
  public email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'user firstName' })
  public firstName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'user lastName' })
  public lastName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'user password' })
  public password: string;

  @IsOptional()
  @IsBoolean()
  @ApiPropertyOptional({ description: 'is user superadmin or not' })
  public isSuperAdmin: boolean;
}
