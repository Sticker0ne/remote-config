import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { User } from '@src/business/users/entities/user.entity';

export class UpdateUserRequestDto implements Partial<User> {
  @IsOptional()
  @IsEmail()
  @IsString()
  @ApiPropertyOptional({ description: 'user email' })
  public email?: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiPropertyOptional({ description: 'user lastName' })
  public lastName?: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiPropertyOptional({ description: 'user firstName' })
  public firstName?: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiPropertyOptional({ description: 'user password' })
  public password?: string;

  @IsOptional()
  @IsBoolean()
  @ApiPropertyOptional({ description: 'is user active or not' })
  public isActive!: boolean;

  @IsOptional()
  @IsBoolean()
  @ApiPropertyOptional({ description: 'is user superadmin or not' })
  public isSuperAdmin?: boolean;
}
