import { ApiProperty } from '@nestjs/swagger';
import { MapProp } from 'ts-simple-automapper';
import { User } from '@src/business/users/entities/user.entity';

export class UserResponseDto implements Omit<User, 'passwordHash'> {
  @MapProp()
  @ApiProperty({ description: 'user id' })
  public id: number;

  @MapProp()
  @ApiProperty({ description: 'user email' })
  public email: string;

  @MapProp()
  @ApiProperty({ description: 'user first name' })
  public firstName: string;

  @MapProp()
  @ApiProperty({ description: 'user first name' })
  public lastName: string;

  @MapProp()
  @ApiProperty({ description: 'is user superadmin or not' })
  public isSuperAdmin!: boolean;

  @MapProp()
  @ApiProperty({ description: 'is user active or not' })
  public isActive!: boolean;

  @MapProp()
  @ApiProperty({ description: 'when user was created' })
  public createdAt: Date;

  @MapProp()
  @ApiProperty({ description: 'when user was updated' })
  public updatedAt: Date;
}
