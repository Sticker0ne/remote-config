import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetAllUsersQueryDto {
  @Transform(({ value }) => value === 'true')
  @IsOptional()
  @IsBoolean()
  @ApiPropertyOptional({ description: 'include disabled users' })
  public includeDisabled?: boolean;
}
