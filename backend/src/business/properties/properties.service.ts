import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { PropertyEntity } from '@src/business/properties/entities/property.entity';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { IError, IErrorAble } from '@src/common/dto/error.types';
import { AppConstants } from '@src/utils/app-constants';
import { PropertyCreateDto } from '@src/business/properties/dto/property-create.dto';
import { PropertyUpdateDto } from '@src/business/properties/dto/property-update.dto';
import { validateDataTypeValue } from '@src/business/common/validators/data-types-validator';
import { CommonUtils } from '@src/utils/common.utils';

@Injectable()
export class PropertiesService {
  public constructor(
    @InjectRepository(PropertyEntity)
    private readonly repository: Repository<PropertyEntity>,
  ) {}

  public async findOneByCondition(
    condition: FindConditions<PropertyEntity>,
    withConfig = false,
  ): Promise<IErrorAble<PropertyEntity>> {
    const item = await this.repository.findOne({
      where: condition,
      relations: withConfig ? ['config'] : [],
    });

    if (item) return { payload: item, errors: [] };
    else
      return {
        errors: [{ code: HttpStatus.NOT_FOUND.toString(), message: 'Item was not found' }],
      };
  }

  public async findAllByCondition(
    condition: FindConditions<PropertyEntity>,
    withConfig = false,
  ): Promise<PropertyEntity[]> {
    const items = await this.repository.find({
      where: condition,
      relations: withConfig ? ['config'] : [],
    });

    return items;
  }

  public validateDataTypeValue(
    item: PropertyEntity | PropertyCreateDto,
  ): IErrorAble<PropertyEntity | PropertyCreateDto> {
    const stringifiedValueIsCorrect = validateDataTypeValue(
      item.dataType,
      CommonUtils.jsonSafeParse(item.stringifiedDefaultValue),
    );

    if (!stringifiedValueIsCorrect) {
      return {
        errors: [
          {
            code: HttpStatus.BAD_REQUEST.toString(),
            message: 'Stringified default value does not satisfy dataType constraint',
          },
        ],
      };
    }

    return { payload: item, errors: null };
  }

  public async validateNameUniqueness(
    item: PropertyEntity | PropertyCreateDto,
  ): Promise<IErrorAble<PropertyEntity | PropertyCreateDto>> {
    const itemConfigId = (item as PropertyEntity)?.config?.id;
    const condition = { config: { id: itemConfigId } };

    const itemsInSameScope = await this.findAllByCondition(condition);
    const namesInSameScope = itemsInSameScope.map((item) => item.name);
    const constraintError = namesInSameScope.includes(item.name);

    if (constraintError) {
      return {
        errors: [
          {
            code: HttpStatus.BAD_REQUEST.toString(),
            message: 'The name of extractor is not unique in scope',
          },
        ],
      };
    }

    return { payload: item };
  }

  public async create(createDto: PropertyCreateDto): Promise<IErrorAble<PropertyCreateDto>> {
    const normalizedDto = { ...createDto, config: { id: createDto.configId } };

    const { errors: dataTypeErrors } = this.validateDataTypeValue(normalizedDto);
    const { errors: nameUniqueErrors } = await this.validateNameUniqueness(normalizedDto);

    const resultErrors = [...(dataTypeErrors || []), ...(nameUniqueErrors || [])];
    if (resultErrors.length) return { errors: resultErrors };

    try {
      const item = await this.repository.save({ ...normalizedDto });
      return { payload: item, errors: [] };
    } catch (err) {
      const { databaseForeignKeyConstraintError, databaseUniqueKeyConstraintError } = AppConstants.errorCodes;
      const catchErrorsCodes = [databaseForeignKeyConstraintError, databaseUniqueKeyConstraintError];
      if (catchErrorsCodes.includes(err.code)) {
        const error: IError = { code: err.code, message: err.message || '', details: err.detail || '' };
        return { errors: [error] };
      }

      throw err;
    }
  }

  public async update(id: number, item: PropertyUpdateDto): Promise<IErrorAble<PropertyEntity>> {
    try {
      const { payload: foundItem, errors } = await this.findOneByCondition(
        {
          id,
        },
        true,
      );
      if (!foundItem) return { errors };

      const { errors: dataTypeErrors } = this.validateDataTypeValue({ ...foundItem, ...item });
      if (dataTypeErrors) return { errors: dataTypeErrors };

      const updateData = await this.repository.save({ ...foundItem, ...item });
      return { payload: updateData, errors: null };
    } catch (err) {
      return {
        errors: [
          {
            message: err.message,
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            details: JSON.stringify(err, null, 4),
          },
        ],
      };
    }
  }

  public async remove(id: number): Promise<IErrorAble<DeleteResult>> {
    try {
      const deleteResult = await this.repository.delete({ id });
      if (deleteResult.affected > 0) return { payload: deleteResult, errors: null };
      else
        return {
          errors: [
            {
              message: 'Item was not found',
              code: HttpStatus.NOT_FOUND.toString(),
            },
          ],
        };
    } catch (err) {
      return {
        errors: [
          {
            message: err.message,
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            details: JSON.stringify(err, null, 4),
          },
        ],
      };
    }
  }
}
