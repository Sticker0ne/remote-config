import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Res, Version } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { PropertiesService } from '@src/business/properties/properties.service';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { TypedHttpException } from '@src/common/typed-http-exception';
import { Mapper } from '@src/forked-packages/automapper/mapper2';
import { PropertyResponseDto } from '@src/business/properties/dto/property-response.dto';
import { PropertyCreateDto } from '@src/business/properties/dto/property-create.dto';
import { getCustomParseIntPipe } from '@src/common/custom-pipes';
import { PropertyUpdateDto } from '@src/business/properties/dto/property-update.dto';
import { ForSuperAdminOnly } from '@src/auth/decorators/super-admin-only.decorator';
import { Response } from 'express';

@Controller('properties')
@ApiTags('properties')
export class PropertiesController {
  constructor(private readonly service: PropertiesService) {}

  @ApiCreatedResponse({ type: PropertyResponseDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Post()
  async createV1(@Body() body: PropertyCreateDto): Promise<PropertyResponseDto> {
    const response = await this.service.create(body);

    if (!response.payload) {
      throw new TypedHttpException({ errors: response.errors }, HttpStatus.BAD_REQUEST);
    }

    return new Mapper().map(response.payload, new PropertyResponseDto());
  }

  @ApiOkResponse({ type: PropertyResponseDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get('/byConfigId/:configId')
  async getByConfigIdV1(
    @Param('configId', getCustomParseIntPipe()) configId: number,
  ): Promise<PropertyResponseDto[]> {
    const items = await this.service.findAllByCondition({ config: { id: configId } }, true);

    return new Mapper().mapList(items, PropertyResponseDto);
  }

  @ApiOkResponse({ type: [PropertyResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get()
  async getAllV1(): Promise<PropertyResponseDto[]> {
    const items = await this.service.findAllByCondition({}, true);

    return new Mapper().mapList(items, PropertyResponseDto);
  }

  @ApiOkResponse({ type: [PropertyResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @Version('1')
  @Patch(':id')
  async updateV1(
    @Body() body: PropertyUpdateDto,
    @Param('id', getCustomParseIntPipe()) id: number,
  ): Promise<PropertyResponseDto> {
    const { payload, errors } = await this.service.update(id, body);

    if (!payload) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    return new Mapper().map(payload, new PropertyResponseDto());
  }

  @ApiNoContentResponse()
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ForSuperAdminOnly()
  @Version('1')
  @Delete(':id')
  async removeV1(
    @Param('id', getCustomParseIntPipe()) id: number,
    @Res() response: Response,
  ): Promise<ErrorsDto | void> {
    const { errors } = await this.service.remove(id);

    if (errors?.length) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    response.status(HttpStatus.NO_CONTENT);
    response.send();
  }
}
