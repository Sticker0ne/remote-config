import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { PropertyEntity } from '@src/business/properties/entities/property.entity';

export class PropertyUpdateDto
  implements Omit<PropertyEntity, 'config' | 'createdAt' | 'updatedAt' | 'id' | 'name' | 'dataType'>
{
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'js code' })
  public code: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'description' })
  public description: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'extractor stringified default value' })
  public stringifiedDefaultValue: string;
}
