import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { MapFrom, MapProp } from 'ts-simple-automapper';
import { ApiProperty } from '@nestjs/swagger';
import { PropertyEntity } from '@src/business/properties/entities/property.entity';

export class PropertyResponseDto implements Omit<PropertyEntity, 'config'> {
  @MapProp()
  @ApiProperty({ description: 'id' })
  public id: number;

  @MapFrom(() => PropertyEntity, { mapFrom: (v) => v.config?.id })
  @ApiProperty({ description: 'configId' })
  public configId: number;

  @MapProp()
  @ApiProperty({ description: 'data type', enum: DataTypes, enumName: 'DataTypesEnum' })
  dataType: DataTypes;

  @MapProp()
  @ApiProperty({ description: 'name' })
  public name: string;

  @MapProp()
  @ApiProperty({ description: 'js code of property' })
  public code: string;

  @MapProp()
  @ApiProperty({ description: 'description' })
  public description: string;

  @MapProp()
  @ApiProperty({ description: 'stringified value' })
  public stringifiedDefaultValue: string;

  @MapProp()
  @ApiProperty({ description: 'createdAt' })
  public createdAt: Date;

  @MapProp()
  @ApiProperty({ description: 'updatedAt' })
  public updatedAt: Date;
}
