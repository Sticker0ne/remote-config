import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsAlpha, IsEnum, IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { PropertyEntity } from '@src/business/properties/entities/property.entity';

export class PropertyCreateDto implements Omit<PropertyEntity, 'config' | 'createdAt' | 'updatedAt' | 'id'> {
  @IsInt()
  @ApiProperty({ description: 'id of parent config' })
  public configId: number;

  @IsNotEmpty()
  @IsEnum(DataTypes)
  @ApiProperty({ description: 'data type', enum: DataTypes, enumName: 'DataTypesEnum' })
  dataType: DataTypes;

  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  @ApiProperty({ description: 'name' })
  public name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'js code' })
  public code: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'description' })
  public description: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'extractor stringified default value' })
  public stringifiedDefaultValue: string;
}
