import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { DataTypes } from '@src/business/common/enums/data-types.enum';

@Entity({ name: 'properties', orderBy: { id: 'DESC' } })
export class PropertyEntity {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @ManyToOne(() => ConfigEntity, { orphanedRowAction: 'delete', nullable: false, onDelete: 'CASCADE' })
  config?: ConfigEntity;

  @Column({
    type: 'enum',
    enum: DataTypes,
    default: DataTypes.STRING,
  })
  dataType: DataTypes;

  @Column()
  public name: string;

  @Column({ type: 'text' })
  public code: string;

  @Column()
  public description: string;

  @Column()
  public stringifiedDefaultValue: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}
