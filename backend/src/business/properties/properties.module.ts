import { Module } from '@nestjs/common';
import { PropertiesService } from './properties.service';
import { PropertiesController } from './properties.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PropertyEntity } from '@src/business/properties/entities/property.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PropertyEntity])],
  providers: [PropertiesService],
  controllers: [PropertiesController],
})
export class PropertiesModule {}
