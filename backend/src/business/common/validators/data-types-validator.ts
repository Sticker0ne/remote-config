import { DataTypes } from '@src/business/common/enums/data-types.enum';

type TValidator = (value: any) => boolean;
const validatePrimitive = (value: any, resultType: string): boolean => {
  return typeof value === resultType;
};

const validateArray = (value: any[], itemType: string): boolean => {
  if (!Array.isArray(value)) return false;
  return value.every((item) => validatePrimitive(item, itemType));
};

const dataTypeToValidatorMap: Record<DataTypes, TValidator> = {
  STRING: (value: any) => validatePrimitive(value, 'string'),
  NUMBER: (value: any) => validatePrimitive(value, 'number'),
  BOOLEAN: (value: any) => validatePrimitive(value, 'boolean'),
  ARRAY_OF_STRINGS: (value: any) => validateArray(value, 'string'),
  ARRAY_OF_NUMBERS: (value: any) => validateArray(value, 'number'),
  ARRAY_OF_BOOLEANS: (value: any) => validateArray(value, 'boolean'),
};

export function validateDataTypeValue(dataType: DataTypes, value: any): boolean {
  return dataTypeToValidatorMap[dataType](value);
}
