import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { IError, IErrorAble } from '@src/common/dto/error.types';
import { AppConstants } from '@src/utils/app-constants';
import { VariableEntity } from '@src/business/variables/entities/variable.entity';
import { VariableCreateRequestDto } from '@src/business/variables/dto/variable-create-request.dto';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { VariableUpdateRequestDto } from '@src/business/variables/dto/variable-update-request.dto';
import { validateDataTypeValue } from '@src/business/common/validators/data-types-validator';
import { CommonUtils } from '@src/utils/common.utils';

@Injectable()
export class VariablesService {
  public constructor(
    @InjectRepository(VariableEntity)
    private readonly repository: Repository<VariableEntity>,
  ) {}

  public async findOneByCondition(
    condition: FindConditions<VariableEntity>,
    withConfig = false,
  ): Promise<IErrorAble<VariableEntity>> {
    const options = withConfig ? { relations: ['config'] } : {};
    const item = await this.repository.findOne(condition, options);

    if (item) return { payload: item, errors: [] };
    else
      return {
        errors: [{ code: HttpStatus.NOT_FOUND.toString(), message: 'Variable not found' }],
      };
  }

  public async findAllByCondition(
    condition: FindConditions<VariableEntity>,
    withConfig = false,
  ): Promise<VariableEntity[]> {
    const items = await this.repository.find({
      where: condition,
      relations: withConfig ? ['config'] : [],
    });

    return items;
  }

  public validateDataTypeValue(
    variable: VariableEntity | VariableCreateRequestDto,
  ): IErrorAble<VariableEntity | VariableCreateRequestDto> {
    const stringifiedValueIsCorrect = validateDataTypeValue(
      variable.dataType,
      CommonUtils.jsonSafeParse(variable.stringifiedValue),
    );

    if (!stringifiedValueIsCorrect) {
      return {
        errors: [
          {
            code: HttpStatus.BAD_REQUEST.toString(),
            message: 'Stringified value does not satisfy dataType constraint',
          },
        ],
      };
    }

    return { payload: variable, errors: null };
  }

  public async validateNameUniqueness(
    variable: VariableEntity | VariableCreateRequestDto,
  ): Promise<IErrorAble<VariableEntity | VariableCreateRequestDto>> {
    const variableConfigId = (variable as VariableEntity)?.config?.id;
    const condition = variableConfigId ? { config: { id: variableConfigId } } : { config: null };

    const variablesInSameScope = await this.findAllByCondition(condition);
    const namesInSameScope = variablesInSameScope.map((item) => item.name);
    const constraintError = namesInSameScope.includes(variable.name);

    if (constraintError) {
      return {
        errors: [
          { code: HttpStatus.BAD_REQUEST.toString(), message: 'The name of variable is not unique in scope' },
        ],
      };
    }

    return { payload: variable };
  }

  public async create(createDto: VariableCreateRequestDto): Promise<IErrorAble<VariableEntity>> {
    const normalizedDto = { ...createDto, config: { id: createDto.configId } };

    const { errors: dataTypeErrors } = this.validateDataTypeValue(normalizedDto);
    const { errors: nameUniqueErrors } = await this.validateNameUniqueness(normalizedDto);

    const resultErrors = [...(dataTypeErrors || []), ...(nameUniqueErrors || [])];
    if (resultErrors.length) return { errors: resultErrors };

    try {
      const item = await this.repository.save({ ...normalizedDto });
      return { payload: item, errors: [] };
    } catch (err) {
      const { databaseForeignKeyConstraintError, databaseUniqueKeyConstraintError } = AppConstants.errorCodes;
      const catchErrorsCodes = [databaseForeignKeyConstraintError, databaseUniqueKeyConstraintError];
      if (catchErrorsCodes.includes(err.code)) {
        const error: IError = { code: err.code, message: err.message || '', details: err.detail || '' };
        return { errors: [error] };
      }

      throw err;
    }
  }

  public async update(id: number, item: VariableUpdateRequestDto): Promise<IErrorAble<VariableEntity>> {
    try {
      const { payload: foundItem, errors } = await this.findOneByCondition(
        {
          id,
        },
        true,
      );
      if (!foundItem) return { errors };

      const { errors: dataTypeErrors } = this.validateDataTypeValue({ ...foundItem, ...item });
      if (dataTypeErrors) return { errors: dataTypeErrors };

      const updateData = await this.repository.save({ ...foundItem, ...item });
      return { payload: updateData, errors: null };
    } catch (err) {
      return {
        errors: [
          {
            message: err.message,
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            details: JSON.stringify(err, null, 4),
          },
        ],
      };
    }
  }

  public async remove(id: number): Promise<IErrorAble<DeleteResult>> {
    try {
      const deleteResult = await this.repository.delete({ id });
      if (deleteResult.affected > 0) return { payload: deleteResult, errors: null };
      else
        return {
          errors: [
            {
              message: 'Item was not found',
              code: HttpStatus.NOT_FOUND.toString(),
            },
          ],
        };
    } catch (err) {
      return {
        errors: [
          {
            message: err.message,
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            details: JSON.stringify(err, null, 4),
          },
        ],
      };
    }
  }
}
