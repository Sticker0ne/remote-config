import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { VariableStatuses } from '@src/business/variables/entities/variable-status.enum';
import { MapFrom, MapProp } from 'ts-simple-automapper';
import { ApiProperty } from '@nestjs/swagger';
import { VariableEntity } from '@src/business/variables/entities/variable.entity';

export class VariableResponseDto implements Omit<VariableEntity, 'config'> {
  @MapProp()
  @ApiProperty({ description: 'variable id' })
  public id: number;

  @MapFrom(() => VariableEntity, { mapFrom: (v) => v.config?.id })
  @ApiProperty({ description: 'variable configId' })
  public configId: number;

  @MapProp()
  @ApiProperty({ description: 'variable data type', enum: DataTypes, enumName: 'DataTypesEnum' })
  dataType: DataTypes;

  @MapProp()
  @ApiProperty({ description: 'variable status', enum: Object.keys(VariableStatuses) })
  status: VariableStatuses;

  @MapProp()
  @ApiProperty({ description: 'variable name' })
  public name: string;

  @MapProp()
  @ApiProperty({ description: 'variable description' })
  public description: string;

  @MapProp()
  @ApiProperty({ description: 'variable stringified value' })
  public stringifiedValue: string;

  @MapProp()
  @ApiProperty({ description: 'variable createdAt' })
  public createdAt: Date;

  @MapProp()
  @ApiProperty({ description: 'variable updatedAt' })
  public updatedAt: Date;
}
