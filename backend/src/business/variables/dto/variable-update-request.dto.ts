import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { VariableEntity } from '@src/business/variables/entities/variable.entity';
import { VariableStatuses } from '@src/business/variables/entities/variable-status.enum';

export class VariableUpdateRequestDto implements Partial<VariableEntity> {
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiPropertyOptional({ description: 'config new description' })
  public description: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @ApiPropertyOptional({ description: 'config new stringified value' })
  public stringifiedValue: string;

  @IsOptional()
  @IsNotEmpty()
  @IsEnum(VariableStatuses)
  @ApiPropertyOptional({ enum: Object.keys(VariableStatuses), description: 'variable new status' })
  status: VariableStatuses;
}
