import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsAlpha, IsEnum, IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { VariableEntity } from '@src/business/variables/entities/variable.entity';
import { DataTypes } from '@src/business/common/enums/data-types.enum';

export class VariableCreateRequestDto
  implements Omit<VariableEntity, 'status' | 'id' | 'createdAt' | 'updatedAt' | 'config'>
{
  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  @ApiProperty({ description: 'variable name' })
  public name: string;

  @IsNotEmpty()
  @IsEnum(DataTypes)
  @ApiProperty({ enum: DataTypes, enumName: 'DataTypesEnum', description: 'variable data type' })
  public dataType: DataTypes;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'config description' })
  public description: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'stringified value of variable' })
  public stringifiedValue: string;

  @IsOptional()
  @IsInt()
  @ApiPropertyOptional({ description: 'config id' })
  public configId: number;
}
