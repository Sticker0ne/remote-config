import { Global, Module } from '@nestjs/common';
import { VariablesService } from './variables.service';
import { VariablesController } from './variables.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VariableEntity } from '@src/business/variables/entities/variable.entity';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([VariableEntity])],
  providers: [VariablesService],
  controllers: [VariablesController],
  exports: [VariablesService],
})
export class VariablesModule {}
