import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Res, Version } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { TypedHttpException } from '@src/common/typed-http-exception';
import { VariableResponseDto } from '@src/business/variables/dto/variable-response.dto';
import { VariablesService } from '@src/business/variables/variables.service';
import { VariableCreateRequestDto } from '@src/business/variables/dto/variable-create-request.dto';
import { getCustomParseIntPipe } from '@src/common/custom-pipes';
import { VariableUpdateRequestDto } from '@src/business/variables/dto/variable-update-request.dto';
import { Mapper } from '@src/forked-packages/automapper/mapper2';
import { ForSuperAdminOnly } from '@src/auth/decorators/super-admin-only.decorator';
import { Response } from 'express';

@Controller('variables')
@ApiTags('variables')
export class VariablesController {
  constructor(private readonly service: VariablesService) {}

  @ApiCreatedResponse({ type: VariableResponseDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Post()
  async createV1(@Body() body: VariableCreateRequestDto): Promise<VariableResponseDto> {
    const response = await this.service.create(body);

    if (!response.payload) {
      throw new TypedHttpException({ errors: response.errors }, HttpStatus.BAD_REQUEST);
    }

    return new Mapper().map(response.payload, new VariableResponseDto());
  }

  @ApiOkResponse({ type: [VariableResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get('/global')
  async getGlobalV1(): Promise<VariableResponseDto[]> {
    const items = await this.service.findAllByCondition({ config: null });

    return new Mapper().mapList(items, VariableResponseDto);
  }

  @ApiOkResponse({ type: VariableResponseDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get('/byConfigId/:configId')
  async getByConfigIdV1(
    @Param('configId', getCustomParseIntPipe()) configId: number,
  ): Promise<VariableResponseDto[]> {
    const items = await this.service.findAllByCondition({ config: { id: configId } }, true);

    return new Mapper().mapList(items, VariableResponseDto);
  }

  @ApiOkResponse({ type: [VariableResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get()
  async getAllV1(): Promise<VariableResponseDto[]> {
    const items = await this.service.findAllByCondition({}, true);

    return new Mapper().mapList(items, VariableResponseDto);
  }

  @ApiOkResponse({ type: [VariableResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @Version('1')
  @Patch(':id')
  async updateV1(
    @Body() body: VariableUpdateRequestDto,
    @Param('id', getCustomParseIntPipe()) id: number,
  ): Promise<VariableResponseDto> {
    const { payload, errors } = await this.service.update(id, body);

    if (!payload) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    return new Mapper().map(payload, new VariableResponseDto());
  }

  @ApiNoContentResponse()
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ForSuperAdminOnly()
  @Version('1')
  @Delete(':id')
  async removeV1(
    @Param('id', getCustomParseIntPipe()) id: number,
    @Res() response: Response,
  ): Promise<ErrorsDto | void> {
    const { errors } = await this.service.remove(id);

    if (errors?.length) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    response.status(HttpStatus.NO_CONTENT);
    response.send();
  }
}
