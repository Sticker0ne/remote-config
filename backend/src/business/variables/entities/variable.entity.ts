import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { VariableStatuses } from '@src/business/variables/entities/variable-status.enum';

@Entity({ name: 'variables', orderBy: { id: 'DESC' } })
export class VariableEntity {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @ManyToOne(() => ConfigEntity, { orphanedRowAction: 'delete', nullable: true, onDelete: 'CASCADE' })
  config?: ConfigEntity;

  @Column({
    type: 'enum',
    enum: DataTypes,
    default: DataTypes.STRING,
  })
  dataType: DataTypes;

  @Column({
    type: 'enum',
    enum: VariableStatuses,
    default: VariableStatuses.DEFAULT,
  })
  status: VariableStatuses;

  @Column()
  public name: string;

  @Column()
  public description: string;

  @Column()
  public stringifiedValue: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}
