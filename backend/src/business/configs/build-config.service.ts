import { HttpStatus, Injectable } from '@nestjs/common';
import { ConfigsService } from '@src/business/configs/configs.service';
import { IRequestContext } from '@src/business/configs/types/request-context.types';
import { IError, IErrorAble } from '@src/common/dto/error.types';
import { CommonUtils } from '@src/utils/common.utils';
import { VariablesService } from '@src/business/variables/variables.service';
import { VariableEntity } from '@src/business/variables/entities/variable.entity';
import * as ivm from 'isolated-vm';
import { AppConfigService } from '@src/config/app-config.service';
import {
  extractDataFromProperties,
  extractDataFromRequest,
} from '@src/business/configs/utils/data-exctractor.utils';
import { validateDataTypeValue } from '@src/business/common/validators/data-types-validator';

@Injectable()
export class BuildConfigService {
  public constructor(
    private readonly configsService: ConfigsService,
    private readonly variablesService: VariablesService,
    private readonly appConfig: AppConfigService,
  ) {}

  public extractDataFromVariableCollection(collection: VariableEntity[]): IErrorAble<Record<string, any>> {
    const variablesData: Record<string, any> = {};
    const errors: IError[] = [];

    collection.forEach((variable) => {
      const { stringifiedValue, name, dataType } = variable;
      variablesData[name] = CommonUtils.jsonSafeParse(stringifiedValue);

      const valueHasCorrectDataType = validateDataTypeValue(
        dataType,
        CommonUtils.jsonSafeParse(stringifiedValue),
      );
      if (!valueHasCorrectDataType) {
        errors.push({
          code: HttpStatus.UNPROCESSABLE_ENTITY.toString(),
          message: `Variable "${name}" contains incorrect value in database, please set new value via application`,
        });
      }
    });

    const hasErrors = errors.length > 0;
    return hasErrors ? { errors, payload: variablesData } : { payload: variablesData };
  }

  public async extractDataFromGlobalVariables(): Promise<IErrorAble<Record<string, any>>> {
    const variables = await this.variablesService.findAllByCondition({ config: null });
    return this.extractDataFromVariableCollection(variables);
  }

  public async buildConfigResult(name: string, context: IRequestContext): Promise<any> {
    const { errors: findErrors, payload: config } = await this.configsService.findOneByCondition(
      { name },
      true,
    );
    if (!config) return findErrors;

    const { errors: extractErrors, payload: requestData } = extractDataFromRequest(
      context,
      config.extractors || [],
    );

    const { errors: globalVariablesErrors, payload: globalVariablesData } =
      await this.extractDataFromGlobalVariables();

    const { errors: configVariablesErrors, payload: configVariablesData } =
      await this.extractDataFromVariableCollection(config.variables);

    const codeExecutionContext = {
      requestData: requestData || {},
      globalVariables: globalVariablesData || {},
      configVariables: configVariablesData || {},
    };

    const { errors: propertiesErrors, payload: resultData } = extractDataFromProperties(
      codeExecutionContext,
      config.properties,
      new ivm.Isolate({ memoryLimit: 128 }),
      this.appConfig.envConfig.propertyExecutionTimeout,
      this.appConfig.envConfig.disableIsolation,
    );

    return {
      resultData,
      extractedRequestData: requestData,
      extractWarnings: extractErrors,
      globalVariablesData,
      globalVariablesErrors,
      configVariablesData,
      configVariablesErrors,
      propertiesErrors,
    };
  }
}
