import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { ExtractWays } from '@src/business/extractors/entities/extract-way.enum';
import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { ExtractorEntity } from '@src/business/extractors/entities/extractor.entity';
import { PropertyEntity } from '@src/business/properties/entities/property.entity';
import { arrayLikeDataTypes } from '@src/business/configs/utils/data-exctractor.utils';

const extractWayToInMap: Record<ExtractWays, string> = {
  BODY: 'body',
  COOKIE: 'cookie',
  QUERY: 'query',
};

const dataTypeToTypeMap: Record<DataTypes, string> = {
  STRING: 'string',
  NUMBER: 'number',
  BOOLEAN: 'boolean',
  ARRAY_OF_BOOLEANS: 'boolean',
  ARRAY_OF_NUMBERS: 'number',
  ARRAY_OF_STRINGS: 'string',
};

function buildSchemaFromExtractor(entity: ExtractorEntity | PropertyEntity): Record<string, any> {
  const res: Record<string, any> = {
    type: dataTypeToTypeMap[entity.dataType],
  };

  if (arrayLikeDataTypes.includes(entity.dataType)) {
    res.items = {
      type: res.type,
    };
    res.type = 'array';
  }

  return res;
}

function buildParameterFromExtractor(extractor: ExtractorEntity): Record<string, any> {
  const res: Record<string, any> = {
    name: extractor.extractName,
    required: false,
    in: extractWayToInMap[extractor.extractWay],
    description: extractor.description,
  };

  res.schema = buildSchemaFromExtractor(extractor);
  return res;
}

function buildProperty(entity: ExtractorEntity | PropertyEntity): Record<string, any> {
  const schema = buildSchemaFromExtractor(entity);
  return {
    ...schema,
    description: entity.description,
  };
}

function buildProperties(entities: (PropertyEntity | ExtractorEntity)[]): Record<string, any> {
  return entities.reduce((acc: Record<string, any>, entity) => {
    return { ...acc, [entity.name]: buildProperty(entity) };
  }, {});
}

export function buildOpenApiSchema({
  config,
  pathPrefix,
}: {
  config: ConfigEntity;
  pathPrefix: string;
}): Record<string, any> {
  const bodyExtractors = config.extractors.filter((extractor) => extractor.extractWay === ExtractWays.BODY);
  const notBodyExtractors = config.extractors.filter((extr) => extr.extractWay !== ExtractWays.BODY);

  const parameters = notBodyExtractors.map(buildParameterFromExtractor);
  const reqProperties = buildProperties(bodyExtractors);
  const resProperties = buildProperties(config.properties);
  const resPropertiesRequiredNames = config.properties.map((property) => property.name);

  const requestBodySchema = {
    type: 'object',
    properties: reqProperties,
  };

  const responseSchema = {
    type: 'object',
    properties: resProperties,
    required: resPropertiesRequiredNames,
  };

  const schema = {
    openapi: '3.0.0',
    paths: {
      [`${pathPrefix}/${config.name}`]: {
        post: {
          operationId: 'getConfig',
          summary: 'Get config',
          deprecated: false,
          parameters,
          requestBody: {
            required: false,
            content: {
              'application/json': {
                schema: {
                  $ref: `#/components/schemas/RemoteConfigRequestBody`,
                },
              },
            },
          },
          responses: {
            default: {
              description: 'config data',
              content: {
                'application/json': {
                  schema: {
                    $ref: `#/components/schemas/RemoteConfigResponse`,
                  },
                },
              },
            },
          },
          tags: [],
        },
      },
    },
    info: {
      title: `Getting ${config.name} data`,
      description: `Description of ${config.name}: ${config.description}`,
      version: '3.0.0',
      contact: {},
    },
    tags: [],
    servers: [],
    components: {
      schemas: {
        RemoteConfigRequestBody: requestBodySchema,
        RemoteConfigResponse: responseSchema,
      },
    },
  };

  return schema;
}
