import { ICodeExecutionContext } from '@src/business/configs/types/code-execution-context.types';
import { PropertyEntity } from '@src/business/properties/entities/property.entity';
import { IError, IErrorAble } from '@src/common/dto/error.types';
import { Context, Isolate } from 'isolated-vm';
import { FIRST_LINE_SPREAD_CODE } from '@src/business/configs/constants/code.const';
import { HttpStatus } from '@nestjs/common';
import { dataTypeToDefaultValueMap } from '@src/business/configs/constants/data.const';
import { IRequestContext } from '@src/business/configs/types/request-context.types';
import { ExtractorEntity } from '@src/business/extractors/entities/extractor.entity';
import { ExtractWays } from '@src/business/extractors/entities/extract-way.enum';
import { CommonUtils } from '@src/utils/common.utils';
import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { validateDataTypeValue } from '@src/business/common/validators/data-types-validator';

export const arrayLikeDataTypes = [
  DataTypes.ARRAY_OF_BOOLEANS,
  DataTypes.ARRAY_OF_NUMBERS,
  DataTypes.ARRAY_OF_STRINGS,
];

export function extractDataFromProperties(
  codeExecutionContext: ICodeExecutionContext,
  properties: PropertyEntity[],
  isolate: Isolate,
  timeout: number,
  disableIsolation?: boolean,
  context?: Context,
): IErrorAble<Record<string, any>> {
  const resultData: Record<string, any> = {};
  const resultErrors: IError[] = [];

  const normalizedContext = context || isolate.createContextSync();
  const argumentsArray = [codeExecutionContext];
  const evalOptions = {
    timeout,
    arguments: { copy: true },
    result: { copy: true },
  };

  properties.forEach((property) => {
    let codeError: IError | null = null;
    let result: any = null;
    try {
      const patchedCode = FIRST_LINE_SPREAD_CODE + property.code;

      if (disableIsolation) {
        result = Function('$0', patchedCode)(codeExecutionContext);
      }

      if (!disableIsolation) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        result = normalizedContext.evalClosureSync(patchedCode, argumentsArray, evalOptions);
      }
    } catch (err) {
      codeError = {
        code: HttpStatus.UNPROCESSABLE_ENTITY.toString(),
        message: `Errors during property "${property.name}" code execution`,
        details: JSON.stringify(err, null, 4),
      };
      resultErrors.push(codeError);
    }
    const resultHasCorrectDataType = validateDataTypeValue(property.dataType, result);

    if (resultHasCorrectDataType) {
      resultData[property.name] = result;
      return;
    }

    const resultTypeError = {
      code: HttpStatus.UNPROCESSABLE_ENTITY.toString(),
      message: `Code execution of property "${property.name}" return data does not satisfy dataType constraint, default value used instead`,
    };

    resultErrors.push(resultTypeError);

    const { stringifiedDefaultValue } = property;
    const defValueHasCorrectDataType = validateDataTypeValue(
      property.dataType,
      CommonUtils.jsonSafeParse(stringifiedDefaultValue),
    );

    if (defValueHasCorrectDataType) {
      resultData[property.name] = JSON.parse(stringifiedDefaultValue);
      return;
    }

    const defValueError = {
      code: HttpStatus.UNPROCESSABLE_ENTITY.toString(),
      message: `Property "${property.name}" contains incorrect default value in database, please set new value via application. Suitable value for data type used instead`,
    };

    resultErrors.push(defValueError);

    resultData[property.name] = dataTypeToDefaultValueMap[property.dataType];
  });

  return { errors: resultErrors, payload: resultData };
}

const extractWayToRequestKeyMap: Record<ExtractWays, keyof IRequestContext> = {
  BODY: 'body',
  COOKIE: 'cookie',
  QUERY: 'query',
};

export function extractDataFromRequest(
  context: IRequestContext,
  extractors: ExtractorEntity[],
): IErrorAble<Record<string, any>> {
  const requestData: Record<string, any> = {};
  const requestExtractorErrors: IError[] = [];

  context.cookie = context.cookie || {};
  context.body = context.body || {};
  context.query = context.query || {};

  extractors.forEach((extractor) => {
    const extractKey = extractWayToRequestKeyMap[extractor.extractWay];
    const providerObject = context[extractKey];
    const resultKeyName = extractor.name;
    const defaultExtractorValue = CommonUtils.jsonSafeParse(extractor.stringifiedDefaultValue);
    const dataTypeIsArrayLike = arrayLikeDataTypes.includes(extractor.dataType);

    try {
      const providedValue = providerObject[extractor.extractName];
      let resultValue = CommonUtils.jsonClone(defaultExtractorValue);
      let validType = false;

      // Value is empty
      if (providedValue === undefined) {
        requestExtractorErrors.push({
          code: HttpStatus.BAD_REQUEST.toString(),
          message: 'Extractor error',
          details: `Empty variable "${extractor.extractName}" in request "${extractor.extractWay}" should be "${extractor.dataType}".\n default value: ${defaultExtractorValue} used instead`,
        });

        requestData[resultKeyName] = defaultExtractorValue;
        return;
      }

      // Extract value from body
      if (extractor.extractWay === ExtractWays.BODY) {
        validType = validateDataTypeValue(extractor.dataType, providedValue);
        if (validType) resultValue = providedValue;
      }

      // Extract value from query (all queries are string or string[])
      if (extractor.extractWay === ExtractWays.QUERY) {
        let localProvidedValue = CommonUtils.jsonClone(providedValue);
        const localProvidedValueIsArray = Array.isArray(localProvidedValue);

        // normalize by array
        if (localProvidedValueIsArray && !dataTypeIsArrayLike)
          localProvidedValue = localProvidedValue[localProvidedValue.length - 1];
        else if (dataTypeIsArrayLike && !localProvidedValueIsArray) localProvidedValue = [localProvidedValue];

        // if data type is string or string[] return it without parsing. (all queries are string or string[])
        if (extractor.dataType === DataTypes.STRING || extractor.dataType === DataTypes.ARRAY_OF_STRINGS) {
          validType = true;
          resultValue = localProvidedValue;
        } else {
          const parsedLocalValue = Array.isArray(localProvidedValue)
            ? CommonUtils.jsonSafeParseArray(localProvidedValue)
            : CommonUtils.jsonSafeParse(localProvidedValue);

          validType = validateDataTypeValue(extractor.dataType, parsedLocalValue);
          if (validType) resultValue = parsedLocalValue;
        }
      }

      // Extract value from cookie (all cookies are string)
      if (extractor.extractWay === ExtractWays.COOKIE) {
        let localProvidedValue = CommonUtils.jsonClone(providedValue).toString();
        //cookie can be sent as array by dividing by ','
        localProvidedValue = localProvidedValue.split(',');

        const localProvidedValueIsArray = Array.isArray(localProvidedValue);

        // normalize by array
        if (localProvidedValueIsArray && !dataTypeIsArrayLike)
          localProvidedValue = localProvidedValue[localProvidedValue.length - 1];
        else if (dataTypeIsArrayLike && !localProvidedValueIsArray) localProvidedValue = [localProvidedValue];

        // if data type is string or string[] return it without parsing. (now localProvidedValue can be string or string[])
        if (extractor.dataType === DataTypes.STRING || extractor.dataType === DataTypes.ARRAY_OF_STRINGS) {
          validType = true;
          resultValue = localProvidedValue;
        } else {
          const parsedLocalValue = Array.isArray(localProvidedValue)
            ? CommonUtils.jsonSafeParseArray(localProvidedValue)
            : CommonUtils.jsonSafeParse(localProvidedValue);

          validType = validateDataTypeValue(extractor.dataType, parsedLocalValue);
          if (validType) resultValue = parsedLocalValue;
        }
      }

      if (!validType)
        requestExtractorErrors.push({
          code: HttpStatus.BAD_REQUEST.toString(),
          message: 'Extractor error',
          details: `Incorrect type of variable "${extractor.extractName}" in request "${extractor.extractWay}" with value "${providedValue}" should be "${extractor.dataType}".\n default value: ${defaultExtractorValue} used instead`,
        });

      requestData[resultKeyName] = resultValue;
    } catch (e) {
      requestExtractorErrors.push({
        code: HttpStatus.BAD_REQUEST.toString(),
        message: `Unhandled error when extract data from request for extractor "${extractor.extractName}"`,
        details: JSON.stringify(e, null, 4),
      });
      requestData[resultKeyName] = defaultExtractorValue;
    }
  });

  const hasErrors = requestExtractorErrors.length > 0;
  return hasErrors ? { errors: requestExtractorErrors, payload: requestData } : { payload: requestData };
}
