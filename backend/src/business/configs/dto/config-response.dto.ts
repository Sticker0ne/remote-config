import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { MapProp } from 'ts-simple-automapper';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { ConfigStatuses } from '@src/business/configs/entities/config-status.enum';
import { VariableResponseDto } from '@src/business/variables/dto/variable-response.dto';
import { ExtractorResponseDto } from '@src/business/extractors/dto/extractor-response.dto';
import { PropertyResponseDto } from '@src/business/properties/dto/property-response.dto';

type TConfigResponseDto = Omit<ConfigEntity, 'variables' | 'properties' | 'extractors'> &
  Partial<ConfigEntity>;
export class ConfigResponseDto implements TConfigResponseDto {
  @MapProp()
  @ApiProperty({ description: 'config id' })
  public id: number;

  @MapProp()
  @ApiProperty({ description: 'config name' })
  public name: string;

  @MapProp()
  @ApiProperty({ enum: Object.keys(ConfigStatuses), description: 'config status' })
  public status: ConfigStatuses;

  @MapProp()
  @ApiProperty({ description: 'config description' })
  public description: string;

  @MapProp()
  @ApiProperty({ description: 'when user was created' })
  public createdAt: Date;

  @MapProp()
  @ApiProperty({ description: 'when user was updated' })
  public updatedAt: Date;

  @MapProp()
  @ApiPropertyOptional({ type: [VariableResponseDto], description: 'config variables' })
  public variables?: VariableResponseDto[];

  @MapProp()
  @ApiPropertyOptional({ type: [ExtractorResponseDto], description: 'config extractors' })
  public extractors?: ExtractorResponseDto[];

  @MapProp()
  @ApiPropertyOptional({ type: [PropertyResponseDto], description: 'config properties' })
  public properties?: PropertyResponseDto[];
}
