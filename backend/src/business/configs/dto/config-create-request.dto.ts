import { ApiProperty } from '@nestjs/swagger';
import { IsAlpha, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';

export class ConfigCreateRequestDto
  implements
    Omit<
      ConfigEntity,
      'status' | 'id' | 'createdAt' | 'updatedAt' | 'variables' | 'properties' | 'extractors'
    >
{
  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  @ApiProperty({ description: 'config name' })
  public name: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'config description' })
  public description: string;
}
