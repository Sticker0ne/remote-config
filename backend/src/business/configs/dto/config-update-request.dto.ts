import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { ConfigStatuses } from '@src/business/configs/entities/config-status.enum';

export class ConfigUpdateRequestDto
  implements
    Omit<ConfigEntity, 'createdAt' | 'updatedAt' | 'name' | 'id' | 'variables' | 'extractors' | 'properties'>
{
  @IsOptional()
  @IsNotEmpty()
  @IsEnum(ConfigStatuses)
  @ApiProperty({ enum: Object.keys(ConfigStatuses), description: 'config new status' })
  public status: ConfigStatuses;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'config new description' })
  public description: string;
}
