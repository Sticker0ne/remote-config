import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetSwaggerQueryDto {
  @Transform(({ value }) => value.toString())
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ description: 'prefix for path description in schema' })
  public pathPrefix?: string;
}
