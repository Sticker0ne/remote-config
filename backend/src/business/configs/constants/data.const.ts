import { DataTypes } from '@src/business/common/enums/data-types.enum';

export const dataTypeToDefaultValueMap: Record<DataTypes, any> = {
  STRING: '',
  BOOLEAN: false,
  NUMBER: -1,
  ARRAY_OF_NUMBERS: [],
  ARRAY_OF_STRINGS: [],
  ARRAY_OF_BOOLEANS: [],
};
