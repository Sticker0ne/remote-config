import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { VariableEntity } from '@src/business/variables/entities/variable.entity';
import { ConfigStatuses } from '@src/business/configs/entities/config-status.enum';
import { ExtractorEntity } from '@src/business/extractors/entities/extractor.entity';
import { PropertyEntity } from '@src/business/properties/entities/property.entity';

@Entity({ name: 'configs', orderBy: { id: 'DESC' } })
export class ConfigEntity {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ unique: true })
  public name: string;

  @Column({
    type: 'enum',
    enum: ConfigStatuses,
    default: ConfigStatuses.DEFAULT,
  })
  status: ConfigStatuses;

  @OneToMany(() => VariableEntity, (variable) => variable.config)
  variables?: VariableEntity[];

  @OneToMany(() => ExtractorEntity, (entity) => entity.config)
  extractors?: ExtractorEntity[];

  @OneToMany(() => PropertyEntity, (entity) => entity.config)
  properties?: PropertyEntity[];

  // eslint-disable-next-line
  @Column({ default: `` })
  public description: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}
