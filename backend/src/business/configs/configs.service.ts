import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AppConfigService } from '@src/config/app-config.service';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { IError, IErrorAble } from '@src/common/dto/error.types';
import { AppConstants } from '@src/utils/app-constants';
import { ConfigCreateRequestDto } from '@src/business/configs/dto/config-create-request.dto';
import { ConfigUpdateRequestDto } from '@src/business/configs/dto/config-update-request.dto';

@Injectable()
export class ConfigsService {
  public constructor(
    @InjectRepository(ConfigEntity)
    private readonly repository: Repository<ConfigEntity>,
    private readonly appConfigService: AppConfigService,
  ) {}

  public async findOneByCondition(
    condition: Partial<ConfigEntity>,
    withRelations = false,
  ): Promise<IErrorAble<ConfigEntity>> {
    const relations = withRelations ? ['properties', 'variables', 'extractors'] : [];
    const item = await this.repository.findOne(condition, { relations });

    if (item) return { payload: item, errors: [] };
    else
      return {
        errors: [{ code: HttpStatus.NOT_FOUND.toString(), message: 'Config not found' }],
      };
  }

  public async findAllByCondition(condition: Partial<ConfigEntity>): Promise<ConfigEntity[]> {
    const items = await this.repository.find(condition);

    return items;
  }

  public async getAllWithRelations(): Promise<ConfigEntity[]> {
    const items = await this.repository.find({ relations: ['properties', 'variables', 'extractors'] });

    return items;
  }

  public async create(createDto: ConfigCreateRequestDto): Promise<IErrorAble<ConfigEntity>> {
    try {
      const item = await this.repository.save({ ...createDto });
      return { payload: item, errors: [] };
    } catch (err) {
      if (err.code === AppConstants.errorCodes.databaseUniqueKeyConstraintError) {
        const error: IError = { code: err.code, message: err.message || '', details: err.detail || '' };
        return { errors: [error] };
      }

      throw err;
    }
  }

  public async update(id: number, item: ConfigUpdateRequestDto): Promise<IErrorAble<ConfigEntity>> {
    try {
      const { payload: foundItem, errors } = await this.findOneByCondition({
        id,
      });
      if (!foundItem) return { errors };
      const updateData = await this.repository.save({ ...foundItem, ...item });
      return { payload: updateData, errors: null };
    } catch (err) {
      return {
        errors: [
          {
            message: err.message,
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            details: JSON.stringify(err, null, 4),
          },
        ],
      };
    }
  }

  public async remove(id: number): Promise<IErrorAble<any>> {
    try {
      const deleteResult = await this.repository.delete({ id });
      if (deleteResult.affected > 0) return { payload: deleteResult, errors: null };
      else
        return {
          errors: [
            {
              message: 'Item was not found',
              code: HttpStatus.NOT_FOUND.toString(),
            },
          ],
        };
    } catch (err) {
      return {
        errors: [
          {
            message: err.message,
            code: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
            details: JSON.stringify(err, null, 4),
          },
        ],
      };
    }
  }
}
