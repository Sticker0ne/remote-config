import { All, Controller, Param, Req, Version } from '@nestjs/common';
import { ApiBadRequestResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { ConfigResponseDto } from '@src/business/configs/dto/config-response.dto';
import { BuildConfigService } from '@src/business/configs/build-config.service';
import type { Request } from 'express';
import { Public } from '@src/auth/decorators/public.decorator';

@Controller('configs')
@ApiTags('configs')
export class BuildConfigController {
  constructor(private readonly service: BuildConfigService) {}

  @ApiOkResponse({ type: ConfigResponseDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @All('/result/:name')
  @Public()
  async buildV1(@Param('name') name: string, @Req() request: Request): Promise<ConfigResponseDto> {
    const body = request.body || {};
    const query = request.query || {};
    const cookie = request.cookies || {};

    return this.service.buildConfigResult(name, { body, query, cookie });
  }
}
