import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Res, Version } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { TypedHttpException } from '@src/common/typed-http-exception';
import { Mapper } from 'ts-simple-automapper';
import { ConfigsService } from '@src/business/configs/configs.service';
import { ConfigResponseDto } from '@src/business/configs/dto/config-response.dto';
import { ConfigCreateRequestDto } from '@src/business/configs/dto/config-create-request.dto';
import { ConfigUpdateRequestDto } from '@src/business/configs/dto/config-update-request.dto';
import { getCustomParseIntPipe } from '@src/common/custom-pipes';
import { ForSuperAdminOnly } from '@src/auth/decorators/super-admin-only.decorator';
import { Response } from 'express';

@Controller('configs')
@ApiTags('configs')
export class ConfigsController {
  constructor(private readonly service: ConfigsService) {}

  @ApiCreatedResponse({ type: ConfigResponseDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Post()
  async createV1(@Body() body: ConfigCreateRequestDto): Promise<ConfigResponseDto> {
    const response = await this.service.create(body);

    if (!response.payload) {
      throw new TypedHttpException({ errors: response.errors }, HttpStatus.BAD_REQUEST);
    }

    return new Mapper().map(response.payload, new ConfigResponseDto());
  }

  @ApiOkResponse({ type: [ConfigResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get()
  async getAllV1(): Promise<ConfigResponseDto[]> {
    const items = await this.service.findAllByCondition({});

    return new Mapper().mapList(items, ConfigResponseDto);
  }

  @ApiOkResponse({ type: ConfigResponseDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get(':id')
  async getV1(@Param('id', getCustomParseIntPipe()) id: number): Promise<ConfigResponseDto> {
    const response = await this.service.findOneByCondition({ id }, true);

    if (!response.payload) {
      throw new TypedHttpException({ errors: response.errors }, HttpStatus.NOT_FOUND);
    }

    return new Mapper().map(response.payload, new ConfigResponseDto());
  }

  @ApiOkResponse({ type: [ConfigResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @Version('1')
  @Patch(':id')
  async updateV1(
    @Body() body: ConfigUpdateRequestDto,
    @Param('id', getCustomParseIntPipe()) id: number,
  ): Promise<ConfigResponseDto> {
    const { payload, errors } = await this.service.update(id, body);

    if (!payload) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    return new Mapper().map(payload, new ConfigResponseDto());
  }

  @ApiNoContentResponse()
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ForSuperAdminOnly()
  @Version('1')
  @Delete(':id')
  async removeV1(
    @Param('id', getCustomParseIntPipe()) id: number,
    @Res() response: Response,
  ): Promise<ErrorsDto | void> {
    const { payload, errors } = await this.service.remove(id);

    if (!payload) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    response.status(HttpStatus.NO_CONTENT);
    response.send();
  }
}
