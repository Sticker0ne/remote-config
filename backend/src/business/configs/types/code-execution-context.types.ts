export interface ICodeExecutionContext {
  globalVariables: Record<string, any>;
  configVariables: Record<string, any>;
  requestData: Record<string, any>;
}
