export interface IRequestContext {
  cookie: Record<string, any>;
  body: Record<string, any>;
  query: Record<string, any>;
}
