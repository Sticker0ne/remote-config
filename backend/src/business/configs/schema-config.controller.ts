import { Controller, Get, Param, Query, Version } from '@nestjs/common';
import { ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { Public } from '@src/auth/decorators/public.decorator';
import { ConfigsService } from '@src/business/configs/configs.service';
import { buildOpenApiSchema } from '@src/business/configs/utils/openapi-schema-build.utils';
import { GetSwaggerQueryDto } from '@src/business/configs/dto/get-swagger-query.dto';

@Controller('configs')
@ApiTags('configs')
export class SchemaConfigController {
  constructor(private readonly service: ConfigsService) {}

  @ApiOkResponse({ type: Object })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @Version('1')
  @Get('/schema/:name')
  @Public()
  async buildV1(
    @Param('name') name: string,
    @Query() query: GetSwaggerQueryDto,
  ): Promise<Record<string, any>> {
    const { errors, payload } = await this.service.findOneByCondition({ name }, true);
    if (!payload) return errors[0];

    return buildOpenApiSchema({ config: payload, pathPrefix: query.pathPrefix || '' });
  }
}
