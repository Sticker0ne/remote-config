import { Global, Module } from '@nestjs/common';
import { ConfigsController } from './configs.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { ConfigsService } from '@src/business/configs/configs.service';
import { BuildConfigService } from '@src/business/configs/build-config.service';
import { BuildConfigController } from '@src/business/configs/build-config.controller';
import { VariablesModule } from '@src/business/variables/variables.module';
import { SchemaConfigController } from './schema-config.controller';

@Global()
@Module({
  providers: [ConfigsService, BuildConfigService],
  imports: [TypeOrmModule.forFeature([ConfigEntity]), VariablesModule],
  exports: [ConfigsService, BuildConfigService],
  controllers: [ConfigsController, BuildConfigController, SchemaConfigController],
})
export class ConfigsModule {}
