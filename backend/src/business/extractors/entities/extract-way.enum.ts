export enum ExtractWays {
  COOKIE = 'COOKIE',
  BODY = 'BODY',
  QUERY = 'QUERY',
}
