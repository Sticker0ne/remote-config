import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ConfigEntity } from '@src/business/configs/entities/config.entity';
import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { ExtractWays } from '@src/business/extractors/entities/extract-way.enum';

@Entity({ name: 'extractors', orderBy: { id: 'DESC' } })
export class ExtractorEntity {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @ManyToOne(() => ConfigEntity, { orphanedRowAction: 'delete', nullable: false, onDelete: 'CASCADE' })
  config?: ConfigEntity;

  @Column({
    type: 'enum',
    enum: DataTypes,
    default: DataTypes.STRING,
  })
  dataType: DataTypes;

  @Column({
    type: 'enum',
    enum: ExtractWays,
    default: ExtractWays.COOKIE,
  })
  extractWay: ExtractWays;

  @Column()
  public extractName: string;

  @Column()
  public name: string;

  @Column()
  public description: string;

  @Column()
  public stringifiedDefaultValue: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}
