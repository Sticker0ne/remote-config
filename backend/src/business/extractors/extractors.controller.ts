import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Res, Version } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ExtractorsService } from '@src/business/extractors/extractors.service';
import { ErrorsDto } from '@src/common/dto/errors.dto';
import { TypedHttpException } from '@src/common/typed-http-exception';
import { Mapper } from '@src/forked-packages/automapper/mapper2';
import { ExtractorResponseDto } from '@src/business/extractors/dto/extractor-response.dto';
import { ExtractorCreateDto } from '@src/business/extractors/dto/extractor-create.dto';
import { getCustomParseIntPipe } from '@src/common/custom-pipes';
import { ForSuperAdminOnly } from '@src/auth/decorators/super-admin-only.decorator';
import { Response } from 'express';
import { ExtractorUpdateDto } from '@src/business/extractors/dto/extractor-update.dto';

@Controller('extractors')
@ApiTags('extractors')
export class ExtractorsController {
  constructor(private readonly service: ExtractorsService) {}

  @ApiCreatedResponse({ type: ExtractorResponseDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Post()
  async createV1(@Body() body: ExtractorCreateDto): Promise<ExtractorResponseDto> {
    const response = await this.service.create(body);

    if (!response.payload) {
      throw new TypedHttpException({ errors: response.errors }, HttpStatus.BAD_REQUEST);
    }

    return new Mapper().map(response.payload, new ExtractorResponseDto());
  }

  @ApiOkResponse({ type: ExtractorResponseDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get('/byConfigId/:configId')
  async getByConfigIdV1(
    @Param('configId', getCustomParseIntPipe()) configId: number,
  ): Promise<ExtractorResponseDto[]> {
    const items = await this.service.findAllByCondition({ config: { id: configId } }, true);

    return new Mapper().mapList(items, ExtractorResponseDto);
  }

  @ApiOkResponse({ type: [ExtractorResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @Version('1')
  @Get()
  async getAllV1(): Promise<ExtractorResponseDto[]> {
    const items = await this.service.findAllByCondition({}, true);

    return new Mapper().mapList(items, ExtractorResponseDto);
  }

  @ApiOkResponse({ type: [ExtractorResponseDto] })
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @Version('1')
  @Patch(':id')
  async updateV1(
    @Body() body: ExtractorUpdateDto,
    @Param('id', getCustomParseIntPipe()) id: number,
  ): Promise<ExtractorResponseDto> {
    const { payload, errors } = await this.service.update(id, body);

    if (!payload) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    return new Mapper().map(payload, new ExtractorResponseDto());
  }

  @ApiNoContentResponse()
  @ApiBadRequestResponse({ type: ErrorsDto })
  @ApiNotFoundResponse({ type: ErrorsDto })
  @ForSuperAdminOnly()
  @Version('1')
  @Delete(':id')
  async removeV1(
    @Param('id', getCustomParseIntPipe()) id: number,
    @Res() response: Response,
  ): Promise<ErrorsDto | void> {
    const { errors } = await this.service.remove(id);

    if (errors?.length) {
      throw new TypedHttpException({ errors: errors }, Number.parseInt(errors[0].code));
    }

    response.status(HttpStatus.NO_CONTENT);
    response.send();
  }
}
