import { ApiProperty } from '@nestjs/swagger';
import { ExtractorEntity } from '@src/business/extractors/entities/extractor.entity';
import { ExtractWays } from '@src/business/extractors/entities/extract-way.enum';
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class ExtractorUpdateDto
  implements Omit<ExtractorEntity, 'config' | 'createdAt' | 'updatedAt' | 'id' | 'dataType' | 'name'>
{
  @IsOptional()
  @IsNotEmpty()
  @IsEnum(ExtractWays)
  @ApiProperty({ enum: ExtractWays, enumName: 'ExtractWaysEnum', description: 'extract way' })
  extractWay: ExtractWays;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'extract name' })
  public extractName: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'description' })
  public description: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'extractor stringified default value' })
  public stringifiedDefaultValue: string;
}
