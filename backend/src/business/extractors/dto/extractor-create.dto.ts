import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { ApiProperty } from '@nestjs/swagger';
import { ExtractorEntity } from '@src/business/extractors/entities/extractor.entity';
import { ExtractWays } from '@src/business/extractors/entities/extract-way.enum';
import { IsAlpha, IsEnum, IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ConfigStatuses } from '@src/business/configs/entities/config-status.enum';

export class ExtractorCreateDto
  implements Omit<ExtractorEntity, 'config' | 'createdAt' | 'updatedAt' | 'id'>
{
  @IsInt()
  @ApiProperty({ description: 'config id' })
  public configId: number;

  @IsNotEmpty()
  @IsEnum(DataTypes)
  @ApiProperty({ enum: DataTypes, enumName: 'DataTypesEnum', description: 'data type' })
  dataType: DataTypes;

  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  @ApiProperty({ description: 'name' })
  public name: string;

  @IsNotEmpty()
  @IsEnum(ExtractWays)
  @ApiProperty({ enum: ExtractWays, enumName: 'ExtractWaysEnum', description: 'extract way' })
  extractWay: ExtractWays;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'extract name' })
  public extractName: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'description' })
  public description: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'extractor stringified default value' })
  public stringifiedDefaultValue: string;
}
