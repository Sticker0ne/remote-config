import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { MapFrom, MapProp } from 'ts-simple-automapper';
import { ApiProperty } from '@nestjs/swagger';
import { ExtractorEntity } from '@src/business/extractors/entities/extractor.entity';
import { ExtractWays } from '@src/business/extractors/entities/extract-way.enum';

export class ExtractorResponseDto implements Omit<ExtractorEntity, 'config'> {
  @MapProp()
  @ApiProperty({ description: 'id' })
  public id: number;

  @MapFrom(() => ExtractorEntity, { mapFrom: (v) => v.config?.id })
  @ApiProperty({ description: 'configId' })
  public configId: number;

  @MapProp()
  @ApiProperty({ description: 'data type', enum: DataTypes, enumName: 'DataTypesEnum' })
  dataType: DataTypes;

  @MapProp()
  @ApiProperty({ description: 'name' })
  public name: string;

  @MapProp()
  @ApiProperty({ description: 'extract way', enum: ExtractWays, enumName: 'ExtractWaysEnum' })
  extractWay: ExtractWays;

  @MapProp()
  @ApiProperty({ description: 'extract variable name' })
  public extractName: string;

  @MapProp()
  @ApiProperty({ description: 'description' })
  public description: string;

  @MapProp()
  @ApiProperty({ description: 'stringified value' })
  public stringifiedDefaultValue: string;

  @MapProp()
  @ApiProperty({ description: 'createdAt' })
  public createdAt: Date;

  @MapProp()
  @ApiProperty({ description: 'updatedAt' })
  public updatedAt: Date;
}
