import { Module } from '@nestjs/common';
import { ExtractorsController } from './extractors.controller';
import { ExtractorsService } from './extractors.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExtractorEntity } from '@src/business/extractors/entities/extractor.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ExtractorEntity])],
  providers: [ExtractorsService],
  controllers: [ExtractorsController],
})
export class ExtractorsModule {}
