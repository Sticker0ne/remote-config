import * as dotenv from 'dotenv';
import * as fs from 'fs';
import type { IEnvVars, RawEnvObjectType } from './config.types';
import { EnvVarsValidator } from './config.types';
import { FptsUtils } from '../utils/fpts.utils';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AppConfigService {
  private readonly _envConfig: IEnvVars;

  constructor(filePath: string) {
    let parsedEnv = dotenv.parse(fs.readFileSync(filePath));
    parsedEnv = { ...parsedEnv, ...process.env };
    const camelCasedEnv = Object.keys(parsedEnv).reduce((acc, current) => {
      let propertyValue = parsedEnv[current];

      try {
        propertyValue = JSON.parse(propertyValue);
      } catch (e) {}

      acc[AppConfigService.toCamel(current.toLowerCase())] = propertyValue;
      return acc;
    }, {});

    this._envConfig = this.validateInput(camelCasedEnv);
  }

  private static toCamel(name: string) {
    return name.replace(/([-_][A-z])/gi, ($1) => {
      return $1.toUpperCase().replace('-', '').replace('_', '');
    });
  }

  private validateInput(rawEnvObject: RawEnvObjectType): IEnvVars {
    const { errors, value } = FptsUtils.decode(EnvVarsValidator, rawEnvObject);
    if (!value) throw new Error(`Config validation error: ${errors}`);

    return value;
  }

  public get envConfig(): IEnvVars {
    return this._envConfig;
  }
}
