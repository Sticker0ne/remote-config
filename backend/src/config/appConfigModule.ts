import { Global, Module } from '@nestjs/common';
import { AppConfigService } from './app-config.service';
import { AppConstants } from '@src/utils/app-constants';

@Global()
@Module({
  providers: [
    {
      provide: AppConfigService,
      useValue: new AppConfigService(AppConstants.envFilePath),
    },
  ],
  exports: [AppConfigService],
})
export class AppConfigModule {}
