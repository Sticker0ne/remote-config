import * as t from 'io-ts';
import { FptsUtils } from '@src/utils/fpts.utils';

enum DatabaseTypesEnum {
  postgres = 'postgres',
  mysql = 'mysql',
}

export enum CacheServiceTypesEnum {
  redis = 'redis',
  couchbase = 'couchbase',
}

export const EnvVarsValidator = t.type(
  {
    appPort: t.number,
    appBasePath: t.string,
    databaseType: FptsUtils.validatorFromEnum(DatabaseTypesEnum),
    databaseUser: t.string,
    databasePassword: t.string,
    databaseName: t.string,
    databaseHost: t.string,
    databasePort: t.number,
    passwordSalt: t.string,
    jwtAccessSecretKey: t.string,
    jwtAccessExpireTime: t.number,
    jwtRefreshSecretKey: t.string,
    jwtRefreshExpireTime: t.number,
    superAdminEmail: FptsUtils.optional(t.string),
    superAdminPassword: FptsUtils.optional(t.string),
    propertyExecutionTimeout: t.number,
    cacheServiceType: FptsUtils.validatorFromEnum(CacheServiceTypesEnum),
    redisHost: FptsUtils.optional(t.string),
    redisPort: FptsUtils.optional(t.number),
    couchBaseHost: FptsUtils.optional(t.string),
    couchBaseUsername: FptsUtils.optional(t.string),
    couchBasePassword: FptsUtils.optional(t.string),
    couchBaseBucket: FptsUtils.optional(t.string),
    disableIsolation: FptsUtils.optional(t.boolean),
  },
  'EnvVarsValidator',
);
export type IEnvVars = t.TypeOf<typeof EnvVarsValidator>;
export type RawEnvObjectType = { [name: string]: string };
