import {SYMBOL_PROP_MAPPING_RULES} from 'ts-simple-automapper/dist/constants/DecoratorConstants'
import { ClassType } from 'ts-simple-automapper/dist/types/ClassType';

export class Mapper {
  map<TSource extends Record<string, any>, TDestination extends Record<string, any>>(source: TSource, destination: TDestination): TDestination {
    if (!source || !destination) {
      return null;
    }
    // Start with metadata to get rules and keys for properties that are not defined.
    var destinationRules = Reflect.getMetadata(SYMBOL_PROP_MAPPING_RULES, destination.constructor);
    if (!destinationRules) {
      destinationRules = [];
    }
    var _loop_1 = function (destinationKey) {
      // TODO: Needed?
      // if (!destination.hasOwnProperty(propKey)) {
      //     continue;
      // }
      // If property is already in our rules, skip it.
      var ruleIndex = destinationRules.findIndex(function (r) { return r.propName === destinationKey; });
      if (ruleIndex >= 0) {
        return "continue";
      }
      destinationRules.push({
        propName: destinationKey
      });
    };
    // Add properties on the destination object that are not defined in metadata.
    for (var _i = 0, _a = Object.keys(destination); _i < _a.length; _i++) {
      var destinationKey = _a[_i];
      _loop_1(destinationKey);
    }
    // If no properties exist to be mapped, return.
    if (!destinationRules.length) {
      return destination;
    }
    // Get source rules as well.
    var sourceRules = Reflect.getMetadata(SYMBOL_PROP_MAPPING_RULES, source.constructor);
    if (!sourceRules) {
      sourceRules = [];
    }
    // Get list of props to map from our new list of destination rules.
    // More than one rule may have been defined for a destination property.
    // Do not duplicate keys in the list of keys to iterate.
    var destinationKeys = [];
    for (var _b = 0, destinationRules_1 = destinationRules; _b < destinationRules_1.length; _b++) {
      var destinationRule = destinationRules_1[_b];
      if (destinationKeys.indexOf(destinationRule.propName) === -1) {
        destinationKeys.push(destinationRule.propName);
      }
    }
    var _loop_2 = function (destinationKey) {
      // TODO: Needed?
      // if (!destination.hasOwnProperty(destinationKey)) {
      //     continue;
      // }
      // Check if ignored for this source type specifically.
      var propIgnoredForSourceType = destinationRules.findIndex(function (r) {
        return r.propName === destinationKey
          && r.ignore
          && !!r.sourceTypeProvider
          && r.sourceTypeProvider().prototype === source.constructor.prototype;
      }) >= 0;
      // If ignored for this source type, skip.
      if (propIgnoredForSourceType) {
        return "continue";
      }
      // Check if ignored in general.
      var propIgnored = destinationRules.findIndex(function (r) {
        return r.propName === destinationKey
          && r.ignore
          && !r.sourceTypeProvider;
      }) >= 0;
      // Check if mapped for this source type. Save the index to use the rule later if needed.
      var mappingRuleIndex = destinationRules.findIndex(function (r) {
        return r.propName === destinationKey
          && !r.ignore
          && !!r.sourceTypeProvider
      });
      // If ignored, skip.
      // However, if overridden to be mapped for this source type, do not skip.
      if (propIgnored && mappingRuleIndex === -1) {
        return "continue";
      }
      // Check if property is hidden by source type.
      var hidden = sourceRules.findIndex(function (r) {
        return r.propName === destinationKey
          && r.hide
          && (!r.destinationTypeProvider
            || r.destinationTypeProvider().prototype === destination.constructor.prototype);
      }) >= 0;
      // If hidden in general or from this destination type, skip.
      if (hidden) {
        return "continue";
      }
      // Now we can map the property.
      // First, see if there are custom mapping options.
      var mappingOptions = null;
      if (mappingRuleIndex >= 0) {
        mappingOptions = destinationRules[mappingRuleIndex].mapFromOptions;
      }
      var mappedValue = source[destinationKey];
      if (mappingOptions) {
        if (mappingOptions.mapFrom) {
          mappedValue = mappingOptions.mapFrom(source);
        }
        // If a destinationValueTypeProvider was specified,
        // then the source and destination types are both class instances
        // and the destination should be mapped accordingly.
        if (mappingOptions.destinationValueTypeProvider) {
          var mapToConstructor = mappingOptions.destinationValueTypeProvider();
          if (mappedValue instanceof Array) {
            mappedValue = this_1.mapList(mappedValue, mapToConstructor);
          }
          else {
            mappedValue = this_1.map(mappedValue, new mapToConstructor());
          }
        }
      }
      // wtf, typescript?
      // https://github.com/microsoft/TypeScript/issues/31661
      // destination[destinationKey] = mappedValue;
      // @ts-ignore
      destination[destinationKey] = mappedValue;
    };
    var this_1 = this;
    for (var _c = 0, destinationKeys_1 = destinationKeys; _c < destinationKeys_1.length; _c++) {
      // @ts-ignore
      var destinationKey = destinationKeys_1[_c];
      _loop_2(destinationKey);
    }
    return destination;
  };

  public mapList<TSource, TDestination>(sourceList: TSource[], destinationType: ClassType<TDestination>): TDestination[] {
    var _this = this;
    if (!(sourceList instanceof Array)) {
      return [];
    }
    return sourceList.map(function (s) { return _this.map(s, new destinationType()); });
  };
}
