export function jsonClone<T>(payload: T): T {
  return JSON.parse(JSON.stringify(payload));
}

export function jsonSafeParse(payload: any): any {
  try {
    if (Array.isArray(payload)) return payload;
    return JSON.parse(payload);
  } catch (e) {
    return payload;
  }
}

export function jsonSafeParseArray(payload: any): any {
  try {
    // Fix glitch with single length arrays
    if (!Array.isArray(payload)) return payload;

    return payload.map(jsonSafeParse);
  } catch (e) {
    return payload;
  }
}
