import type { IRequestContext } from '@src/business/configs/types/request-context.types';
import { ExtractWays } from '@src/business/extractors/entities/extract-way.enum';
import type { ICacheExtractor } from '@src/cache/types/cache-extractor.type';
import { DataTypes } from '@src/business/common/enums/data-types.enum';
import { validateDataTypeValue } from '@src/business/common/validators/data-types-validator';
import { CommonUtils } from '@src/utils/common.utils';
import { arrayLikeDataTypes } from '@src/business/configs/utils/data-exctractor.utils';

export function extractDataFromRequest(
  context: IRequestContext,
  extractors: ICacheExtractor[],
): Record<string, any> {
  const requestData: Record<string, any> = {};

  context.cookie = context.cookie || {};
  context.body = context.body || {};
  context.query = context.query || {};

  const extractWayToRequestKeyMap: Record<ExtractWays, keyof IRequestContext> = {
    BODY: 'body',
    COOKIE: 'cookie',
    QUERY: 'query',
  };

  extractors.forEach((extractor) => {
    const extractKey = extractWayToRequestKeyMap[extractor.extractWay];
    const providerObject = context[extractKey];
    const resultKeyName = extractor.name;
    const dataTypeIsArrayLike = arrayLikeDataTypes.includes(extractor.dataType);

    let providedValue = providerObject[extractor.extractName];
    requestData[resultKeyName] = extractor.defaultValue;

    // Value is empty
    if (providedValue === undefined) return;

    // Extract value from body
    if (extractor.extractWay === ExtractWays.BODY) {
      const validType = validateDataTypeValue(extractor.dataType, providedValue);
      if (validType) requestData[resultKeyName] = providedValue;
      return;
    }

    // Extract value from query (all queries are string or string[])
    if (extractor.extractWay === ExtractWays.QUERY) {
      const providedValueIsArray = Array.isArray(providedValue);

      // normalize by array
      if (providedValueIsArray && !dataTypeIsArrayLike)
        providedValue = providedValue[providedValue.length - 1];
      else if (dataTypeIsArrayLike && !providedValueIsArray) providedValue = [providedValue];

      // if data type is string or string[] return it without parsing. (all queries are string or string[])
      if (extractor.dataType === DataTypes.STRING || extractor.dataType === DataTypes.ARRAY_OF_STRINGS) {
        const validType = validateDataTypeValue(extractor.dataType, providedValue);

        if (validType) requestData[resultKeyName] = providedValue;
        return;
      }

      const parsedLocalValue = Array.isArray(providedValue)
        ? CommonUtils.jsonSafeParseArray(providedValue)
        : CommonUtils.jsonSafeParse(providedValue);

      const validType = validateDataTypeValue(extractor.dataType, parsedLocalValue);
      if (validType) requestData[resultKeyName] = parsedLocalValue;
      return;
    }

    // Extract value from cookie (all cookies are string)
    if (extractor.extractWay === ExtractWays.COOKIE) {
      providedValue = providedValue.toString().split(',');

      const providedValueIsArray = Array.isArray(providedValue);

      // normalize by array
      if (providedValueIsArray && !dataTypeIsArrayLike)
        providedValue = providedValue[providedValue.length - 1];
      else if (dataTypeIsArrayLike && !providedValueIsArray) providedValue = [providedValue];

      // if data type is string or string[] return it without parsing. (now localProvidedValue can be string or string[])
      if (extractor.dataType === DataTypes.STRING || extractor.dataType === DataTypes.ARRAY_OF_STRINGS) {
        const validType = validateDataTypeValue(extractor.dataType, providedValue);

        if (validType) requestData[resultKeyName] = providedValue;
        return;
      }

      const parsedLocalValue = Array.isArray(providedValue)
        ? CommonUtils.jsonSafeParseArray(providedValue)
        : CommonUtils.jsonSafeParse(providedValue);

      const validType = validateDataTypeValue(extractor.dataType, parsedLocalValue);

      if (validType) requestData[resultKeyName] = parsedLocalValue;
      return;
    }
  });

  return requestData;
}
