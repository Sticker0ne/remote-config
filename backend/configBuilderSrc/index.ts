import fastify from 'fastify';
import { createClient } from 'redis';
import * as ivm from 'isolated-vm';
import { parse } from 'cookie';
import { IRequestContext } from '@src/business/configs/types/request-context.types';
import { ICacheConfig } from '@src/cache/types/cache-config.type';
import { extractDataFromRequest } from './requestDataExtractor';
import { extractDataFromProperties, extractDataFromPropertiesNoIsolation } from './propertiesDataExtractor';
import { connect } from 'couchbase';
import type { Isolate } from 'isolated-vm';

type TVariables = Record<string, any> | undefined;
type TGetAsync = (key: string) => Promise<string | null>;

const fastifyInstance = fastify({ logger: false });

let {
  BUILDER_APP_HOST,
  BUILDER_API_PREFIX,
  REDIS_HOST,
  REDIS_PORT,
  BUILDER_APP_PORT,
  CACHE_SERVICE_TYPE,
  COUCH_BASE_HOST,
  COUCH_BASE_USERNAME,
  COUCH_BASE_PASSWORD,
  COUCH_BASE_BUCKET,
  ISOLATE_MEMORY_LIMIT,
  DISABLE_ISOLATION,
} = process.env;

BUILDER_APP_HOST = BUILDER_APP_HOST || '0.0.0.0';
BUILDER_API_PREFIX = BUILDER_API_PREFIX || '/fast-builder';
REDIS_HOST = REDIS_HOST || 'localhost';
REDIS_PORT = REDIS_PORT || (6379).toString();
BUILDER_APP_PORT = BUILDER_APP_PORT || (8080).toString();

CACHE_SERVICE_TYPE = CACHE_SERVICE_TYPE || 'couchbase';
COUCH_BASE_HOST = COUCH_BASE_HOST || 'couchbase://localhost';
COUCH_BASE_USERNAME = COUCH_BASE_USERNAME || 'Administrator';
COUCH_BASE_PASSWORD = COUCH_BASE_PASSWORD || 'password';
COUCH_BASE_BUCKET = COUCH_BASE_BUCKET || 'remote-config-bucket';

ISOLATE_MEMORY_LIMIT = ISOLATE_MEMORY_LIMIT || (4096).toString();
DISABLE_ISOLATION = DISABLE_ISOLATION || 'false';

const shouldDisableIsolation = DISABLE_ISOLATION !== 'true';

let isolate!: Isolate;
if (!shouldDisableIsolation)
  isolate = new ivm.Isolate({ memoryLimit: Number.parseInt(ISOLATE_MEMORY_LIMIT) });

let getAsync!: TGetAsync;

async function buildCacheServiceGetFunction(): Promise<TGetAsync> {
  const needRedis = CACHE_SERVICE_TYPE === 'redis';
  const needCouchbase = CACHE_SERVICE_TYPE === 'couchbase';

  if (needRedis) {
    const redisClient = createClient(Number.parseInt(REDIS_PORT) || 6379, REDIS_HOST);
    redisClient.on('error', (err) => console.log(err));

    const getAsyncRedis = (key: string): Promise<string | null> => {
      return new Promise((resolve) => {
        try {
          if (!redisClient?.connected) {
            resolve(null);
            return;
          }
          redisClient?.get(key, (err, payload) => {
            resolve(payload);
          });
        } catch (e) {
          resolve(null);
        }
      });
    };

    return getAsyncRedis;
  }

  if (needCouchbase) {
    const cluster = await connect(COUCH_BASE_HOST, {
      username: COUCH_BASE_USERNAME,
      password: COUCH_BASE_PASSWORD,
    });

    const bucket = cluster.bucket(COUCH_BASE_BUCKET);
    const cache = bucket.defaultCollection();

    const getAsyncCouchbase = async (key: string) => {
      const res = await cache.get(key);
      if (!res?.content) return null;
      if (typeof res?.content !== 'string') return res?.content?.toString();
      return res?.content;
    };

    return getAsyncCouchbase;
  }

  throw new Error('incorrect environment variables for cache service');
}

async function onConfigRequest(configName: string, requestContext: IRequestContext): Promise<any> {
  let config!: string;
  let variables!: string;

  try {
    config = await getAsync('config_' + configName);
    variables = await getAsync('variables');
  } catch (err) {
    throw { status: 404, message: "can't load data for config from cache service" };
  }

  if (!config || !variables) {
    throw { status: 404, message: "can't load data for config from cache service" };
  }

  const parsedConfig: ICacheConfig = JSON.parse(config);
  const globalParsedVariables: TVariables = JSON.parse(variables);

  const requestData = extractDataFromRequest(requestContext, parsedConfig.extractors);
  const codeExecutionContext = {
    globalVariables: globalParsedVariables,
    configVariables: parsedConfig.variables,
    requestData,
  };

  let configResult!: Record<string, unknown>;

  if (shouldDisableIsolation)
    configResult = extractDataFromPropertiesNoIsolation(codeExecutionContext, parsedConfig.properties);
  else configResult = extractDataFromProperties(codeExecutionContext, parsedConfig.properties, isolate, 10);

  return configResult;
}

// Declare a route
fastifyInstance.all(`${BUILDER_API_PREFIX}/:configName`, async (request) => {
  const cookie = parse(request.headers['cookie'] || '');
  const body = request.body || {};
  const query = request.query || {};

  return await onConfigRequest(request.params['configName'] || '', { cookie, query, body });
});

// Run the server!
const start = async () => {
  try {
    getAsync = await buildCacheServiceGetFunction();
    await fastifyInstance.listen({
      port: Number.parseInt(BUILDER_APP_PORT || '8080'),
      host: BUILDER_APP_HOST,
    });
  } catch (err) {
    fastifyInstance.log.error(err);
    process.exit(1);
  }
};

start();
