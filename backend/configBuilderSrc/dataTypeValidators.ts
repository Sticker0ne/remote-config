import type { DefaultValueType } from '@src/cache/types/default-value.type';
import { DataTypes } from '@src/business/common/enums/data-types.enum';

function validateStringType(payload: any, defaultValue: DefaultValueType): string {
  if (typeof payload === 'string') return payload;
  else return defaultValue as string;
}

function validateBooleanType(payload: any, defaultValue: DefaultValueType): boolean {
  if (typeof payload === 'boolean') return payload;
  else return defaultValue as boolean;
}

function validateNumberType(payload: any, defaultValue: DefaultValueType): number {
  if (typeof payload === 'number') return payload;
  else return defaultValue as number;
}

function validateStringArray(payload: any, defaultValue: DefaultValueType): string[] {
  if (!Array.isArray(payload)) return defaultValue as string[];

  const itemsTypeIsCorrect = payload.every((item) => typeof item === 'string');
  return itemsTypeIsCorrect ? payload : (defaultValue as string[]);
}

function validateBooleanArray(payload: any, defaultValue: DefaultValueType): boolean[] {
  if (!Array.isArray(payload)) return defaultValue as boolean[];

  const itemsTypeIsCorrect = payload.every((item) => typeof item === 'boolean');
  return itemsTypeIsCorrect ? payload : (defaultValue as boolean[]);
}

function validateNumberArray(payload: any, defaultValue: DefaultValueType): number[] {
  if (!Array.isArray(payload)) return defaultValue as number[];

  const itemsTypeIsCorrect = payload.every((item) => typeof item === 'number');
  return itemsTypeIsCorrect ? payload : (defaultValue as number[]);
}

export function getValidDataType(
  dataType: DataTypes,
  payload: any,
  defaultValue: DefaultValueType,
): DefaultValueType {
  if (dataType === DataTypes.STRING) return validateStringType(payload, defaultValue);
  if (dataType === DataTypes.NUMBER) return validateNumberType(payload, defaultValue);
  if (dataType === DataTypes.BOOLEAN) return validateBooleanType(payload, defaultValue);
  if (dataType === DataTypes.ARRAY_OF_STRINGS) return validateStringArray(payload, defaultValue);
  if (dataType === DataTypes.ARRAY_OF_NUMBERS) return validateNumberArray(payload, defaultValue);
  if (dataType === DataTypes.ARRAY_OF_BOOLEANS) return validateBooleanArray(payload, defaultValue);
}
