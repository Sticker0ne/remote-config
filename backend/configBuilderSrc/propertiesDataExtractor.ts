import type { ICodeExecutionContext } from '@src/business/configs/types/code-execution-context.types';
import type { Isolate } from 'isolated-vm';
import { FIRST_LINE_SPREAD_CODE } from '@src/business/configs/constants/code.const';
import type { ICacheProperty } from '@src/cache/types/cache-property.type';
import { getValidDataType } from './dataTypeValidators';

export function extractDataFromProperties(
  codeExecutionContext: ICodeExecutionContext,
  properties: ICacheProperty[],
  isolate: Isolate,
  timeout: number,
): Record<string, any> {
  const resultData: Record<string, any> = {};
  const normalizedContext = isolate.createContextSync();

  const argumentsArray = [codeExecutionContext];
  const evalOptions = {
    timeout,
    arguments: { copy: true },
    result: { copy: true },
  };

  const evalPromises: any[] = properties.map((property) => {
    const patchedCode = FIRST_LINE_SPREAD_CODE + property.code;
    try {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const res = normalizedContext.evalClosureSync(patchedCode, argumentsArray, evalOptions);
      return getValidDataType(property.dataType, res, property.defaultValue);
    } catch (e) {
      console.log(e);
      return property.defaultValue;
    }
  });

  evalPromises.forEach((result, index) => {
    resultData[properties[index].name] = result;
  });

  normalizedContext.release();

  return resultData;
}

export function extractDataFromPropertiesNoIsolation(
  codeExecutionContext: ICodeExecutionContext,
  properties: ICacheProperty[],
): Record<string, any> {
  return properties.reduce((acc: Record<string, any>, property) => {
    const patchedCode = FIRST_LINE_SPREAD_CODE + property.code;
    let res!: any;
    try {
      const functionResult = Function('$0', patchedCode)(codeExecutionContext);
      res = getValidDataType(property.dataType, functionResult, property.defaultValue);
    } catch (e) {
      console.log(e);
      res = property.defaultValue;
    }

    acc[property.name] = res;
    return acc;
  }, {});
}
