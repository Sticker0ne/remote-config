# Build
FROM node:14-alpine as build-stage

ENV REDIS_HOST=localhost
ENV REDIS_PORT=6379
ENV BUILDER_APP_PORT=8080

RUN apk add --update make=4.2.1-r2 --repository=http://dl-cdn.alpinelinux.org/alpine/v3.10/main
RUN apk add --update g++=9.3.0-r0 python3=3.8.10-r0 py3-pip --repository=http://dl-cdn.alpinelinux.org/alpine/v3.11/main

WORKDIR /app
COPY . .
RUN yarn --frozen-lockfile --network-timeout 1000000

ENTRYPOINT ["npm", "run", "start-builder"]
EXPOSE 8080
