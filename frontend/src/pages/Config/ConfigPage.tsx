import React, { useState } from 'react';
import styles from './ConfigPage.module.css';
import WithAsyncData from 'components/common/WithAsyncData/WithAsyncData';
import { useAppDispatch, useAppSelector } from 'common/hooks/store.hook';
import { fetchConfig, selectConfig } from 'business/configs/configs.slice';
import { useParams } from 'react-router-dom';
import { Avatar, Button, Empty, PageHeader, Space, Tooltip } from 'antd';
import { fetchGlobalVariables, selectVariables } from 'business/variables/variables.slice';
import VariablesCRUD from 'business/variables/components/VariablesCRUD/VariablesCRUD';
import ExtractorsCRUD from 'business/extractors/components/ExtractorsCRUD/ExtractorsCRUD';
import PropertiesCRUD from 'business/properties/components/PropertiesCRUD/PropertiesCRUD';
import SwaggerModal from 'components/SwaggerModal/SwaggerModal';
import { CodeSandboxOutlined } from '@ant-design/icons';

const sandboxPrefix: string | undefined = process.env.REACT_APP_SANDBOX_PREFIX || undefined;
const fastSandboxPrefix: string | undefined = process.env.REACT_APP_FAST_SANDBOX_PREFIX || undefined;

function ConfigPage() {
  const { id } = useParams<{ id?: string }>();
  const [showSandbox, setShowSandbox] = useState(false);
  const [modalPathPrefix, setModalPathPrefix] = useState('');

  const openSandboxButton = sandboxPrefix ? (
    <Tooltip
      key={1}
      placement={'top'}
      title={'Sandbox from main backend. Try it BEFORE saving changes in cache.'}
    >
      <Button
        type="primary"
        icon={<CodeSandboxOutlined />}
        onClick={() => {
          setModalPathPrefix(sandboxPrefix || '');
          setShowSandbox(true);
        }}
      >
        Open sandbox
      </Button>
    </Tooltip>
  ) : null;

  const openFastSandboxButton = fastSandboxPrefix ? (
    <Tooltip
      key={2}
      placement={'top'}
      title={'Sandbox from fast-builder-module. Try it AFTER saving changes in cache.'}
    >
      <Button
        key={1}
        type="default"
        icon={<CodeSandboxOutlined />}
        onClick={() => {
          setModalPathPrefix(fastSandboxPrefix || '');
          setShowSandbox(true);
        }}
      >
        Open fast-builder sandbox
      </Button>
    </Tooltip>
  ) : null;

  const dispatch = useAppDispatch();
  const config = useAppSelector(selectConfig);
  const globalVars = useAppSelector(selectVariables);

  const avatarSrc = `https://robohash.org/${config?.name}`;

  function asyncData() {
    const fetchConfigPromise = dispatch(fetchConfig({ id: Number(id) }));
    const fetchGlobalVarsPromise = dispatch(fetchGlobalVariables());

    return Promise.all([fetchConfigPromise, fetchGlobalVarsPromise]);
  }

  function reFetchConfig() {
    return dispatch(fetchConfig({ id: Number(id) }));
  }

  return (
    <WithAsyncData asyncDataFunction={asyncData} fullscreen>
      <div className={styles.pageContainer}>
        {config ? (
          <>
            {showSandbox ? (
              <SwaggerModal
                onClose={() => setShowSandbox(false)}
                configName={config.name}
                sandboxPrefix={modalPathPrefix}
              />
            ) : null}
            <PageHeader
              className={styles.pageHeader}
              title={
                <Space>
                  <Avatar size="large" src={avatarSrc} />
                  <span>{config.name}</span>
                </Space>
              }
              subTitle={config.description}
              extra={[openSandboxButton, openFastSandboxButton]}
            />
            <VariablesCRUD
              variableList={config.variables || []}
              tableTitle="Config variables"
              configId={config.id}
              onItemsChange={reFetchConfig}
            />
            <ExtractorsCRUD
              extractorList={config.extractors || []}
              configId={config.id}
              onItemsChange={reFetchConfig}
            />
            <PropertiesCRUD
              propertyList={config?.properties || []}
              configId={config.id}
              globalVars={globalVars}
              configVars={config.variables || []}
              configExtractors={config.extractors || []}
              onItemsChange={reFetchConfig}
            />
          </>
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        )}
      </div>
    </WithAsyncData>
  );
}

export default ConfigPage;
