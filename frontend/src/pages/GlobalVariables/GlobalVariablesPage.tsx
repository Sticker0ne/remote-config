import React from 'react';
import styles from './GlobalVariablesPage.module.css';
import WithAsyncData from 'components/common/WithAsyncData/WithAsyncData';
import { useAppDispatch, useAppSelector } from 'common/hooks/store.hook';
import { fetchGlobalVariables, selectVariables } from 'business/variables/variables.slice';
import VariablesCRUD from 'business/variables/components/VariablesCRUD/VariablesCRUD';

function GlobalVariablesPage() {
  const dispatch = useAppDispatch();
  const variables = useAppSelector(selectVariables);

  function fetchVariables() {
    return dispatch(fetchGlobalVariables());
  }

  const asyncData = () => {
    return fetchVariables();
  };

  return (
    <WithAsyncData asyncDataFunction={asyncData} fullscreen>
      <div className={styles.pageContainer}>
        <VariablesCRUD
          variableList={variables}
          tableTitle="Global variables that are available in all configs"
          onItemsChange={fetchVariables}
        />
      </div>
    </WithAsyncData>
  );
}

export default GlobalVariablesPage;
