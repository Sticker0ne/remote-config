import React, { useRef } from 'react';
import styles from 'pages/Login/LoginPage.module.css';
import { Button, Card, Form, Input, Spin } from 'antd';
import type { ILoginFormData } from './LoginPage.types';
import { Rule } from 'rc-field-form/lib/interface';
import { useLoader } from 'common/hooks/useLoader.hook';
import { CommonUtils } from 'common/utils/Common.utils';
import { useForm } from 'antd/es/form/Form';
import { commonHistory } from 'common/router/history';
import { AppRoutes } from 'common/router/routes';
import { AuthService } from 'api/AuthService';

const emailInputRules: Rule[] = [
  { required: true, message: 'Please input your email!', validateTrigger: ['onFinish'] },
  { type: 'email', validateTrigger: ['onFinish'] },
];
const passwordInputRules: Rule[] = [{ required: true, message: 'Please input your password!' }];

const safeLogin = CommonUtils.errorifyAsyncFunction(AuthService.authControllerLoginV1, {
  autoShowErrorsNotifications: true,
});

function LoginPage() {
  const [loginPending, processLogin] = useLoader();
  const [form] = useForm<ILoginFormData>();
  const passwordInputRef = useRef<HTMLInputElement>(null);

  const onLoginError = () => {
    passwordInputRef.current?.focus();
    form.setFieldsValue({ password: '' });
  };

  const onLoginSuccess = () => {
    commonHistory.push(AppRoutes.home.path);
  };

  const onFinish = async (values: ILoginFormData) => {
    const loginResult = await processLogin(safeLogin({ body: values }));
    const { errors } = loginResult;
    if (errors?.length) onLoginError();
    else onLoginSuccess();
  };

  return (
    <div className={styles.loginPageContainer}>
      <Spin spinning={loginPending} size="large">
        <Card title="Login">
          <Form
            name="basic"
            onFinish={onFinish}
            autoComplete="off"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            form={form}
          >
            <Form.Item label="Email" name="email" rules={emailInputRules}>
              <Input placeholder="Email" />
            </Form.Item>

            <Form.Item label="Password" name="password" rules={passwordInputRules}>
              <Input.Password placeholder="Password" />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </Spin>
    </div>
  );
}

export default LoginPage;
