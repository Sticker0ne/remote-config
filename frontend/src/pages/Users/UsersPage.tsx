import React, { useState } from 'react';
import styles from 'pages/Users/UsersPage.module.css';
import { useAppDispatch, useAppSelector } from 'common/hooks/store.hook';
import { fetchAllUsers, selectAllUsers, selectCurrentUser } from 'business/users/users.slice';
import WithAsyncData from 'components/common/WithAsyncData/WithAsyncData';
import UserCard from 'business/users/components/UserCard/UserCard';
import UserEditModal from 'business/users/components/UserEditModal/UserEditModal';
import { CreateUserRequestDto, UpdateUserRequestDto, UserResponseDto } from 'api/index.defs';
import AppLoader from 'components/common/AppLoader/AppLoader';
import { UsersService } from 'api/UsersService';
import { CommonUtils } from 'common/utils/Common.utils';
import { Col, Row } from 'antd';
import AddNewCardStub from 'components/AddNewCardStub/AddNewCardStub';
import { UserAddOutlined } from '@ant-design/icons';

const safeRemoveUser = CommonUtils.errorifyAsyncFunction(UsersService.usersControllerRemoveV1, {
  autoShowErrorsNotifications: true,
});
const safeCreateUser = CommonUtils.errorifyAsyncFunction(UsersService.usersControllerCreateV1, {
  autoShowErrorsNotifications: true,
});
const safeUpdateUser = CommonUtils.errorifyAsyncFunction(UsersService.usersControllerUpdateV1, {
  autoShowErrorsNotifications: true,
});

function UsersPage() {
  const dispatch = useAppDispatch();
  const users = useAppSelector(selectAllUsers);
  const currentUser = useAppSelector(selectCurrentUser);

  const [modalShouldBeShown, setModalVisibility] = useState(false);
  const [userForEdit, setUserForEdit] = useState<UserResponseDto>();
  const [loaderShouldBeShown, setLoaderVisibility] = useState(false);

  const asyncData = () => {
    return dispatch(fetchAllUsers());
  };

  function openUserEditModal(index?: number) {
    if (index !== undefined) setUserForEdit(users[index]);
    else setUserForEdit(undefined);

    setModalVisibility(true);
  }

  function closeUserEditModal() {
    setModalVisibility(false);
  }

  async function onDeleteUser(userId: number) {
    if (userId === currentUser?.id) {
      CommonUtils.showErrors([{ code: '403', message: 'You can not delete yourself, sorry!' }]);
      closeUserEditModal();
      return;
    }

    setLoaderVisibility(true);
    try {
      await safeRemoveUser({ id: userId });
      await dispatch(fetchAllUsers());
    } finally {
      setLoaderVisibility(false);
      closeUserEditModal();
    }
  }

  async function onCreateUser(userData: CreateUserRequestDto) {
    setLoaderVisibility(true);
    try {
      await safeCreateUser({ body: userData });
      await dispatch(fetchAllUsers());
    } finally {
      setLoaderVisibility(false);
      closeUserEditModal();
    }
  }

  async function onUpdateUser(userId: number, userData: UpdateUserRequestDto) {
    setLoaderVisibility(true);
    try {
      await safeUpdateUser({ id: userId, body: userData });
      await dispatch(fetchAllUsers());
    } finally {
      setLoaderVisibility(false);
      closeUserEditModal();
    }
  }

  return (
    <WithAsyncData asyncDataFunction={asyncData} fullscreen>
      <div className={styles.pageContainer}>
        {loaderShouldBeShown && <AppLoader fullscreen />}
        {modalShouldBeShown && (
          <UserEditModal
            user={userForEdit}
            onClose={closeUserEditModal}
            onCreate={onCreateUser}
            onDelete={onDeleteUser}
            onUpdate={onUpdateUser}
          />
        )}
        <Row gutter={[24, 24]}>
          <Col span={12} lg={8} xl={6}>
            <AddNewCardStub onClick={openUserEditModal} width={290} height={180}>
              <UserAddOutlined className={styles.cardIcon} />
            </AddNewCardStub>
          </Col>
          {users.map((user, index) => (
            <Col span={12} lg={8} xl={6} key={`user-list-key-${user.id}`}>
              <UserCard
                user={user}
                showRemoveAction
                onEditAction={() => openUserEditModal(index)}
                onRemoveAction={onDeleteUser}
              />
            </Col>
          ))}
        </Row>
      </div>
    </WithAsyncData>
  );
}

export default UsersPage;
