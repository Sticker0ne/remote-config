import React, { useState } from 'react';
import styles from 'pages/Home/HomePage.module.css';
import WithAsyncData from 'components/common/WithAsyncData/WithAsyncData';
import { useAppDispatch, useAppSelector } from 'common/hooks/store.hook';
import { fetchConfigs, selectConfigs } from 'business/configs/configs.slice';
import ConfigCard from 'business/configs/components/ConfigCard/ConfigCard';
import { Col, Row } from 'antd';
import { CommonUtils } from 'common/utils/Common.utils';
import { ConfigsService } from 'api/ConfigsService';
import AppLoader from 'components/common/AppLoader/AppLoader';
import AddNewCardStub from 'components/AddNewCardStub/AddNewCardStub';
import { PlusCircleOutlined } from '@ant-design/icons';
import ConfigCreateModal from 'business/configs/components/ConfigCreateModal/ConfigCreateModal';
import { ConfigCreateRequestDto } from 'api/index.defs';
import { commonHistory } from 'common/router/history';
import { AppRoutes } from 'common/router/routes';

const safeCreateConfig = CommonUtils.errorifyAsyncFunction(ConfigsService.configsControllerCreateV1, {
  autoShowErrorsNotifications: true,
});
const safeDeleteConfig = CommonUtils.errorifyAsyncFunction(ConfigsService.configsControllerRemoveV1, {
  autoShowErrorsNotifications: true,
});

function HomePage() {
  const dispatch = useAppDispatch();
  const configs = useAppSelector(selectConfigs);

  const [loaderShouldBeShown, setLoaderVisibility] = useState(false);
  const [modalShouldBeShown, setModalVisibility] = useState(false);

  function asyncData() {
    return dispatch(fetchConfigs());
  }

  async function onDeleteConfig(id: number) {
    setLoaderVisibility(true);
    try {
      await safeDeleteConfig({ id });
      await dispatch(fetchConfigs());
    } finally {
      setLoaderVisibility(false);
    }
  }

  async function onCreateConfig(data: ConfigCreateRequestDto) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeCreateConfig({ body: data });
      if (errors?.length) return;
      await dispatch(fetchConfigs());
      closeModal();
    } finally {
      setLoaderVisibility(false);
    }
  }

  function onEditConfig(id: number) {
    if (AppRoutes.config.build) {
      commonHistory.push(AppRoutes.config.build({ id }));
    }
  }

  function openModal() {
    setModalVisibility(true);
  }

  function closeModal() {
    setModalVisibility(false);
  }

  return (
    <WithAsyncData asyncDataFunction={asyncData} fullscreen>
      <div className={styles.homePageContainer}>
        {loaderShouldBeShown && <AppLoader fullscreen />}
        {modalShouldBeShown && <ConfigCreateModal onClose={closeModal} onCreate={onCreateConfig} />}
        <Row gutter={[24, 24]}>
          {configs.map((config) => (
            <Col span={12} lg={8} xl={6} key={`config-list-key-${config.id}`}>
              <ConfigCard
                config={config}
                onDeleteActionClick={onDeleteConfig}
                onEditActionClick={onEditConfig}
              />
            </Col>
          ))}
          <Col span={8}>
            <AddNewCardStub width={290} height={242} onClick={openModal}>
              <PlusCircleOutlined style={{ fontSize: 60 }} />
            </AddNewCardStub>
          </Col>
        </Row>
      </div>
    </WithAsyncData>
  );
}

export default HomePage;
