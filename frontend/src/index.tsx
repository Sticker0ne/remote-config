import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { serviceOptions } from 'api/index.defs';
import { getAxiosInstance } from './common/axios';
import { Provider } from 'react-redux';
import { store } from 'store';

const instance = getAxiosInstance();
serviceOptions.axios = instance;

ReactDOM.render(
  // <React.StrictMode>
  <Provider store={store}>
    <App />
  </Provider>,
  // </React.StrictMode>,
  document.getElementById('root'),
);
