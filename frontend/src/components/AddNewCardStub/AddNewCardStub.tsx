import React from 'react';
import styles from 'components/AddNewCardStub/AddNewCardStub.module.css';

function AddNewCardStub({
  onClick,
  children,
  width,
  height,
}: {
  onClick?: () => void;
  children?: React.ReactNode | React.ReactNodeArray;
  width?: number;
  height?: number;
}) {
  return (
    <div className={styles.cardInner} style={{ width, height }} onClick={onClick}>
      {children}
    </div>
  );
}

export default AddNewCardStub;
