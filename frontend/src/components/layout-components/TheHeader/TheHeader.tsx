import React from 'react';
import styles from 'components/layout-components/TheHeader/TheHeader.module.css';
import TheNavbar from 'components/layout-components/TheNavbar/TheNavbar';
import { Header } from 'antd/es/layout/layout';
import UserBadge from 'business/users/components/UserBadge/UserBadge';

function TheHeader() {
  return (
    <Header className={styles.header}>
      <div className={styles.headerInner}>
        <h1 className={styles.headerText}>Remote config</h1>
        <div className={styles.navbarContainer}>
          <TheNavbar />
        </div>
        <div className={styles.userBadgerWrapper}>
          <UserBadge />
        </div>
      </div>
    </Header>
  );
}

export default TheHeader;
