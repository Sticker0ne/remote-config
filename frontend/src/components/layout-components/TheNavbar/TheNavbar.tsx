import { commonHistory } from 'common/router/history';
import { Popconfirm, Tabs } from 'antd';
import { FireOutlined, GlobalOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import { AppRoutes } from 'common/router/routes';
import React from 'react';
import WithScope from 'business/users/components/WithScope';
import { UserScopes } from 'business/users/enums/UserScopes.enum';
import styles from './TheNavbar.module.css';
import { CacheService } from 'api/CacheService';
import AppLoader from 'components/common/AppLoader/AppLoader';
import { useLoader } from 'common/hooks/useLoader.hook';
import { CommonUtils } from 'common/utils/Common.utils';

const cacheTabKey = 'cacheTabKey';

const safeFillCache = CommonUtils.errorifyAsyncFunction(CacheService.cacheControllerFillV1, {
  autoShowErrorsNotifications: true,
});

function TheNavbar() {
  const [cacheLoader, processCache] = useLoader();

  async function onCacheUpdateConfirm() {
    await processCache(safeFillCache());
  }

  function onTabChange(key: string) {
    if (key === cacheTabKey) {
      return;
    }
    commonHistory.push(key);
  }

  return (
    <Tabs activeKey={commonHistory.location.pathname} onChange={onTabChange}>
      {cacheLoader && <AppLoader fullscreen />}
      <Tabs.TabPane
        tab={
          <span>
            <SettingOutlined />
            Configs
          </span>
        }
        key={AppRoutes.home.path}
      />
      <Tabs.TabPane
        tab={
          <WithScope scopes={[UserScopes.canReadUsers]}>
            <span>
              <UserOutlined />
              Users
            </span>
          </WithScope>
        }
        key={AppRoutes.users.path}
      />
      <Tabs.TabPane
        tab={
          <span>
            <GlobalOutlined />
            Global variables
          </span>
        }
        key={AppRoutes.globalVariables.path}
      />
      <Tabs.TabPane
        tab={
          <Popconfirm
            title={
              <>
                <div>Are you sure to update cache?</div>
                <div>This action update cache for all configs!</div>
                <div>This action is not reversible!</div>
                <div>All changes would be apply in production!</div>
              </>
            }
            onConfirm={onCacheUpdateConfirm}
            okText="Yes"
            okButtonProps={{ danger: true }}
            cancelText="No"
          >
            <span className={styles.cacheTab}>
              <FireOutlined />
              Save to cache
            </span>
          </Popconfirm>
        }
        key={cacheTabKey}
      />
    </Tabs>
  );
}

export default TheNavbar;
