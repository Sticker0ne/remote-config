import React, { ReactComponentElement, useState } from 'react';
import AppLoader from 'components/common/AppLoader/AppLoader';
import { useMounted } from 'common/hooks/useMounted.hook';

function WithAsyncData({
  asyncDataFunction,
  fullscreen,
  children,
}: {
  asyncDataFunction: () => Promise<any>;
  children?: ReactComponentElement<any> | ReactComponentElement<any>[];
  fullscreen?: boolean;
}) {
  const [shouldLoaderBeShown, setLoaderVisibility] = useState(true);

  useMounted(() => {
    let isMounted = true;
    asyncDataFunction().finally(() => {
      if (isMounted) setLoaderVisibility(false);
    });

    return () => {
      isMounted = false;
    };
  });

  return shouldLoaderBeShown ? <AppLoader fullscreen={fullscreen} /> : <>{children}</>;
}

export default WithAsyncData;
