import React from 'react';
import styles from 'components/common/AppLoader/AppLoader.module.css';
import { Spin } from 'antd';
import { CommonUtils } from 'common/utils/Common.utils';

function AppLoader({ fullscreen }: { fullscreen?: boolean }) {
  const overlayClasses = CommonUtils.buildClass({
    [styles.fullScreenOverlay]: !!fullscreen,
    [styles.fullHeightOverlay]: !fullscreen,
    [styles.overlay]: true,
  });

  return (
    <div className={overlayClasses} tabIndex={0}>
      <Spin spinning={true} size={'large'}>
        <div className={styles.spinnerExpander} />
      </Spin>
    </div>
  );
}

export default AppLoader;
