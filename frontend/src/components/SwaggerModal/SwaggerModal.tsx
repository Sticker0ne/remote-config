import React from 'react';
import { Alert, Modal } from 'antd';
import SwaggerUI from 'swagger-ui-react';
import 'swagger-ui-react/swagger-ui.css';
import styles from 'components/SwaggerModal/SwaggerModal.module.css';

function SwaggerModal({
  onClose,
  configName,
  sandboxPrefix,
}: {
  onClose: () => unknown;
  configName: string;
  sandboxPrefix: string;
}) {
  const modalTitle = 'Sandbox';

  return (
    <Modal title={modalTitle} visible={true} onCancel={onClose} footer={[]} width={800}>
      <Alert
        message="Swagger ui can't change cookies in browser. Please set it manually via debug tools"
        type="error"
      />

      <div className={styles.swaggerContainer}>
        <SwaggerUI url={`/api/v1/configs/schema/${configName}?pathPrefix=${sandboxPrefix}`} />
      </div>
    </Modal>
  );
}

export default SwaggerModal;
