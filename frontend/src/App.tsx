import React from 'react';
import 'styles/global.css';
import { Router, Switch, Route } from 'react-router-dom';
import HomePage from 'pages/Home/HomePage';
import LoginPage from 'pages/Login/LoginPage';
import { commonHistory } from 'common/router/history';
import { AppRoutes } from 'common/router/routes';
import UsersPage from 'pages/Users/UsersPage';
import { DefaultLayoutRoute } from 'layouts/Default/DefaultLayout.route';
import GlobalVariablesPage from 'pages/GlobalVariables/GlobalVariablesPage';
import ConfigPage from 'pages/Config/ConfigPage';

function App() {
  return (
    <Router history={commonHistory}>
      <Switch>
        <DefaultLayoutRoute exact path={AppRoutes.home.path} component={HomePage} />
        <Route path={AppRoutes.login.path}>
          <LoginPage />
        </Route>
        <DefaultLayoutRoute path={AppRoutes.users.path} component={UsersPage} />
        <DefaultLayoutRoute path={AppRoutes.globalVariables.path} component={GlobalVariablesPage} />
        <DefaultLayoutRoute path={AppRoutes.config.path} component={ConfigPage} />
      </Switch>
    </Router>
  );
}

export default App;
