import React, { useState } from 'react';
import styles from './ExtractorEditModal.module.css';
import { Button, Form, Input, Modal, Select } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { useMounted } from 'common/hooks/useMounted.hook';
import { DataTypesEnum, ExtractWaysEnum } from 'api/index.defs';
import { Rule } from 'rc-field-form/lib/interface';
import { DataTypesEnumArray, DataTypeToVarValueMap, ExtractWaysEnumArray } from 'business/common/constants';
import { IExtractorEditFormData, IExtractorEditModalProps } from './ExtractorEditModal.types';

const nameInputRules: Rule[] = [
  () => ({
    validateTrigger: ['onFinish'],
    validator(_, value) {
      const matchPattern = new RegExp(/^[a-zA-Z]+$/).test(value);
      if (value && matchPattern) {
        return Promise.resolve();
      }
      return Promise.reject(new Error('Please input correct name! (one word a-zA-Z camelCase)'));
    },
  }),
];
const descriptionInputRules: Rule[] = [
  { required: true, message: 'Please input var description!', validateTrigger: ['onFinish'] },
];
const defaultValueInputRules: Rule[] = [
  { required: true, message: 'Please input correct default value!', validateTrigger: ['onFinish'] },
];
const extractNameInputRules: Rule[] = [
  { required: true, message: 'Please input correct extract name!', validateTrigger: ['onFinish'] },
];

function ExtractorEditModal({ configId, extractor, onClose, onCreate, onUpdate }: IExtractorEditModalProps) {
  const [form] = useForm<IExtractorEditFormData>();

  const [defaultValueStub, setDefaultValueStub] = useState(
    DataTypeToVarValueMap[DataTypesEnum.ARRAY_OF_BOOLEANS],
  );

  function onSelectValueChange() {
    const currentDataType = form.getFieldsValue().dataType;
    const newValue = DataTypeToVarValueMap[currentDataType];
    setDefaultValueStub(newValue);
    form.setFieldsValue({ stringifiedDefaultValue: extractor?.stringifiedDefaultValue || newValue });
  }

  useMounted(() => {
    form.setFieldsValue(extractor || { dataType: DataTypesEnum.BOOLEAN });
    onSelectValueChange();
    form.setFieldsValue({ extractWay: extractor?.extractWay || ExtractWaysEnum.COOKIE });
  });

  const isNewExtractor = !extractor;
  const modalTitle = isNewExtractor ? 'Create new extractor' : 'Edit extractor';
  const submitButtonTitle = isNewExtractor ? 'Create' : 'Save';

  function onModalClose() {
    if (onClose) onClose();
  }

  function onFormFinish(data: IExtractorEditFormData) {
    if (isNewExtractor && onCreate && configId) onCreate({ ...data, configId });
    else if (!isNewExtractor && onUpdate && extractor?.id) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { dataType, name, ...normalizedValue } = data;
      onUpdate(extractor.id, normalizedValue);
    }
  }

  return (
    <Modal
      title={modalTitle}
      visible={true}
      onCancel={onModalClose}
      className={styles.userEditModal}
      width={800}
      footer={[
        <div key="footer" className={styles.modalFooter}>
          <Button key="submit" type="primary" onClick={form.submit} className={styles.submitButton}>
            {submitButtonTitle}
          </Button>
        </div>,
      ]}
    >
      <Form labelCol={{ span: 6 }} wrapperCol={{ span: 24 }} form={form} onFinish={onFormFinish}>
        <Form.Item label="Extractor to variable name" name="name" rules={nameInputRules} required>
          <Input placeholder="locationCode" disabled={!isNewExtractor} />
        </Form.Item>
        <Form.Item label="Data type" name="dataType" required>
          <Select<DataTypesEnum> onSelect={onSelectValueChange} disabled={!isNewExtractor}>
            {DataTypesEnumArray.map((item) => (
              <Select.Option key={item} value={item}>
                {item}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Default value"
          name="stringifiedDefaultValue"
          rules={defaultValueInputRules}
          required
        >
          <Input.TextArea placeholder={defaultValueStub} autoSize />
        </Form.Item>
        <Form.Item label="Extract way" name="extractWay" required>
          <Select<ExtractWaysEnum>>
            {ExtractWaysEnumArray.map((item) => (
              <Select.Option key={item} value={item}>
                {item}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Extractor key" name="extractName" rules={extractNameInputRules} required>
          <Input placeholder="LOCATION_CODE" />
        </Form.Item>
        <Form.Item label="Description" name="description" rules={descriptionInputRules} required>
          <Input placeholder="Extract location from cookie" />
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default ExtractorEditModal;
