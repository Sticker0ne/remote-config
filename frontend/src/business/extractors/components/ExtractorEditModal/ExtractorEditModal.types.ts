import {
  DataTypesEnum,
  ExtractorCreateDto,
  ExtractorResponseDto,
  ExtractorUpdateDto,
  ExtractWaysEnum,
} from 'api/index.defs';

export interface IExtractorEditModalProps {
  configId: number;
  extractor?: ExtractorResponseDto;
  onUpdate?: (id: number, extractorData: ExtractorUpdateDto) => void;
  onCreate?: (extractorData: ExtractorCreateDto) => void;
  onClose?: () => void;
}

export interface IExtractorEditFormData {
  name: string;
  description: string;
  dataType: DataTypesEnum;
  stringifiedDefaultValue: string;
  extractWay: ExtractWaysEnum;
  extractName: string;
}
