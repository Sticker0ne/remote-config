import React from 'react';
import styles from './ExtractorsTable.module.css';
import { Popconfirm, Space, Table, Tooltip } from 'antd';
import { DeleteTwoTone, EditOutlined, PlusCircleOutlined } from '@ant-design/icons';
import WithScope from 'business/users/components/WithScope';
import { UserScopes } from 'business/users/enums/UserScopes.enum';
import { IExtractorsTableProps } from './ExtractorsTable.types';

function buildColumnProperties(actions: (id: number) => JSX.Element) {
  return [
    {
      title: 'Extractor varName',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Extract way',
      dataIndex: 'extractWay',
      key: 'extractWay',
    },
    {
      title: 'Extract key',
      dataIndex: 'extractName',
      key: 'extractName',
    },
    {
      title: 'Data type',
      dataIndex: 'dataType',
      key: 'dataType',
    },
    {
      title: 'Default value',
      key: 'stringifiedDefaultValue',
      dataIndex: 'stringifiedDefaultValue',
      className: styles.valueCell,
      render: (text: string) => (
        <Tooltip title={text} placement="left">
          {text}
        </Tooltip>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      dataIndex: 'id',
      render: (id: number) => actions(id),
    },
  ];
}

function ExtractorsTable({
  extractors,
  onDeleteActionClick,
  onEditActionClick,
  onAddActionClick,
  itemsPerPage,
}: IExtractorsTableProps) {
  function onDeleteClick(itemId: number) {
    if (onDeleteActionClick) onDeleteActionClick(itemId);
  }
  function onEditClick(itemId: number) {
    if (onEditActionClick) onEditActionClick(itemId);
  }
  function onAddClick() {
    if (onAddActionClick) onAddActionClick();
  }

  const deleteAction = (itemId: number) => (
    <WithScope scopes={[UserScopes.canDeleteExtractors]}>
      <Popconfirm
        title={
          <>
            <div>Are you sure to delete this extractor?</div>
            <div>Extractor data will be absolutely lost!</div>
          </>
        }
        onConfirm={() => onDeleteClick(itemId)}
        okText="Yes"
        okButtonProps={{ danger: true }}
        cancelText="No"
      >
        <Tooltip title="delete" key="delete" placement="left">
          <DeleteTwoTone twoToneColor="#eb2f96" />
        </Tooltip>
      </Popconfirm>
    </WithScope>
  );

  const editAction = (itemId: number) => (
    <Tooltip title="Edit" key="edit">
      <EditOutlined onClick={() => onEditClick(itemId)} />
    </Tooltip>
  );

  const actionsFunc = (itemId: number) => (
    <Space size={20}>
      {editAction(itemId)}
      {deleteAction(itemId)}
    </Space>
  );

  const columnProperties = buildColumnProperties(actionsFunc);
  const paginationObject = itemsPerPage ? { pageSize: itemsPerPage } : undefined;

  const titleFunc = () => <b>Config extractors</b>;
  const footerFunc = () => (
    <div className={styles.tableFooter} onClick={onAddClick}>
      <PlusCircleOutlined />
      &nbsp;&nbsp;Add new extractor
    </div>
  );

  return (
    <div className={styles.tableContainer}>
      <Table
        columns={columnProperties}
        dataSource={extractors}
        title={titleFunc}
        rowKey={'id'}
        footer={footerFunc}
        size="small"
        pagination={paginationObject}
      />
    </div>
  );
}

export default ExtractorsTable;
