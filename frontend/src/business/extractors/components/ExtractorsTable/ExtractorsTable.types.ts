import { ExtractorResponseDto } from 'api/index.defs';

export interface IExtractorsTableProps {
  extractors: ExtractorResponseDto[];
  itemsPerPage?: number;
  onDeleteActionClick?: (id: number) => void;
  onEditActionClick?: (id: number) => void;
  onAddActionClick?: () => void;
}
