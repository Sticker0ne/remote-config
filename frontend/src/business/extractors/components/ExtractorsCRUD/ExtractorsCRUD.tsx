import React, { useState } from 'react';
import { ExtractorCreateDto, ExtractorResponseDto, ExtractorUpdateDto } from 'api/index.defs';
import AppLoader from 'components/common/AppLoader/AppLoader';
import { CommonUtils } from 'common/utils/Common.utils';
import { IExtractorsCRUDProps } from './ExtractorsCRUD.types';
import { ExtractorsService } from 'api/ExtractorsService';
import ExtractorEditModal from 'business/extractors/components/ExtractorEditModal/ExtractorEditModal';
import ExtractorsTable from 'business/extractors/components/ExtractorsTable/ExtractorsTable';

const safeDelete = CommonUtils.errorifyAsyncFunction(ExtractorsService.extractorsControllerRemoveV1, {
  autoShowErrorsNotifications: true,
});
const safeUpdate = CommonUtils.errorifyAsyncFunction(ExtractorsService.extractorsControllerUpdateV1, {
  autoShowErrorsNotifications: true,
});
const safeCreate = CommonUtils.errorifyAsyncFunction(ExtractorsService.extractorsControllerCreateV1, {
  autoShowErrorsNotifications: true,
});

function ExtractorsCRUD({ tableItemsPerPage, extractorList, onItemsChange, configId }: IExtractorsCRUDProps) {
  const [modalShouldBeShown, setModalVisibility] = useState(false);
  const [itemForEdit, setItemForEdit] = useState<ExtractorResponseDto>();
  const [loaderShouldBeShown, setLoaderVisibility] = useState(false);

  function openEditModal(itemId?: number) {
    const variable = extractorList.find((item) => item.id === itemId);
    setItemForEdit(variable);

    setModalVisibility(true);
  }

  function closeEditModal() {
    setModalVisibility(false);
  }

  async function onDeleteItem(id: number) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeDelete({ id });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
    } finally {
      setLoaderVisibility(false);
    }
  }

  async function onCreateItem(data: ExtractorCreateDto) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeCreate({ body: data });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
      closeEditModal();
    } finally {
      setLoaderVisibility(false);
    }
  }

  async function onUpdateItem(id: number, data: ExtractorUpdateDto) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeUpdate({ id, body: data });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
      closeEditModal();
    } finally {
      setLoaderVisibility(false);
    }
  }

  return (
    <div>
      {loaderShouldBeShown && <AppLoader fullscreen />}
      {modalShouldBeShown && (
        <ExtractorEditModal
          configId={configId}
          extractor={itemForEdit}
          onClose={closeEditModal}
          onUpdate={onUpdateItem}
          onCreate={onCreateItem}
        />
      )}
      <ExtractorsTable
        extractors={extractorList}
        onAddActionClick={openEditModal}
        onEditActionClick={openEditModal}
        onDeleteActionClick={onDeleteItem}
        itemsPerPage={tableItemsPerPage}
      />
    </div>
  );
}

export default ExtractorsCRUD;
