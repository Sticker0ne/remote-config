import { ExtractorResponseDto } from 'api/index.defs';

export interface IExtractorsCRUDProps {
  extractorList: ExtractorResponseDto[];
  tableItemsPerPage?: number;
  onItemsChange?: () => void;
  configId: number;
}
