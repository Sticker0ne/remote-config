import { VariableResponseDto } from 'api/index.defs';

export interface IVariablesTableProps {
  variables: VariableResponseDto[];
  title?: string;
  itemsPerPage?: number;
  onDeleteActionClick?: (id: number) => void;
  onEditActionClick?: (id: number) => void;
  onAddActionClick?: () => void;
}
