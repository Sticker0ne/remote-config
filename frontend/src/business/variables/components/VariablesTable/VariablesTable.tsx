import React from 'react';
import styles from './VariablesTable.module.css';
import { IVariablesTableProps } from 'business/variables/components/VariablesTable/VariablesTable.types';
import { Popconfirm, Space, Table, Tooltip } from 'antd';
import { DeleteTwoTone, EditOutlined, PlusCircleOutlined } from '@ant-design/icons';
import WithScope from 'business/users/components/WithScope';
import { UserScopes } from 'business/users/enums/UserScopes.enum';

function buildColumnProperties(actions: (id: number) => JSX.Element) {
  return [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Data type',
      dataIndex: 'dataType',
      key: 'dataType',
    },
    {
      title: 'Value',
      key: 'stringifiedValue',
      dataIndex: 'stringifiedValue',
      className: styles.valueCell,
      render: (text: string) => (
        <Tooltip title={text} placement="left">
          {text}
        </Tooltip>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      dataIndex: 'id',
      render: (id: number) => actions(id),
    },
  ];
}

function VariablesTable({
  variables,
  title,
  onDeleteActionClick,
  onEditActionClick,
  onAddActionClick,
  itemsPerPage,
}: IVariablesTableProps) {
  function onDeleteClick(itemId: number) {
    if (onDeleteActionClick) onDeleteActionClick(itemId);
  }
  function onEditClick(itemId: number) {
    if (onEditActionClick) onEditActionClick(itemId);
  }
  function onAddClick() {
    if (onAddActionClick) onAddActionClick();
  }

  const deleteAction = (itemId: number) => (
    <WithScope scopes={[UserScopes.canDeleteVariables]}>
      <Popconfirm
        title={
          <>
            <div>Are you sure to delete this global variable?</div>
            <div>Variable data will be absolutely lost!</div>
          </>
        }
        onConfirm={() => onDeleteClick(itemId)}
        okText="Yes"
        okButtonProps={{ danger: true }}
        cancelText="No"
      >
        <Tooltip title="delete" key="delete" placement="left">
          <DeleteTwoTone twoToneColor="#eb2f96" />
        </Tooltip>
      </Popconfirm>
    </WithScope>
  );

  const editAction = (itemId: number) => (
    <Tooltip title="Edit" key="edit">
      <EditOutlined onClick={() => onEditClick(itemId)} />
    </Tooltip>
  );

  const actionsFunc = (itemId: number) => (
    <Space size={20}>
      {editAction(itemId)}
      {deleteAction(itemId)}
    </Space>
  );

  const columnProperties = buildColumnProperties(actionsFunc);
  const paginationObject = itemsPerPage ? { pageSize: itemsPerPage } : undefined;

  const titleFunc = title ? () => <b>{title}</b> : undefined;
  const footerFunc = () => (
    <div className={styles.tableFooter} onClick={onAddClick}>
      <PlusCircleOutlined />
      &nbsp;&nbsp;Add new variable
    </div>
  );

  return (
    <div className={styles.tableContainer}>
      <Table
        columns={columnProperties}
        dataSource={variables}
        title={titleFunc}
        rowKey={'id'}
        footer={footerFunc}
        size="small"
        pagination={paginationObject}
      />
    </div>
  );
}

export default VariablesTable;
