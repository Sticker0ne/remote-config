import React, { useState } from 'react';
import styles from './VariableEditModal.module.css';
import {
  IVariableEditFormData,
  IVariableEditModalProps,
} from 'business/variables/components/VariableEditModal/VariableEditModal.types';
import { Button, Form, Input, Modal, Select } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { useMounted } from 'common/hooks/useMounted.hook';
import { DataTypesEnum } from 'api/index.defs';
import { Rule } from 'rc-field-form/lib/interface';
import { DataTypesEnumArray, DataTypeToVarValueMap } from 'business/common/constants';

const nameInputRules: Rule[] = [
  () => ({
    validateTrigger: ['onFinish'],
    validator(_, value) {
      const matchPattern = new RegExp(/^[a-zA-Z]+$/).test(value);
      if (value && matchPattern) {
        return Promise.resolve();
      }
      return Promise.reject(new Error('Please input correct name! (one word a-zA-Z camelCase)'));
    },
  }),
];

const descriptionInputRules: Rule[] = [
  { required: true, message: 'Please input var description!', validateTrigger: ['onFinish'] },
];

const varValueInputRules: Rule[] = [
  { required: true, message: 'Please input correct variable value!', validateTrigger: ['onFinish'] },
];

function VariableEditModal({ variable, onClose, onCreate, onUpdate }: IVariableEditModalProps) {
  const [form] = useForm<IVariableEditFormData>();

  const [varValueStub, setVarValueStub] = useState(DataTypeToVarValueMap[DataTypesEnum.ARRAY_OF_BOOLEANS]);

  function onSelectValueChange() {
    const currentDataType = form.getFieldsValue().dataType;
    const newValue = DataTypeToVarValueMap[currentDataType];
    setVarValueStub(newValue);
    form.setFieldsValue({ stringifiedValue: variable?.stringifiedValue || newValue });
  }

  useMounted(() => {
    form.setFieldsValue(variable || { dataType: DataTypesEnum.BOOLEAN });
    onSelectValueChange();
  });

  const isNewVariable = !variable;
  const modalTitle = isNewVariable ? 'Create new variable' : 'Edit variable';
  const submitButtonTitle = isNewVariable ? 'Create' : 'Save';

  function onModalClose() {
    if (onClose) onClose();
  }

  function onFormFinish(data: IVariableEditFormData) {
    if (isNewVariable && onCreate) onCreate(data);
    else if (!isNewVariable && onUpdate && variable?.id) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { dataType, name, ...normalizedValue } = data;
      onUpdate(variable.id, normalizedValue);
    }
  }

  return (
    <Modal
      title={modalTitle}
      visible={true}
      onCancel={onModalClose}
      className={styles.userEditModal}
      width={800}
      footer={[
        <div key="footer" className={styles.modalFooter}>
          <Button key="submit" type="primary" onClick={form.submit} className={styles.submitButton}>
            {submitButtonTitle}
          </Button>
        </div>,
      ]}
    >
      <Form labelCol={{ span: 6 }} wrapperCol={{ span: 24 }} form={form} onFinish={onFormFinish}>
        <Form.Item label="Variable name" name="name" rules={nameInputRules} required>
          <Input placeholder="varName" disabled={!isNewVariable} />
        </Form.Item>
        <Form.Item label="Data type" name="dataType" required>
          <Select<DataTypesEnum> onSelect={onSelectValueChange} disabled={!isNewVariable}>
            {DataTypesEnumArray.map((item) => (
              <Select.Option key={item} value={item}>
                {item}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Value" name="stringifiedValue" rules={varValueInputRules} required>
          <Input.TextArea placeholder={varValueStub} autoSize />
        </Form.Item>
        <Form.Item label="Description" name="description" rules={descriptionInputRules} required>
          <Input placeholder="Very important var" />
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default VariableEditModal;
