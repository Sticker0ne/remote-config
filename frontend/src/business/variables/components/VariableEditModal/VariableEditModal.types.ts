import {
  DataTypesEnum,
  VariableCreateRequestDto,
  VariableResponseDto,
  VariableUpdateRequestDto,
} from 'api/index.defs';

export interface IVariableEditModalProps {
  variable?: VariableResponseDto;
  onUpdate?: (id: number, variableData: VariableUpdateRequestDto) => unknown;
  onCreate?: (variableData: VariableCreateRequestDto) => unknown;
  onClose?: () => unknown;
}

export interface IVariableEditFormData {
  name: string;
  description: string;
  dataType: DataTypesEnum;
  stringifiedValue: string;
}
