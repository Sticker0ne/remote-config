import React, { useState } from 'react';
import VariablesTable from 'business/variables/components/VariablesTable/VariablesTable';
import VariableEditModal from 'business/variables/components/VariableEditModal/VariableEditModal';
import { VariableCreateRequestDto, VariableResponseDto, VariableUpdateRequestDto } from 'api/index.defs';
import AppLoader from 'components/common/AppLoader/AppLoader';
import { CommonUtils } from 'common/utils/Common.utils';
import { VariablesService } from 'api/VariablesService';
import { IVariablesCRUDProps } from 'business/variables/components/VariablesCRUD/VariablesCRUD.types';

const safeDeleteVariables = CommonUtils.errorifyAsyncFunction(VariablesService.variablesControllerRemoveV1, {
  autoShowErrorsNotifications: true,
});
const safeUpdateVariables = CommonUtils.errorifyAsyncFunction(VariablesService.variablesControllerUpdateV1, {
  autoShowErrorsNotifications: true,
});
const safeCreateVariables = CommonUtils.errorifyAsyncFunction(VariablesService.variablesControllerCreateV1, {
  autoShowErrorsNotifications: true,
});

function VariablesCRUD({
  tableTitle,
  tableItemsPerPage,
  variableList,
  onItemsChange,
  configId,
}: IVariablesCRUDProps) {
  const [modalShouldBeShown, setModalVisibility] = useState(false);
  const [variableForEdit, setVariableForEdit] = useState<VariableResponseDto>();
  const [loaderShouldBeShown, setLoaderVisibility] = useState(false);

  function openVariableEditModal(variableId?: number) {
    const variable = variableList.find((item) => item.id === variableId);
    setVariableForEdit(variable);

    setModalVisibility(true);
  }

  function closeUserEditModal() {
    setModalVisibility(false);
  }

  async function onDeleteVariable(id: number) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeDeleteVariables({ id });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
    } finally {
      setLoaderVisibility(false);
    }
  }

  async function onCreateVariable(data: VariableCreateRequestDto) {
    setLoaderVisibility(true);
    try {
      const normalizedData = Number.isFinite(configId) ? { ...data, configId } : data;
      const { errors } = await safeCreateVariables({ body: normalizedData });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
      closeUserEditModal();
    } finally {
      setLoaderVisibility(false);
    }
  }

  async function onUpdateVariable(id: number, data: VariableUpdateRequestDto) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeUpdateVariables({ id, body: data });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
      closeUserEditModal();
    } finally {
      setLoaderVisibility(false);
    }
  }

  return (
    <div>
      {loaderShouldBeShown && <AppLoader fullscreen />}
      {modalShouldBeShown && (
        <VariableEditModal
          variable={variableForEdit}
          onClose={closeUserEditModal}
          onUpdate={onUpdateVariable}
          onCreate={onCreateVariable}
        />
      )}
      <VariablesTable
        variables={variableList}
        title={tableTitle}
        onAddActionClick={openVariableEditModal}
        onEditActionClick={openVariableEditModal}
        onDeleteActionClick={onDeleteVariable}
        itemsPerPage={tableItemsPerPage}
      />
    </div>
  );
}

export default VariablesCRUD;
