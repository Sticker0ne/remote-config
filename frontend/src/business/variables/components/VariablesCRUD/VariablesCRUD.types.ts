import { VariableResponseDto } from 'api/index.defs';

export interface IVariablesCRUDProps {
  variableList: VariableResponseDto[];
  tableTitle?: string;
  tableItemsPerPage?: number;
  onItemsChange?: () => void;
  configId?: number;
}
