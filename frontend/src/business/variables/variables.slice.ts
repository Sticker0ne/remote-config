import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from 'store';
import { VariableResponseDto } from 'api/index.defs';
import { CommonUtils } from 'common/utils/Common.utils';
import { VariablesService } from 'api/VariablesService';

interface IState {
  variables: VariableResponseDto[];
}

const initialState: IState = {
  variables: [],
};

const safeGetVariables = CommonUtils.errorifyAsyncFunction(VariablesService.variablesControllerGetGlobalV1, {
  autoShowErrorsNotifications: true,
});
export const fetchGlobalVariables = createAsyncThunk(
  'variables/fetchGlobalVariables',
  async (_action, { dispatch, rejectWithValue }) => {
    const response = await safeGetVariables();
    if (response.errors) rejectWithValue(response.errors);
    dispatch(setVariables(response.payload || []));

    return response.payload;
  },
);

export const variableSlice = createSlice({
  name: 'variables',
  initialState,
  reducers: {
    setVariables: (state, action: PayloadAction<VariableResponseDto[]>) => {
      state.variables = action.payload;
    },
  },
});

export const { setVariables } = variableSlice.actions;

export const selectVariables = (state: RootState) => state.variables.variables;

export const variablesReducer = variableSlice.reducer;
