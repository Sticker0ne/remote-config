export enum UserScopes {
  canEditCurrentUser = 'canEditCurrentUser',
  canReadUsers = 'canReadUsers',
  canDeleteVariables = 'canDeleteVariables',
  canDeleteConfigs = 'canDeleteConfigs',
  canDeleteExtractors = 'canDeleteExtractors',
  canDeleteProperty = 'canDeleteProperty',
}
