import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from 'store';
import { UserResponseDto } from 'api/index.defs';
import { CommonUtils } from 'common/utils/Common.utils';
import { UsersService } from 'api/UsersService';

interface UsersState {
  users: UserResponseDto[];
  currentUser: UserResponseDto | null;
  fetchCurrentUserPending: boolean;
}

const initialState: UsersState = {
  users: [],
  currentUser: null,
  fetchCurrentUserPending: false,
};

const safeGetCurrentUser = CommonUtils.errorifyAsyncFunction(UsersService.usersControllerGetCurrentUserV1, {
  autoShowErrorsNotifications: true,
});
export const fetchCurrentUser = createAsyncThunk(
  'users/fetchCurrentUser',
  async (_action, { dispatch, rejectWithValue }) => {
    const response = await safeGetCurrentUser();
    if (response.errors) rejectWithValue(response.errors);
    dispatch(setCurrentUser(response.payload || null));

    return response.payload;
  },
);

const safeGetAllUsers = CommonUtils.errorifyAsyncFunction(UsersService.usersControllerGetAllV1, {
  autoShowErrorsNotifications: true,
});
export const fetchAllUsers = createAsyncThunk(
  'users/fetchAllUser',
  async (_action, { dispatch, rejectWithValue }) => {
    const response = await safeGetAllUsers();
    if (response.errors) rejectWithValue(response.errors);
    dispatch(setAllUsers(response.payload || []));

    return response.payload;
  },
);

export const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setCurrentUser: (state, action: PayloadAction<UserResponseDto | null>) => {
      state.currentUser = action.payload;
    },
    setAllUsers: (state, action: PayloadAction<UserResponseDto[]>) => {
      state.users = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCurrentUser.pending, (state) => {
      state.fetchCurrentUserPending = true;
    });

    builder.addCase(fetchCurrentUser.fulfilled, (state) => {
      state.fetchCurrentUserPending = false;
    });

    builder.addCase(fetchCurrentUser.rejected, (state) => {
      state.fetchCurrentUserPending = false;
    });
  },
});

export const { setCurrentUser, setAllUsers } = usersSlice.actions;

export const selectCurrentUser = (state: RootState) => state.users.currentUser;
export const selectAllUsers = (state: RootState) => state.users.users;

export const usersReducer = usersSlice.reducer;
