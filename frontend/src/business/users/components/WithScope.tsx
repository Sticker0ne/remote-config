import React from 'react';
import PropTypes, { InferProps } from 'prop-types';
import { UserScopes } from 'business/users/enums/UserScopes.enum';
import { useAppSelector } from 'common/hooks/store.hook';
import { selectCurrentUser } from 'business/users/users.slice';

function WithScope({ children, scopes }: InferProps<typeof WithScope.propTypes> & { scopes: UserScopes[] }) {
  const currentUser = useAppSelector(selectCurrentUser);
  const currentUserScopes = currentUser?.isSuperAdmin
    ? [
        UserScopes.canEditCurrentUser,
        UserScopes.canReadUsers,
        UserScopes.canDeleteVariables,
        UserScopes.canDeleteConfigs,
        UserScopes.canDeleteExtractors,
        UserScopes.canDeleteProperty,
      ]
    : [];

  const hasNecessaryScope = scopes.some((scope) => currentUserScopes.includes(scope));

  return <>{hasNecessaryScope && children}</>;
}

WithScope.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default WithScope;
