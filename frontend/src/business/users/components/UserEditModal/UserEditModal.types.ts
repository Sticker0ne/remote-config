import { CreateUserRequestDto, UpdateUserRequestDto, UserResponseDto } from 'api/index.defs';

export interface IUserEditModalProps {
  user?: UserResponseDto;
  onUpdate?: (id: number, userData: UpdateUserRequestDto) => unknown;
  onCreate?: (userData: CreateUserRequestDto) => unknown;
  onClose?: () => unknown;
  onDelete?: (userId: number) => unknown;
}

export interface IUserEditFormData {
  firstName: string;
  lastName: string;
  password: string;
  email: string;
  isSuperAdmin: boolean;
}
