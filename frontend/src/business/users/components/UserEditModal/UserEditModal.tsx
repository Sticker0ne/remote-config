import React from 'react';
import styles from './UserEditModal.module.css';
import { Button, Form, Input, Modal, Popconfirm, Switch } from 'antd';
import {
  IUserEditFormData,
  IUserEditModalProps,
} from 'business/users/components/UserEditModal/UserEditModal.types';
import { useForm } from 'antd/es/form/Form';
import { useMounted } from 'common/hooks/useMounted.hook';
import { Rule } from 'rc-field-form/lib/interface';
import { useAppSelector } from 'common/hooks/store.hook';
import { selectCurrentUser } from 'business/users/users.slice';

const firstNameInputRules: Rule[] = [{ required: true, message: 'Please input first name!' }];
const lastNameInputRules: Rule[] = [{ required: true, message: 'Please input last name!' }];
const emailInputRules: Rule[] = [
  { required: true, message: 'Please input email!', validateTrigger: ['onFinish'] },
  { type: 'email', validateTrigger: ['onFinish'] },
];
const passwordInputRules: Rule[] = [{ required: true, message: 'Please input password!' }];

function UserEditModal({ user, onUpdate, onCreate, onDelete, onClose }: IUserEditModalProps) {
  const [form] = useForm<IUserEditFormData>();
  const currentUser = useAppSelector(selectCurrentUser);

  useMounted(() => {
    form.setFieldsValue(user || {});
  });

  const isNewUser = !user;
  const isCurrentUserOnEdit = currentUser?.id === user?.id;
  const modalTitle = isNewUser ? 'Create new user' : 'Edit user';
  const submitButtonTitle = isNewUser ? 'Create' : 'Save';
  const passwordPlaceholder = isNewUser ? 'Password' : 'Keep empty if dont wanna change';

  function onModalClose() {
    if (onClose) onClose();
  }

  function onDeleteUser() {
    if (user?.id && onDelete) onDelete(user.id);
  }

  function onFormFinish(data: IUserEditFormData) {
    const { password, ...formData } = { ...data };
    const normalizedFormData = { ...formData, password: password ? password : undefined };

    if (isNewUser && onCreate) onCreate(data);
    if (!isNewUser && onUpdate) onUpdate(user.id, normalizedFormData);
  }

  return (
    <Modal
      title={modalTitle}
      visible={true}
      onCancel={onModalClose}
      className={styles.userEditModal}
      footer={[
        <div key="footer" className={styles.modalFooter}>
          {!isNewUser && (
            <Popconfirm
              title={
                <>
                  <div>Are you sure to delete this user?</div>
                  <div>This user data will be absolutely lost!</div>
                </>
              }
              onConfirm={onDeleteUser}
              okText="Yes"
              okButtonProps={{ danger: true }}
              cancelText="No"
            >
              <Button key="remove" danger>
                Remove user
              </Button>
            </Popconfirm>
          )}
          <Button key="submit" type="primary" onClick={form.submit} className={styles.submitButton}>
            {submitButtonTitle}
          </Button>
        </div>,
      ]}
    >
      <Form labelCol={{ span: 7 }} wrapperCol={{ span: 14 }} form={form} onFinish={onFormFinish}>
        <Form.Item label="First name" name="firstName" rules={firstNameInputRules} required>
          <Input placeholder="John" />
        </Form.Item>
        <Form.Item label="Last Name" name="lastName" rules={lastNameInputRules} required>
          <Input placeholder="Doe" />
        </Form.Item>
        <Form.Item label="Email" name="email" rules={emailInputRules} required>
          <Input placeholder="Email" />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={isNewUser ? passwordInputRules : []}
          required={isNewUser}
        >
          <Input.Password placeholder={passwordPlaceholder} />
        </Form.Item>
        {!isCurrentUserOnEdit && (
          <Form.Item label="Is super admin" valuePropName="checked" name="isSuperAdmin" initialValue={false}>
            <Switch />
          </Form.Item>
        )}
      </Form>
    </Modal>
  );
}

export default UserEditModal;
