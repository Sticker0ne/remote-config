import { Avatar, Card, Popconfirm, Space, Tag, Tooltip } from 'antd';
import React from 'react';
import styles from './UserCard.module.css';
import { DeleteTwoTone, EditOutlined } from '@ant-design/icons';
import { UserResponseDto } from 'api/index.defs';
import Meta from 'antd/es/card/Meta';

function UserCard({
  user,
  showRemoveAction,
  onEditAction,
  onRemoveAction,
}: {
  user: UserResponseDto;
  showRemoveAction?: boolean;
  onEditAction?: (userId: number) => any;
  onRemoveAction?: (userId: number) => any;
}) {
  const userFullName = `${user.firstName} ${user.lastName}`;
  const avatarSrc = `https://robohash.org/${user.firstName}+${user.lastName}`;

  function onEditActionClick() {
    if (onEditAction) onEditAction(user.id);
  }

  function onRemoveActionClick() {
    if (onRemoveAction) onRemoveAction(user.id);
  }

  const removeAction = (
    <Popconfirm
      title={
        <>
          <div>Are you sure to delete this user?</div>
          <div>This user data will be absolutely lost!</div>
        </>
      }
      onConfirm={onRemoveActionClick}
      okText="Yes"
      okButtonProps={{ danger: true }}
      cancelText="No"
    >
      <Tooltip title="Remove" key="remove">
        <DeleteTwoTone key="delete" twoToneColor="#eb2f96" />
      </Tooltip>
    </Popconfirm>
  );

  const editAction = (
    <Tooltip title="Edit" key="edit">
      <EditOutlined onClick={onEditActionClick} />
    </Tooltip>
  );

  function buildActionList() {
    const result = [editAction];
    if (showRemoveAction) result.push(removeAction);

    return result;
  }

  return (
    <Card style={{ width: 290 }} actions={buildActionList()}>
      <Meta avatar={<Avatar src={avatarSrc} />} title={userFullName} description={user.email} />
      <div className={styles.tagsContainer}>
        <Space>{user.isSuperAdmin && <Tag color="blue">Super admin</Tag>}</Space>
      </div>
    </Card>
  );
}

export default UserCard;
