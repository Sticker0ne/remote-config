import { Avatar, Dropdown, Menu, Space, Typography } from 'antd';
import { InferProps } from 'prop-types';
import React from 'react';
import styles from './UserBadge.module.css';
import { LogoutOutlined, EditOutlined } from '@ant-design/icons';
import { useAppSelector } from 'common/hooks/store.hook';
import { selectCurrentUser } from 'business/users/users.slice';
import WithScope from 'business/users/components/WithScope';
import { UserScopes } from 'business/users/enums/UserScopes.enum';
import { logoutUsecase } from 'business/users/useCases/logout.usecase';

function UserBadge({}: InferProps<typeof UserBadge.propTypes>) {
  const currentUser = useAppSelector(selectCurrentUser);

  const onLogoutClick = () => logoutUsecase();
  const avatarSrc = `https://robohash.org/${currentUser?.firstName}+${currentUser?.lastName}`;

  const menu = (
    <Menu>
      <Menu.Item key="-1" disabled>
        <Typography.Text type="secondary">{currentUser?.email}</Typography.Text>
      </Menu.Item>
      <WithScope scopes={[UserScopes.canEditCurrentUser]}>
        <Menu.Item key="0">
          <EditOutlined />
          <Typography.Text className={styles.emailContainer}>Edit</Typography.Text>
        </Menu.Item>
      </WithScope>
      <Menu.Item key="1" onClick={onLogoutClick}>
        <LogoutOutlined />
        <Typography.Text className={styles.emailContainer}>Exit</Typography.Text>
      </Menu.Item>
    </Menu>
  );

  return (
    <Dropdown overlay={menu} trigger={['click']}>
      <Space align="center" className={styles.userBadge}>
        <Avatar size="large" src={avatarSrc} />
        <Typography.Text className={styles.userName}>
          {currentUser?.firstName} {currentUser?.lastName}
        </Typography.Text>
      </Space>
    </Dropdown>
  );
}

UserBadge.propTypes = {};

export default UserBadge;
