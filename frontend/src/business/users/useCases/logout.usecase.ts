import { AuthService } from 'api/AuthService';
import { commonHistory } from 'common/router/history';
import { AppRoutes } from 'common/router/routes';

export async function logoutUsecase() {
  await AuthService.authControllerLogoutV1();
  commonHistory.push(AppRoutes.login.path);
}
