import { DataTypesEnum, ExtractWaysEnum } from 'api/index.defs';

export const DataTypesEnumArray = Object.values(DataTypesEnum);
export const DataTypeToVarValueMap: Record<DataTypesEnum, string> = {
  BOOLEAN: 'false',
  STRING: '"some string value"',
  NUMBER: '42',
  ARRAY_OF_BOOLEANS: '[true, false]',
  ARRAY_OF_NUMBERS: '[1, 2]',
  ARRAY_OF_STRINGS: '["first", "second"]',
};

export const ExtractWaysEnumArray = Object.values(ExtractWaysEnum);
