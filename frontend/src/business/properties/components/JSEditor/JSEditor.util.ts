import { DataTypesEnum, ExtractorResponseDto, VariableResponseDto } from 'api/index.defs';
import { DataTypeToVarValueMap } from 'business/common/constants';

const startSeparator = '/* start user code here */';
const endSeparator = '/* end user code here */';
function buildVarsObjectString(name: string, vars: VariableResponseDto[]): string {
  const stringifiedData = vars.map((item) => `${item.name}: ${item.stringifiedValue}`).join(', ');
  return `const ${name} = { ${stringifiedData} };`;
}

function buildExtractorsObjectString(extractors: ExtractorResponseDto[]): string {
  const stringifiedData = extractors
    .map((item) => `${item.name}: ${item.stringifiedDefaultValue}`)
    .join(', ');

  return `const requestData = { ${stringifiedData} };`;
}

function buildFirstStubPart(
  globalVars: VariableResponseDto[],
  configVars: VariableResponseDto[],
  configExtractors: ExtractorResponseDto[],
  functionName: string,
): string {
  const globalVarsString = buildVarsObjectString('globalVariables', globalVars);
  const configVarsString = buildVarsObjectString('configVariables', configVars);
  const extractorsString = buildExtractorsObjectString(configExtractors);

  const dataString = `/* !IMPORTANT! Request data is object that created by config extractors. Here you can see only default values, but in runtime real REQUEST data will override default by extractors */
  \n${extractorsString}\n${globalVarsString}\n${configVarsString}\n\nconst $0 = { requestData, globalVariables, configVariables };`;

  return `${dataString}\n\nfunction ${functionName}() {
    const { requestData, globalVariables, configVariables } = $0;
    /* !IMPORTANT! if return runtype data type will not satisfy property data type then fallback value will be used instead */
    ${startSeparator}`;
}

function buildUserPart(dataType: DataTypesEnum, userCode?: string): string {
  if (userCode) return userCode;

  return `\n\n    return ${DataTypeToVarValueMap[dataType]};\n\n`;
}

function buildLastStubPart(): string {
  return `    ${endSeparator}\n}`;
}

export function buildDefaultCode({
  globalVars,
  configVars,
  configExtractors,
  propertyDataType,
  functionName,
  userCode,
}: {
  globalVars: VariableResponseDto[];
  configVars: VariableResponseDto[];
  configExtractors: ExtractorResponseDto[];
  propertyDataType: DataTypesEnum;
  functionName: string;
  userCode?: string;
}): { firstPart: string; userPart: string; lastPart: string; fullCode: string } {
  const firstPart = buildFirstStubPart(globalVars, configVars, configExtractors, functionName);
  const userPart = buildUserPart(propertyDataType, userCode);
  const lastPart = buildLastStubPart();

  const fullCode = `${firstPart}${userPart}${lastPart}`;

  return { firstPart, lastPart, userPart, fullCode };
}

export function checkValidChange(value: string, firstStubPart: string, lastStubPart: string): boolean {
  if (!value) return false;

  const firstParts = value.split(startSeparator);
  if (firstParts?.length !== 2) return false;

  const secondParts = firstParts[1].split(endSeparator);
  if (secondParts?.length !== 2) return false;

  const parts = [firstParts[0], ...secondParts];
  const firstPartAreIdentical = parts[0] + startSeparator === firstStubPart;
  const secondPartAreIdentical = endSeparator + parts[2] === lastStubPart.trim();

  if (!firstPartAreIdentical || !secondPartAreIdentical) return false;

  return true;
}

export function extractUserPart(value: string): string {
  const firstParts = value.split(startSeparator);
  if (firstParts?.length !== 2) return '';

  const secondParts = firstParts[1].split(endSeparator);
  if (secondParts?.length !== 2) return '';

  return '\n\n    ' + secondParts[0].trim() + '\n\n';
}
