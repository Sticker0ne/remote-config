import { DataTypesEnum, ExtractorResponseDto, VariableResponseDto } from 'api/index.defs';

export interface IJSEditorProps {
  globalVars: VariableResponseDto[];
  configVars: VariableResponseDto[];
  configExtractors: ExtractorResponseDto[];
  propertyDataType: DataTypesEnum;
  functionName: string;
  userCode?: string;
  onChange?: (value: string) => void;
}
