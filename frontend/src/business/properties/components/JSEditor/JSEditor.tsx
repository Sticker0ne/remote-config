import React, { useState } from 'react';
import Editor, { Monaco } from '@monaco-editor/react';
import { IJSEditorProps } from 'business/properties/components/JSEditor/JSEditor.type';
import {
  buildDefaultCode,
  checkValidChange,
  extractUserPart,
} from 'business/properties/components/JSEditor/JSEditor.util';
import { useMounted } from 'common/hooks/useMounted.hook';

function JSEditor({
  globalVars,
  configVars,
  configExtractors,
  propertyDataType,
  userCode,
  functionName,
  onChange,
}: IJSEditorProps) {
  const monaco = (window as unknown as { monaco: Monaco }).monaco;

  const builtCode = buildDefaultCode({
    globalVars,
    configVars,
    configExtractors,
    propertyDataType,
    functionName,
    userCode,
  });

  useMounted(() => {
    if (onChange) onChange(extractUserPart(builtCode.fullCode));
  });

  const [lastCodeValue, setLastCodeValue] = useState(builtCode.fullCode);

  function onEditorChange(value?: string) {
    const normalizedValue = value || '';
    if (normalizedValue === lastCodeValue) return;

    if (!checkValidChange(normalizedValue, builtCode.firstPart, builtCode.lastPart)) return onFallback();

    setLastCodeValue(normalizedValue);
    if (onChange) onChange(extractUserPart(normalizedValue));
  }

  function onFallback() {
    monaco?.editor.getModels()?.[0]?.setValue(lastCodeValue);
  }

  return (
    <Editor
      height="50vh"
      defaultLanguage="javascript"
      onChange={onEditorChange}
      defaultValue={builtCode.fullCode}
      theme="vs-dark"
      options={{
        minimap: {
          enabled: false,
        },
      }}
    />
  );
}

export default JSEditor;
