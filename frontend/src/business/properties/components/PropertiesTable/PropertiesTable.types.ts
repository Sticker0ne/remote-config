import { PropertyResponseDto } from 'api/index.defs';

export interface IPropertiesTableProps {
  properties: PropertyResponseDto[];
  itemsPerPage?: number;
  onDeleteActionClick?: (id: number) => void;
  onEditActionClick?: (id: number) => void;
  onAddActionClick?: () => void;
}
