import React from 'react';
import styles from './PropertiesTable.module.css';
import { Popconfirm, Space, Table, Tooltip } from 'antd';
import { DeleteTwoTone, EditOutlined, PlusCircleOutlined } from '@ant-design/icons';
import WithScope from 'business/users/components/WithScope';
import { UserScopes } from 'business/users/enums/UserScopes.enum';
import { IPropertiesTableProps } from 'business/properties/components/PropertiesTable/PropertiesTable.types';

function buildColumnProperties(actions: (id: number) => JSX.Element) {
  return [
    {
      title: 'Property Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Data type',
      dataIndex: 'dataType',
      key: 'dataType',
    },
    {
      title: 'Default value',
      key: 'stringifiedDefaultValue',
      dataIndex: 'stringifiedDefaultValue',
      className: styles.valueCell,
      render: (text: string) => (
        <Tooltip title={text} placement="left">
          {text}
        </Tooltip>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      dataIndex: 'id',
      render: (id: number) => actions(id),
    },
  ];
}

function PropertiesTable({
  properties,
  onDeleteActionClick,
  onEditActionClick,
  onAddActionClick,
  itemsPerPage,
}: IPropertiesTableProps) {
  function onDeleteClick(itemId: number) {
    if (onDeleteActionClick) onDeleteActionClick(itemId);
  }
  function onEditClick(itemId: number) {
    if (onEditActionClick) onEditActionClick(itemId);
  }
  function onAddClick() {
    if (onAddActionClick) onAddActionClick();
  }

  const deleteAction = (itemId: number) => (
    <WithScope scopes={[UserScopes.canDeleteProperty]}>
      <Popconfirm
        title={
          <>
            <div>Are you sure to delete this property?</div>
            <div>Property data will be absolutely lost!</div>
          </>
        }
        onConfirm={() => onDeleteClick(itemId)}
        okText="Yes"
        okButtonProps={{ danger: true }}
        cancelText="No"
      >
        <Tooltip title="delete" key="delete" placement="left">
          <DeleteTwoTone twoToneColor="#eb2f96" />
        </Tooltip>
      </Popconfirm>
    </WithScope>
  );

  const editAction = (itemId: number) => (
    <Tooltip title="Edit" key="edit">
      <EditOutlined onClick={() => onEditClick(itemId)} />
    </Tooltip>
  );

  const actionsFunc = (itemId: number) => (
    <Space size={20}>
      {editAction(itemId)}
      {deleteAction(itemId)}
    </Space>
  );

  const columnProperties = buildColumnProperties(actionsFunc);
  const paginationObject = itemsPerPage ? { pageSize: itemsPerPage } : undefined;

  const titleFunc = () => <b>Config properties</b>;
  const footerFunc = () => (
    <div className={styles.tableFooter} onClick={onAddClick}>
      <PlusCircleOutlined />
      &nbsp;&nbsp;Add new Property
    </div>
  );

  return (
    <div className={styles.tableContainer}>
      <Table
        columns={columnProperties}
        dataSource={properties}
        title={titleFunc}
        rowKey={'id'}
        footer={footerFunc}
        size="small"
        pagination={paginationObject}
      />
    </div>
  );
}

export default PropertiesTable;
