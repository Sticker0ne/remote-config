import React, { useState } from 'react';
import { PropertyCreateDto, PropertyResponseDto, PropertyUpdateDto } from 'api/index.defs';
import AppLoader from 'components/common/AppLoader/AppLoader';
import { CommonUtils } from 'common/utils/Common.utils';
import { PropertiesService } from 'api/PropertiesService';
import { IPropertiesCRUDProps } from 'business/properties/components/PropertiesCRUD/PropertiesCRUD.types';
import PropertyEditModal from 'business/properties/components/PropertyEditModal/PropertyEditModal';
import PropertiesTable from 'business/properties/components/PropertiesTable/PropertiesTable';

const safeDelete = CommonUtils.errorifyAsyncFunction(PropertiesService.propertiesControllerRemoveV1, {
  autoShowErrorsNotifications: true,
});
const safeUpdate = CommonUtils.errorifyAsyncFunction(PropertiesService.propertiesControllerUpdateV1, {
  autoShowErrorsNotifications: true,
});
const safeCreate = CommonUtils.errorifyAsyncFunction(PropertiesService.propertiesControllerCreateV1, {
  autoShowErrorsNotifications: true,
});

function PropertiesCRUD({
  tableItemsPerPage,
  propertyList,
  onItemsChange,
  configId,
  globalVars,
  configVars,
  configExtractors,
}: IPropertiesCRUDProps) {
  const [modalShouldBeShown, setModalVisibility] = useState(false);
  const [itemForEdit, setItemForEdit] = useState<PropertyResponseDto>();
  const [loaderShouldBeShown, setLoaderVisibility] = useState(false);

  function openEditModal(itemId?: number) {
    const property = propertyList.find((item) => item.id === itemId);
    setItemForEdit(property);

    setModalVisibility(true);
  }

  function closeEditModal() {
    setModalVisibility(false);
  }

  async function onDeleteItem(id: number) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeDelete({ id });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
    } finally {
      setLoaderVisibility(false);
    }
  }

  async function onCreateItem(data: PropertyCreateDto) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeCreate({ body: data });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
      closeEditModal();
    } finally {
      setLoaderVisibility(false);
    }
  }

  async function onUpdateItem(id: number, data: PropertyUpdateDto) {
    setLoaderVisibility(true);
    try {
      const { errors } = await safeUpdate({ id, body: data });
      if (errors?.length) return;
      if (onItemsChange) await onItemsChange();
      closeEditModal();
    } finally {
      setLoaderVisibility(false);
    }
  }

  return (
    <div>
      {loaderShouldBeShown && <AppLoader fullscreen />}
      {modalShouldBeShown && (
        <PropertyEditModal
          configId={configId}
          property={itemForEdit}
          onClose={closeEditModal}
          onUpdate={onUpdateItem}
          onCreate={onCreateItem}
          globalVars={globalVars}
          configVars={configVars}
          configExtractors={configExtractors}
        />
      )}
      <PropertiesTable
        properties={propertyList}
        onAddActionClick={openEditModal}
        onEditActionClick={openEditModal}
        onDeleteActionClick={onDeleteItem}
        itemsPerPage={tableItemsPerPage}
      />
    </div>
  );
}

export default PropertiesCRUD;
