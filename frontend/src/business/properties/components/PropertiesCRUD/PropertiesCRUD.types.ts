import { ExtractorResponseDto, PropertyResponseDto, VariableResponseDto } from 'api/index.defs';

export interface IPropertiesCRUDProps {
  propertyList: PropertyResponseDto[];
  tableItemsPerPage?: number;
  onItemsChange?: () => void;
  configId: number;
  globalVars: VariableResponseDto[];
  configVars: VariableResponseDto[];
  configExtractors: ExtractorResponseDto[];
}
