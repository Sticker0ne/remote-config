import React, { useState } from 'react';
import styles from './PropertyEditModal.module.css';
import { Button, Form, Input, Modal, Select } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { useMounted } from 'common/hooks/useMounted.hook';
import { DataTypesEnum } from 'api/index.defs';
import { Rule } from 'rc-field-form/lib/interface';
import { DataTypesEnumArray, DataTypeToVarValueMap } from 'business/common/constants';
import {
  IPropertyEditFormData,
  IPropertyEditModalProps,
} from 'business/properties/components/PropertyEditModal/PropertyEditModal.types';
import JSEditor from 'business/properties/components/JSEditor/JSEditor';
import { CommonUtils } from 'common/utils/Common.utils';

const nameInputRules: Rule[] = [
  () => ({
    validateTrigger: ['onFinish'],
    validator(_, value) {
      const matchPattern = new RegExp(/^[a-zA-Z]+$/).test(value);
      if (value && matchPattern) {
        return Promise.resolve();
      }
      return Promise.reject(new Error('Please input correct name! (one word a-zA-Z camelCase)'));
    },
  }),
];
const descriptionInputRules: Rule[] = [
  { required: true, message: 'Please input var description!', validateTrigger: ['onFinish'] },
];
const defaultValueInputRules: Rule[] = [
  { required: true, message: 'Please input correct default value!', validateTrigger: ['onFinish'] },
];

function PropertyEditModal({
  configId,
  property,
  onClose,
  onCreate,
  onUpdate,
  globalVars,
  configExtractors,
  configVars,
}: IPropertyEditModalProps) {
  const [form] = useForm<IPropertyEditFormData>();

  const [selectedDataType, setSelectedDataType] = useState(property?.dataType || DataTypesEnum.BOOLEAN);
  const [functionName, setFunctionName] = useState(property?.name || 'propertyName');
  const [jsEditorKey, setJsEditorKey] = useState(1);
  const [jsEditorValue, setJsEditorValue] = useState(property?.code || '');

  function reRenderJSEditor() {
    setFunctionName(form.getFieldsValue().name || 'propertyName');
    setJsEditorKey(jsEditorKey + 1);
  }

  function onSelectValueChange() {
    const currentDataType = form.getFieldsValue().dataType;
    const newValue = DataTypeToVarValueMap[currentDataType];
    setSelectedDataType(currentDataType);
    form.setFieldsValue({ stringifiedDefaultValue: property?.stringifiedDefaultValue || newValue });
    reRenderJSEditor();
  }

  useMounted(() => {
    form.setFieldsValue(property || { dataType: DataTypesEnum.BOOLEAN });
    onSelectValueChange();
  });

  const isNewProperty = !property;
  const modalTitle = isNewProperty ? 'Create new property' : 'Edit property';
  const submitButtonTitle = isNewProperty ? 'Create' : 'Save';

  function onModalClose() {
    if (onClose) onClose();
  }

  function checkUserCode(value: string): boolean {
    const isValid = value.includes('return');
    if (!isValid)
      CommonUtils.showErrors([{ code: '403', message: 'User part code should contains "return" statement' }]);

    return isValid;
  }

  function onJSEditorChange(value: string) {
    setJsEditorValue(value);
  }

  function onFormFinish(data: IPropertyEditFormData) {
    const userCodeIsValid = checkUserCode(jsEditorValue);
    if (!userCodeIsValid) return;

    const normalizedData = { ...data, code: jsEditorValue };

    if (isNewProperty && onCreate && configId) onCreate({ ...normalizedData, configId });
    else if (!isNewProperty && onUpdate && property?.id) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { dataType, name, ...normalizedValue } = normalizedData;
      onUpdate(property.id, normalizedValue);
    }
  }

  return (
    <Modal
      title={modalTitle}
      visible={true}
      onCancel={onModalClose}
      width={800}
      footer={[
        <div key="footer" className={styles.modalFooter}>
          <Button key="submit" type="primary" onClick={form.submit} className={styles.submitButton}>
            {submitButtonTitle}
          </Button>
        </div>,
      ]}
      centered
    >
      <Form labelCol={{ span: 4 }} wrapperCol={{ span: 24 }} form={form} onFinish={onFormFinish}>
        <Form.Item label="Property name" name="name" rules={nameInputRules} required>
          <Input placeholder="enableSomeFeature" disabled={!isNewProperty} onBlur={reRenderJSEditor} />
        </Form.Item>
        <Form.Item label="Data type" name="dataType" required>
          <Select<DataTypesEnum> onSelect={onSelectValueChange} disabled={!isNewProperty}>
            {DataTypesEnumArray.map((item) => (
              <Select.Option key={item} value={item}>
                {item}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Description" name="description" rules={descriptionInputRules} required>
          <Input placeholder="this feature flags enables some feature" />
        </Form.Item>
        <Form.Item
          label="Fallback value"
          name="stringifiedDefaultValue"
          rules={defaultValueInputRules}
          required
        >
          <Input.TextArea placeholder={DataTypeToVarValueMap[selectedDataType]} autoSize />
        </Form.Item>
        <JSEditor
          key={`js-editor-key-${jsEditorKey}`}
          globalVars={globalVars}
          configExtractors={configExtractors}
          configVars={configVars}
          propertyDataType={selectedDataType}
          functionName={functionName}
          userCode={property?.code}
          onChange={onJSEditorChange}
        />
      </Form>
    </Modal>
  );
}

export default PropertyEditModal;
