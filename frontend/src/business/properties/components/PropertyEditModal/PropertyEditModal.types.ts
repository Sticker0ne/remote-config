import {
  DataTypesEnum,
  ExtractorResponseDto,
  PropertyCreateDto,
  PropertyResponseDto,
  PropertyUpdateDto,
  VariableResponseDto,
} from 'api/index.defs';

export interface IPropertyEditModalProps {
  configId: number;
  property?: PropertyResponseDto;
  globalVars: VariableResponseDto[];
  configVars: VariableResponseDto[];
  configExtractors: ExtractorResponseDto[];
  onUpdate?: (id: number, extractorData: PropertyUpdateDto) => void;
  onCreate?: (extractorData: PropertyCreateDto) => void;
  onClose?: () => void;
}

export interface IPropertyEditFormData {
  name: string;
  description: string;
  code: string;
  dataType: DataTypesEnum;
  stringifiedDefaultValue: string;
}
