import { ConfigResponseDto } from 'api/index.defs';

export interface IConfigCardProps {
  config: ConfigResponseDto;
  onDeleteActionClick?: (id: number) => void;
  onEditActionClick?: (id: number) => void;
}
