import React from 'react';
import styles from './ConfigCard.module.css';
import { Card, Popconfirm, Tooltip } from 'antd';
import Meta from 'antd/es/card/Meta';
import { DeleteTwoTone, EditOutlined } from '@ant-design/icons';
import WithScope from 'business/users/components/WithScope';
import { UserScopes } from 'business/users/enums/UserScopes.enum';
import { IConfigCardProps } from 'business/configs/components/ConfigCard/ConfigCard.types';

function ConfigCard({
  config: { name, description, id },
  onDeleteActionClick,
  onEditActionClick,
}: IConfigCardProps) {
  const roboHashUrl = `https://robohash.org/${name}`;

  function onEditClick() {
    if (onEditActionClick) onEditActionClick(id);
  }

  function onDeleteClick() {
    if (onDeleteActionClick) onDeleteActionClick(id);
  }

  const deleteAction = (
    <WithScope scopes={[UserScopes.canDeleteConfigs]}>
      <Popconfirm
        title={
          <>
            <div>Are you sure to delete this config?</div>
            <div>This config data will be absolutely lost!</div>
            <div>Apps that depend on this config can break!</div>
          </>
        }
        onConfirm={onDeleteClick}
        okText="Yes"
        okButtonProps={{ danger: true }}
        cancelText="No"
      >
        <Tooltip title="Remove" key="remove">
          <DeleteTwoTone key="delete" twoToneColor="#eb2f96" />
        </Tooltip>
      </Popconfirm>
    </WithScope>
  );

  const editAction = (
    <Tooltip title="Edit" key="edit">
      <EditOutlined onClick={onEditClick} />
    </Tooltip>
  );

  return (
    <Card
      className={styles.cardContainer}
      style={{ width: 290 }}
      cover={<img src={roboHashUrl} height="100" className={styles.configImage} />}
      actions={[editAction, deleteAction]}
    >
      <Meta className={styles.meta} title={name} description={description || <>&nbsp;</>} />
    </Card>
  );
}

export default ConfigCard;
