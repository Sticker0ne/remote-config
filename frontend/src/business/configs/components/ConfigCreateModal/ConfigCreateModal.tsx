import React from 'react';
import { Button, Form, Input, Modal } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { Rule } from 'rc-field-form/lib/interface';
import {
  IConfigCreateFormData,
  IConfigCreateModalProps,
} from 'business/configs/components/ConfigCreateModal/ConfigCreateModal.types';

const nameInputRules: Rule[] = [
  () => ({
    validateTrigger: ['onFinish'],
    validator(_, value) {
      const matchPattern = new RegExp(/^[a-zA-Z]+$/).test(value);
      if (value && matchPattern) {
        return Promise.resolve();
      }
      return Promise.reject(new Error('Please input correct name! (one word a-zA-Z camelCase)'));
    },
  }),
];
const descriptionInputRules: Rule[] = [
  { required: true, message: 'Please input description name!', validateTrigger: ['onFinish'] },
];

function ConfigCreateModal({ onCreate, onClose }: IConfigCreateModalProps) {
  const [form] = useForm<IConfigCreateFormData>();

  function onModalClose() {
    if (onClose) onClose();
  }

  function onFormFinish(data: IConfigCreateFormData) {
    if (onCreate) onCreate(data);
  }

  return (
    <Modal
      title="Create new config"
      visible={true}
      onCancel={onModalClose}
      footer={[
        <Button key="submit" type="primary" onClick={form.submit}>
          Create
        </Button>,
      ]}
    >
      <Form labelCol={{ span: 7 }} wrapperCol={{ span: 14 }} form={form} onFinish={onFormFinish}>
        <Form.Item label="Config name" name="name" rules={nameInputRules} required>
          <Input placeholder="androidAppShopConfig" />
        </Form.Item>
        <Form.Item label="Description" name="description" rules={descriptionInputRules} required>
          <Input placeholder="Remote config for shop android app" />
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default ConfigCreateModal;
