import { ConfigCreateRequestDto } from 'api/index.defs';

export interface IConfigCreateModalProps {
  onCreate?: (configData: ConfigCreateRequestDto) => unknown;
  onClose?: () => unknown;
}

export interface IConfigCreateFormData {
  name: string;
  description: string;
}
