import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from 'store';
import { ConfigResponseDto } from 'api/index.defs';
import { CommonUtils } from 'common/utils/Common.utils';
import { ConfigsService } from 'api/ConfigsService';

interface IState {
  configs: ConfigResponseDto[];
  config: ConfigResponseDto | null;
}

const initialState: IState = {
  configs: [],
  config: null,
};

const safeGetConfigs = CommonUtils.errorifyAsyncFunction(ConfigsService.configsControllerGetAllV1, {
  autoShowErrorsNotifications: true,
});
export const fetchConfigs = createAsyncThunk(
  'configs/fetchConfigs',
  async (_action, { dispatch, rejectWithValue }) => {
    const response = await safeGetConfigs();
    if (response.errors) rejectWithValue(response.errors);
    dispatch(setConfigs(response.payload || []));

    return response.payload;
  },
);

const safeGetConfig = CommonUtils.errorifyAsyncFunction(ConfigsService.configsControllerGetV1, {
  autoShowErrorsNotifications: true,
});
export const fetchConfig = createAsyncThunk(
  'configs/fetchConfig',
  async (action: { id: number }, { dispatch, rejectWithValue }) => {
    const response = await safeGetConfig(action);
    if (response.errors) rejectWithValue(response.errors);
    dispatch(setConfig(response.payload || null));

    return response.payload;
  },
);

export const configsSlice = createSlice({
  name: 'configs',
  initialState,
  reducers: {
    setConfigs: (state, action: PayloadAction<ConfigResponseDto[]>) => {
      state.configs = action.payload;
    },
    setConfig: (state, action: PayloadAction<ConfigResponseDto | null>) => {
      state.config = action.payload;
    },
  },
});

export const { setConfigs, setConfig } = configsSlice.actions;

export const selectConfigs = (state: RootState) => state.configs.configs;
export const selectConfig = (state: RootState) => state.configs.config;

export const configsReducer = configsSlice.reducer;
