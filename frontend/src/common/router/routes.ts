import type { IRoute } from './router.types';

export enum AppRoutesNames {
  login = 'login',
  home = 'home',
  config = 'config',
  users = 'users',
  globalVariables = 'globalVariables',
}

export const AppRoutes: Record<AppRoutesNames, IRoute> = {
  login: { path: '/login' },
  home: { path: '/' },
  config: { path: '/config/:id', build: (params) => `/config/${params.id}` },
  users: { path: '/users' },
  globalVariables: { path: '/global-variables' },
};
