export interface IRoute {
  path: string;
  build?: (params: Record<string, any>) => string;
}
