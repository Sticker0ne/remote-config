import { useState } from 'react';

type TLoadFunc = <P extends Promise<any>>(promise: P) => P;
type THookType = [boolean, TLoadFunc];

export function useLoader(initFlagValue = false): THookType {
  const [loaderFlag, setLoader] = useState(initFlagValue);

  function load<P extends Promise<any>>(promise: P): P {
    setLoader(true);
    promise.finally(() => setLoader(false));
    return promise;
  }

  return [loaderFlag, load];
}
