import { SyntheticEvent, useRef, useState } from 'react';

export function useInput<E>(initValue: string) {
  const [value, setValue] = useState(initValue);
  const ref = useRef<E>(null);

  // eslint-disable-next-line @typescript-eslint/ban-types
  function onChange(e: SyntheticEvent<HTMLInputElement>) {
    setValue(e.currentTarget.value);
  }

  const bind = {
    value,
    onChange,
    ref,
  };
  return {
    bind,
    value,
    setValue,
    ref,
  };
}
