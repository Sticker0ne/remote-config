import { useEffect } from 'react';

export function useAsync<P>(asyncFn: () => Promise<P>, onSuccess?: (data: P) => void) {
  useEffect(() => {
    let isActive = true;
    asyncFn().then((data) => {
      if (isActive && onSuccess) onSuccess(data);
    });
    return () => {
      isActive = false;
    };
  }, [asyncFn, onSuccess]);
}
