import { useEffect } from 'react';

export function useMounted(fn: () => any): void {
  useEffect(fn, []);
}
