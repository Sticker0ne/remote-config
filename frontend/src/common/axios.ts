import axios from 'axios';
import { commonHistory } from 'common/router/history';
import { AppRoutes } from './router/routes';

export function getAxiosInstance() {
  const instance = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
  });

  instance.interceptors.request.use(function (request) {
    if (request.data === null) delete request.data;

    return request;
  });

  instance.interceptors.response.use(
    function (response) {
      return response;
    },
    function (error) {
      if (error?.response?.status === 401) commonHistory.push(AppRoutes.login.path);
      return Promise.reject(error);
    },
  );

  return instance;
}
