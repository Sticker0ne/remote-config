import type { IError } from '../types/Error.types';
import { notification } from 'antd';
import type { IErrorAble } from '../types/Error.types';

interface ICatcherParams {
  autoShowErrorsNotifications?: boolean;
}

export class CommonUtils {
  public static showErrors(errors: IError[]): void {
    if (!errors.length) return;

    errors.forEach((error) => {
      notification.error({ message: `${error.code} \t${error.message}`, description: error.details || '' });
    });
  }

  public static errorifyAsyncFunction<P extends Array<any>, R>(
    apiFunc: (...params: P) => Promise<R>,
    options?: ICatcherParams,
  ): (...params: P) => Promise<IErrorAble<R>> {
    const resFunc = async (...params: P) => {
      try {
        const res = await apiFunc(...params);
        return { payload: res };
      } catch (err: any) {
        const unexpectedError: IError = { message: 'Unexpected error', code: '500' };
        const errors = err?.response?.data?.errors || [unexpectedError];

        if (options?.autoShowErrorsNotifications) {
          CommonUtils.showErrors(errors);
        }
        return { errors };
      }
    };

    return resFunc;
  }

  public static buildClass(payload: Record<string, boolean>): string {
    const resKeys = Object.keys(payload).reduce((acc: string[], key) => {
      return payload[key] ? [...acc, key] : acc;
    }, []);

    return resKeys.join(' ');
  }
}
