import React from 'react';
import { InferProps } from 'prop-types';
import { Route } from 'react-router-dom';
import { RouteProps } from 'react-router';

export function layoutify(LayoutElement: React.ComponentType) {
  function RouteWithLayout({ component: Component, ...other }: InferProps<RouteProps<any>>) {
    return (
      <Route
        {...other}
        render={(props) => (
          <LayoutElement>
            <Component {...props} />
          </LayoutElement>
        )}
      />
    );
  }

  return RouteWithLayout;
}
