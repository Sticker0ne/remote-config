// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { codegen } = require('swagger-axios-codegen');

codegen({
  methodNameMode: 'operationId',
  remoteUrl: 'http://localhost:5000/swagger-json',
  outputDir: './src/api',
  useStaticMethod: true,
  multipleFileMode: true,
  modelMode: 'interface',
});
