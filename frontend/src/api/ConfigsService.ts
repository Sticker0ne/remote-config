import {
  ConfigCreateRequestDto,
  ConfigResponseDto,
  EnumConfigResponseDtoStatus,
  VariableResponseDto,
  DataTypesEnum,
  EnumVariableResponseDtoStatus,
  ExtractorResponseDto,
  ExtractWaysEnum,
  PropertyResponseDto,
  ConfigUpdateRequestDto,
  EnumConfigUpdateRequestDtoStatus,
  IList,
  List,
  IListResult,
  ListResultDto,
  IPagedResult,
  PagedResultDto,
  Dictionary,
  IDictionary,
  IRequestOptions,
  IRequestConfig,
  getConfigs,
  axios,
  basePath
} from './index.defs';

export class ConfigsService {
  /** Generate by swagger-axios-codegen */
  // @ts-nocheck
  /* eslint-disable */

  /**
   *
   */
  static configsControllerCreateV1(
    params: {
      /** requestBody */
      body?: ConfigCreateRequestDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ConfigResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/configs';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static configsControllerGetAllV1(options: IRequestOptions = {}): Promise<ConfigResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/configs';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static configsControllerGetV1(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ConfigResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/configs/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static configsControllerUpdateV1(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ConfigUpdateRequestDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ConfigResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/configs/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static configsControllerRemoveV1(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/configs/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static buildConfigControllerBuildV1(
    params: {
      /**  */
      name: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ConfigResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/configs/result/{name}';
      url = url.replace('{name}', params['name'] + '');

      const configs: IRequestConfig = getConfigs('all', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}
