import {
  CreateUserRequestDto,
  UserResponseDto,
  UpdateUserRequestDto,
  IList,
  List,
  IListResult,
  ListResultDto,
  IPagedResult,
  PagedResultDto,
  Dictionary,
  IDictionary,
  IRequestOptions,
  IRequestConfig,
  getConfigs,
  axios,
  basePath
} from './index.defs';

export class UsersService {
  /** Generate by swagger-axios-codegen */
  // @ts-nocheck
  /* eslint-disable */

  /**
   *
   */
  static usersControllerCreateV1(
    params: {
      /** requestBody */
      body?: CreateUserRequestDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/users';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersControllerGetAllV1(
    params: {
      /** include disabled users */
      includeDisabled?: boolean;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/users';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { includeDisabled: params['includeDisabled'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersControllerGetCurrentUserV1(options: IRequestOptions = {}): Promise<UserResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/users/current';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersControllerGetV1(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/users/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersControllerUpdateV1(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateUserRequestDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/users/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersControllerRemoveV1(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/users/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}
