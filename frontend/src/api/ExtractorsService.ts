import {
  ExtractorCreateDto,
  DataTypesEnum,
  ExtractWaysEnum,
  ExtractorResponseDto,
  ExtractorUpdateDto,
  IList,
  List,
  IListResult,
  ListResultDto,
  IPagedResult,
  PagedResultDto,
  Dictionary,
  IDictionary,
  IRequestOptions,
  IRequestConfig,
  getConfigs,
  axios,
  basePath
} from './index.defs';

export class ExtractorsService {
  /** Generate by swagger-axios-codegen */
  // @ts-nocheck
  /* eslint-disable */

  /**
   *
   */
  static extractorsControllerCreateV1(
    params: {
      /** requestBody */
      body?: ExtractorCreateDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ExtractorResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/extractors';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static extractorsControllerGetAllV1(options: IRequestOptions = {}): Promise<ExtractorResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/extractors';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static extractorsControllerGetByConfigIdV1(
    params: {
      /**  */
      configId: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ExtractorResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/extractors/byConfigId/{configId}';
      url = url.replace('{configId}', params['configId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static extractorsControllerUpdateV1(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ExtractorUpdateDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ExtractorResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/extractors/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static extractorsControllerRemoveV1(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/extractors/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}
