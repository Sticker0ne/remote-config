/** Generate by swagger-axios-codegen */
/* eslint-disable */
// @ts-nocheck
import axiosStatic, { AxiosInstance, AxiosRequestConfig } from 'axios';

export interface IRequestOptions extends AxiosRequestConfig {}

export interface IRequestConfig {
  method?: any;
  headers?: any;
  url?: any;
  data?: any;
  params?: any;
}

// Add options interface
export interface ServiceOptions {
  axios?: AxiosInstance;
}

// Add default options
export const serviceOptions: ServiceOptions = {};

// Instance selector
export function axios(configs: IRequestConfig, resolve: (p: any) => void, reject: (p: any) => void): Promise<any> {
  if (serviceOptions.axios) {
    return serviceOptions.axios
      .request(configs)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err);
      });
  } else {
    throw new Error('please inject yourself instance like axios  ');
  }
}

export function getConfigs(method: string, contentType: string, url: string, options: any): IRequestConfig {
  const configs: IRequestConfig = { ...options, method, url };
  configs.headers = {
    ...options.headers,
    'Content-Type': contentType
  };
  return configs;
}

export const basePath = '';

export interface IList<T> extends Array<T> {}
export interface List<T> extends Array<T> {}
export interface IDictionary<TValue> {
  [key: string]: TValue;
}
export interface Dictionary<TValue> extends IDictionary<TValue> {}

export interface IListResult<T> {
  items?: T[];
}

export class ListResultDto<T> implements IListResult<T> {
  items?: T[];
}

export interface IPagedResult<T> extends IListResult<T> {
  totalCount?: number;
  items?: T[];
}

export class PagedResultDto<T = any> implements IPagedResult<T> {
  totalCount?: number;
  items?: T[];
}

// customer definition
// empty

export interface CreateUserRequestDto {
  /** user email */
  email: string;

  /** user firstName */
  firstName: string;

  /** user lastName */
  lastName: string;

  /** user password */
  password: string;

  /** is user superadmin or not */
  isSuperAdmin?: boolean;
}

export interface UserResponseDto {
  /** user id */
  id: number;

  /** user email */
  email: string;

  /** user first name */
  firstName: string;

  /** user first name */
  lastName: string;

  /** is user superadmin or not */
  isSuperAdmin: boolean;

  /** is user active or not */
  isActive: boolean;

  /** when user was created */
  createdAt: Date;

  /** when user was updated */
  updatedAt: Date;
}

export interface ErrorDto {
  /** error code */
  code: string;

  /** error message */
  message: string;

  /** error details */
  details?: string;
}

export interface ErrorsDto {
  /** errors list */
  errors: ErrorDto[];
}

export interface UpdateUserRequestDto {
  /** user email */
  email?: string;

  /** user lastName */
  lastName?: string;

  /** user firstName */
  firstName?: string;

  /** user password */
  password?: string;

  /** is user active or not */
  isActive?: boolean;

  /** is user superadmin or not */
  isSuperAdmin?: boolean;
}

export interface LoginRequestDto {
  /** user email */
  email: string;

  /** user password */
  password: string;
}

export interface ConfigCreateRequestDto {
  /** config name */
  name: string;

  /** config description */
  description: string;
}

export interface VariableResponseDto {
  /** variable id */
  id: number;

  /** variable configId */
  configId: number;

  /** variable data type */
  dataType: DataTypesEnum;

  /** variable status */
  status: EnumVariableResponseDtoStatus;

  /** variable name */
  name: string;

  /** variable description */
  description: string;

  /** variable stringified value */
  stringifiedValue: string;

  /** variable createdAt */
  createdAt: Date;

  /** variable updatedAt */
  updatedAt: Date;
}

export interface ExtractorResponseDto {
  /** id */
  id: number;

  /** configId */
  configId: number;

  /** data type */
  dataType: DataTypesEnum;

  /** name */
  name: string;

  /** extract way */
  extractWay: ExtractWaysEnum;

  /** extract variable name */
  extractName: string;

  /** description */
  description: string;

  /** stringified value */
  stringifiedDefaultValue: string;

  /** createdAt */
  createdAt: Date;

  /** updatedAt */
  updatedAt: Date;
}

export interface PropertyResponseDto {
  /** id */
  id: number;

  /** configId */
  configId: number;

  /** data type */
  dataType: DataTypesEnum;

  /** name */
  name: string;

  /** js code of property */
  code: string;

  /** description */
  description: string;

  /** stringified value */
  stringifiedDefaultValue: string;

  /** createdAt */
  createdAt: Date;

  /** updatedAt */
  updatedAt: Date;
}

export interface ConfigResponseDto {
  /** config id */
  id: number;

  /** config name */
  name: string;

  /** config status */
  status: EnumConfigResponseDtoStatus;

  /** config description */
  description: string;

  /** when user was created */
  createdAt: Date;

  /** when user was updated */
  updatedAt: Date;

  /** config variables */
  variables?: VariableResponseDto[];

  /** config extractors */
  extractors?: ExtractorResponseDto[];

  /** config properties */
  properties?: PropertyResponseDto[];
}

export interface ConfigUpdateRequestDto {
  /** config new status */
  status: EnumConfigUpdateRequestDtoStatus;

  /** config new description */
  description: string;
}

export interface VariableCreateRequestDto {
  /** variable name */
  name: string;

  /** variable data type */
  dataType: DataTypesEnum;

  /** config description */
  description: string;

  /** stringified value of variable */
  stringifiedValue: string;

  /** config id */
  configId?: number;
}

export interface VariableUpdateRequestDto {
  /** config new description */
  description?: string;

  /** config new stringified value */
  stringifiedValue?: string;

  /** variable new status */
  status?: EnumVariableUpdateRequestDtoStatus;
}

export interface ExtractorCreateDto {
  /** config id */
  configId: number;

  /** data type */
  dataType: DataTypesEnum;

  /** name */
  name: string;

  /** extract way */
  extractWay: ExtractWaysEnum;

  /** extract name */
  extractName: string;

  /** description */
  description: string;

  /** extractor stringified default value */
  stringifiedDefaultValue: string;
}

export interface ExtractorUpdateDto {
  /** extract way */
  extractWay: ExtractWaysEnum;

  /** extract name */
  extractName: string;

  /** description */
  description: string;

  /** extractor stringified default value */
  stringifiedDefaultValue: string;
}

export interface PropertyCreateDto {
  /** id of parent config */
  configId: number;

  /** data type */
  dataType: DataTypesEnum;

  /** name */
  name: string;

  /** js code */
  code: string;

  /** description */
  description: string;

  /** extractor stringified default value */
  stringifiedDefaultValue: string;
}

export interface PropertyUpdateDto {
  /** js code */
  code: string;

  /** description */
  description: string;

  /** extractor stringified default value */
  stringifiedDefaultValue: string;
}

export enum DataTypesEnum {
  'BOOLEAN' = 'BOOLEAN',
  'STRING' = 'STRING',
  'NUMBER' = 'NUMBER',
  'ARRAY_OF_BOOLEANS' = 'ARRAY_OF_BOOLEANS',
  'ARRAY_OF_STRINGS' = 'ARRAY_OF_STRINGS',
  'ARRAY_OF_NUMBERS' = 'ARRAY_OF_NUMBERS'
}
export enum EnumVariableResponseDtoStatus {
  'DEFAULT' = 'DEFAULT',
  'DEPRECATED' = 'DEPRECATED'
}
export enum ExtractWaysEnum {
  'COOKIE' = 'COOKIE',
  'BODY' = 'BODY',
  'QUERY' = 'QUERY'
}
export enum EnumConfigResponseDtoStatus {
  'DEFAULT' = 'DEFAULT',
  'DEPRECATED' = 'DEPRECATED'
}
export enum EnumConfigUpdateRequestDtoStatus {
  'DEFAULT' = 'DEFAULT',
  'DEPRECATED' = 'DEPRECATED'
}
export enum EnumVariableUpdateRequestDtoStatus {
  'DEFAULT' = 'DEFAULT',
  'DEPRECATED' = 'DEPRECATED'
}
