import {
  VariableCreateRequestDto,
  DataTypesEnum,
  VariableResponseDto,
  EnumVariableResponseDtoStatus,
  VariableUpdateRequestDto,
  EnumVariableUpdateRequestDtoStatus,
  IList,
  List,
  IListResult,
  ListResultDto,
  IPagedResult,
  PagedResultDto,
  Dictionary,
  IDictionary,
  IRequestOptions,
  IRequestConfig,
  getConfigs,
  axios,
  basePath
} from './index.defs';

export class VariablesService {
  /** Generate by swagger-axios-codegen */
  // @ts-nocheck
  /* eslint-disable */

  /**
   *
   */
  static variablesControllerCreateV1(
    params: {
      /** requestBody */
      body?: VariableCreateRequestDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VariableResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/variables';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static variablesControllerGetAllV1(options: IRequestOptions = {}): Promise<VariableResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/variables';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static variablesControllerGetGlobalV1(options: IRequestOptions = {}): Promise<VariableResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/variables/global';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static variablesControllerGetByConfigIdV1(
    params: {
      /**  */
      configId: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VariableResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/variables/byConfigId/{configId}';
      url = url.replace('{configId}', params['configId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static variablesControllerUpdateV1(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: VariableUpdateRequestDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VariableResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/variables/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static variablesControllerRemoveV1(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/variables/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}
