import {
  PropertyCreateDto,
  DataTypesEnum,
  PropertyResponseDto,
  PropertyUpdateDto,
  IList,
  List,
  IListResult,
  ListResultDto,
  IPagedResult,
  PagedResultDto,
  Dictionary,
  IDictionary,
  IRequestOptions,
  IRequestConfig,
  getConfigs,
  axios,
  basePath
} from './index.defs';

export class PropertiesService {
  /** Generate by swagger-axios-codegen */
  // @ts-nocheck
  /* eslint-disable */

  /**
   *
   */
  static propertiesControllerCreateV1(
    params: {
      /** requestBody */
      body?: PropertyCreateDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PropertyResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/properties';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static propertiesControllerGetAllV1(options: IRequestOptions = {}): Promise<PropertyResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/properties';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static propertiesControllerGetByConfigIdV1(
    params: {
      /**  */
      configId: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PropertyResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/properties/byConfigId/{configId}';
      url = url.replace('{configId}', params['configId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static propertiesControllerUpdateV1(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: PropertyUpdateDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PropertyResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/properties/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static propertiesControllerRemoveV1(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/api/v1/properties/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}
