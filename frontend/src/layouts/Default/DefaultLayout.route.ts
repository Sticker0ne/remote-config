import DefaultLayout from './DefaultLayout';
import { layoutify } from 'common/utils/Layoutify';

export const DefaultLayoutRoute = layoutify(DefaultLayout);
