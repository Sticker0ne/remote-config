import React from 'react';
import styles from './DefaultLayout.module.css';
import PropTypes, { InferProps } from 'prop-types';
import { Layout } from 'antd';
import { Content } from 'antd/es/layout/layout';
import TheHeader from 'components/layout-components/TheHeader/TheHeader';
import { useAppDispatch } from 'common/hooks/store.hook';
import { fetchCurrentUser } from 'business/users/users.slice';
import WithAsyncData from 'components/common/WithAsyncData/WithAsyncData';

function DefaultLayout({ children }: InferProps<typeof DefaultLayout.propTypes>) {
  const dispatch = useAppDispatch();

  const asyncData = () => {
    return dispatch(fetchCurrentUser());
  };

  return (
    <WithAsyncData asyncDataFunction={asyncData} fullscreen>
      <Layout className={styles.layoutContainer}>
        <TheHeader />
        <Content>{children}</Content>
      </Layout>
    </WithAsyncData>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default DefaultLayout;
