import { configureStore } from '@reduxjs/toolkit';
import { usersReducer } from 'business/users/users.slice';
import { variablesReducer } from 'business/variables/variables.slice';
import { configsReducer } from 'business/configs/configs.slice';

export const store = configureStore({
  reducer: {
    users: usersReducer,
    variables: variablesReducer,
    configs: configsReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
